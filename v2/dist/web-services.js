// @ts-check

async function register()
{
    const button = document.querySelector('#buat-akun')
    button.setAttribute('disabled',true)
    button.innerText = 'memproses...'
    const regs = document.querySelectorAll('.reg')
    const fd = new FormData
    regs.forEach((e)=>{
        fd.append(e.dataset.key,e.value)
    })
    const request = await fetch(base_urls + 'advomin/register',{
        method: 'POST',
        body: fd
    })
    const data = await request.json()
    if(request.ok){
        button.removeAttribute('disabled')
        button.innerText = 'Buat Akun'
        alert(data.message)
        opens('email-verify',null)
    } else {
        button.removeAttribute('disabled')
        button.innerText = 'Buat Akun'
        alert(data.message)
    }
}

async function login()
{
    const button = document.querySelector('#login')
    button.setAttribute('disabled',true)
    button.innerText = 'memproses...'
    const logs = document.querySelectorAll('.log')
    const fd = new FormData
    logs.forEach((e)=>{
        fd.append(e.dataset.key,e.value)
    })
    const request = await fetch(base_urls + 'advomin/login',{
        method: 'POST',
        body: fd
    })
    const data = await request.json()
    if(request.ok){
        button.removeAttribute('disabled')
        button.innerText = 'Login'
        opens('',[{keys: 'token',data: data.data.Token}])
        checkState()
    } else {
        button.removeAttribute('disabled')
        button.innerText = 'Login'
        alert(data.message)
    }
}

function load_params()
{
    const query_strings = app.getParams()
    let nama = ''
    query_strings.forEach((e)=>{
        if(e.keys === 'name'){
            nama = e.data
        }
    })
    alert(nama) 
} 

function back(elem)
{
    opens(elem.dataset.page,null)
    document.querySelector('.btnback').classList.add('hidden')
    document.querySelector('.btnhome').classList.remove('hidden')
}

function back_to_page(page)
{
    opens(page,null)
    document.querySelector('.btnback').classList.add('hidden')
    document.querySelector('.btnhome').classList.remove('hidden')
}

async function add_article()
{
    const btn = document.querySelector('#buat-artikel')
    btn.innerText = 'menyimpan...'
    btn.setAttribute('disabled',false)
    const query_strings = app.getParams()
    let token = ''
    query_strings.forEach((e)=>{
        if(e.keys === 'token'){
            token = e.data
        }
    })

    const fd = new FormData
    const forms = document.querySelectorAll('.reg')
    forms.forEach((e)=>{
        fd.append(e.dataset.key,e.value)
    })
    const hdr = new Headers
    hdr.append('Authorization',token)
    const request = await fetch(base_urls + 'advomin/addarticle',{
        method: 'POST',
        body: fd,
        headers: hdr
    })
    const result = await request.json()
    if(request.ok){
        btn.innerText = 'Tambah Artikel'
        btn.removeAttribute('disabled')
        alert('artikel berhasil ditambah')
       back_to_page('article-add')
    }
}

async function update_article()
{
    const btn = document.querySelector('#ubah-artikel')
    btn.innerText = 'menyimpan...'
    btn.setAttribute('disabled',false)
    const query_strings = app.getParams()
    let token = ''
    let id = ''
    query_strings.forEach((e)=>{
        if(e.keys === 'token'){
            token = e.data
        } else if(e.keys == 'id_article'){
            id = e.data
        }
    })

    const fd = new FormData
    const forms = document.querySelectorAll('.reg')
    forms.forEach((e)=>{
        fd.append(e.dataset.key,e.value)
    })
    const hdr = new Headers
    hdr.append('Authorization',token)
    const request = await fetch(base_urls + 'advomin/updatearticle?id=' + id,{
        method: 'POST',
        body: fd,
        headers: hdr
    })
    const result = await request.json()
    if(request.ok){
        btn.innerText = 'Ubah Artikel'
        btn.removeAttribute('disabled')
        alert('artikel berhasil diupdate')
       back_to_page('article-add')
    }
}

async function delete_article(id)
{
    const ask = confirm('yakin akan menghapus?')
    if(ask){
        const req = await fetch(base_urls + 'advomin/deletearticle?id=' + id)
        if(req.ok){
            alert('berhasil hapus data')
            location.reload()
        }
    }
}

async function search(id,key)
{
    const term = document.querySelector('#' + id).value
    const req = await fetch(base_urls + 'advomin/getarticle?term=' + term)
    const res = await req.json() 
    if(req.ok){ 
        let elems = ''
        for(let e of res.data){
            if(key == 'article-list'){
                elems +=  `<li>
                <div class="lister">
                    <div class="caption">${e.Caption}</div>
                    <div class="article-excerpt">${(e.Content).substring(0,25)} ...</div>
                    <div class="lister-icon">
                    <button onclick="opens('article-add-edit',[{keys: 'id_article',data: ${e.Id}}])">Edit</button>
                        <button onclick="delete_article(${e.Id})">Hapus</button>
                    </div>
                </div>
            </li>`
            } else if('all-article-list'){
                elems +=  `<li>
                <div class="lister">
                    <div class="caption">${e.Caption}</div>
                    <div class="article-excerpt">${(e.Content).substring(0,25)} ...</div>
                    <div class="lister-icon">
                        <button onclick="opens('article-detail',[{keys: 'id_article',data: ${e.Id}}])">Selengkapnya</button> 
                    </div>
                </div>
            </li>`
            }
        } 
          document.querySelector('.' + key).innerHTML = elems 
       }
}
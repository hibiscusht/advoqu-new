"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var App = /** @class */ (function () {
    function App() {
        this.base_url = '';
        this.current_url = '';
        this.current_url = document.URL;
    }
    App.prototype.cleanUrl = function (uri) {
        var raw = uri.split('?');
        var unclean = raw[0].substring(this.base_url.length);
        var true_url = '';
        var clean = '';
        if (unclean === '') {
            clean = uri.substring(this.base_url.length);
        }
        else {
            true_url = raw[0];
            clean = uri.substring(true_url.length);
        }
        return clean;
    };
    App.prototype.getParams = function () {
        var clean_url = this.cleanUrl(this.current_url);
        var uri = new URLSearchParams(clean_url);
        var params = [];
        uri.forEach(function (value, key) {
            var values = {
                keys: key,
                data: value
            };
            params.push(values);
        });
        return params;
    };
    App.prototype.getCurrentPage = function () {
        var raw = this.current_url.split('?');
        var unclean = raw[0].substring(this.base_url.length);
        return unclean;
    };
    App.prototype.goForward = function (page) {
        return __awaiter(this, void 0, void 0, function () {
            var data, clean_url, url, page_file, q, r;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        data = {
                            page: page
                        };
                        clean_url = this.cleanUrl(this.current_url);
                        url = this.base_url + page + clean_url;
                        history.pushState(data, '', url);
                        page_file = page === '' ? 'home' : page;
                        return [4 /*yield*/, fetch(this.base_url + 'pages/' + page_file + '.html')];
                    case 1:
                        q = _a.sent();
                        r = q.text();
                        if (q.ok) {
                            return [2 /*return*/, r];
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    App.prototype.goBackward = function (page) {
        return __awaiter(this, void 0, void 0, function () {
            var data, clean_url, url, page_file, q, r;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        data = {
                            page: page
                        };
                        clean_url = this.cleanUrl(this.current_url);
                        url = this.base_url + page + clean_url;
                        history.replaceState(data, '', url);
                        page_file = page === '' ? 'home' : page;
                        return [4 /*yield*/, fetch(this.base_url + 'pages/' + page_file + '.html')];
                    case 1:
                        q = _a.sent();
                        r = q.text();
                        if (q.ok) {
                            return [2 /*return*/, r];
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    App.prototype.logout = function () {
        return __awaiter(this, void 0, void 0, function () {
            var page, data, url, page_file, q, r;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        page = '';
                        data = {
                            page: page
                        };
                        url = this.base_url;
                        history.pushState(data, '', url);
                        page_file = page === '' ? 'home' : page;
                        return [4 /*yield*/, fetch(this.base_url + 'pages/' + page_file + '.html')];
                    case 1:
                        q = _a.sent();
                        r = q.text();
                        if (q.ok) {
                            return [2 /*return*/, r];
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    App = __decorate([
        Injectibles
    ], App);
    return App;
}());
var app = new App();
app.base_url = 'http://localhost/lawfirm/v2/';
var base_url_main = 'http://localhost/lawfirm/';
function filterInjectibles(page) {
    switch (page) {
        case 'addcase':
            app.loadCaseData();
            break;
        case 'article-add-new':
            app.setBackButton('article-add');
            break;
        case 'article-add':
            app.loadArticleData(base_url_main + 'advomin/getarticle');
            break;
        case 'all-articles':
            app.loadAllArticles(base_url_main + 'advomin/getarticle');
            break;
        case 'article-detail':
            app.loadArticleDetail(base_url_main + 'advomin/getarticlebyid');
            break;
        case 'article-add-edit':
            app.loadArticleEdit(base_url_main + 'advomin/getarticlebyid');
            app.setBackButton('article-add');
            break;
        default:
            app.checkUserType();
            break;
    }
}
function opens(page, options) {
    return __awaiter(this, void 0, void 0, function () {
        var query_strings, opt_len, new_query_strings, stopped_string, matched_keys, _i, query_strings_1, string, _a, options_1, opt, last_matched, _b, options_2, opt, _c, matched_keys_1, matched, query_strings, new_query_strings, _d, query_strings_2, string, html;
        return __generator(this, function (_e) {
            switch (_e.label) {
                case 0:
                    if (options !== null) {
                        query_strings = app.getParams();
                        opt_len = options.length;
                        new_query_strings = '?';
                        stopped_string = '';
                        matched_keys = [];
                        for (_i = 0, query_strings_1 = query_strings; _i < query_strings_1.length; _i++) {
                            string = query_strings_1[_i];
                            for (_a = 0, options_1 = options; _a < options_1.length; _a++) {
                                opt = options_1[_a];
                                // jika ada entry di opt yg sama, maka value di string di timpa
                                if (opt.keys === string.keys) {
                                    new_query_strings += string.keys + '=' + opt.data + '&';
                                    stopped_string = string.keys;
                                    matched_keys.push(string.keys);
                                    break;
                                }
                            }
                            if (string.keys !== stopped_string) {
                                new_query_strings += string.keys + '=' + string.data + '&';
                            }
                        }
                        last_matched = '';
                        for (_b = 0, options_2 = options; _b < options_2.length; _b++) {
                            opt = options_2[_b];
                            for (_c = 0, matched_keys_1 = matched_keys; _c < matched_keys_1.length; _c++) {
                                matched = matched_keys_1[_c];
                                if (matched === opt.keys) {
                                    last_matched = opt.keys;
                                    break;
                                }
                            }
                            if (opt.keys !== last_matched) {
                                new_query_strings += opt.keys + '=' + opt.data + '&';
                            }
                        }
                        new_query_strings = new_query_strings.substring(0, new_query_strings.length - 1);
                        app.injectCurrentUrl(app.base_url + new_query_strings);
                    }
                    else if (options === null && page === '' || page === 'home') {
                        query_strings = app.getParams();
                        new_query_strings = '';
                        for (_d = 0, query_strings_2 = query_strings; _d < query_strings_2.length; _d++) {
                            string = query_strings_2[_d];
                            if (string.keys === 'token') {
                                new_query_strings = '?' + string.keys + '=' + string.data;
                                break;
                            }
                        }
                        app.injectCurrentUrl(app.base_url + new_query_strings);
                    }
                    return [4 /*yield*/, app.goForward(page)];
                case 1:
                    html = _e.sent();
                    if (html === undefined) {
                        document.querySelector('.body').innerHTML = '<div class="empty">Maaf, halaman tidak ditemukan</div>';
                    }
                    else {
                        document.querySelector('.body').innerHTML = html;
                        filterInjectibles(page);
                    }
                    return [2 /*return*/];
            }
        });
    });
}
function reloads() {
    var current_page = app.getCurrentPage();
    opens(current_page, null);
}
reloads();
window.onpopstate = function (state) { return __awaiter(void 0, void 0, void 0, function () {
    var html;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, app.goBackward(state.state.page)];
            case 1:
                html = _a.sent();
                if (html === undefined) {
                    document.querySelector('.body').innerHTML = '<div class="empty">Maaf, halaman tidak ditemukan</div>';
                }
                else {
                    document.querySelector('.body').innerHTML = html;
                    filterInjectibles(state.state.page);
                }
                return [2 /*return*/];
        }
    });
}); };
function checkState() {
    return __awaiter(this, void 0, void 0, function () {
        var query_strings, check_token, token, _i, query_strings_3, elem, btnlogin, btnlogout, hdr, req, res;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    query_strings = app.getParams();
                    check_token = 0;
                    token = '';
                    for (_i = 0, query_strings_3 = query_strings; _i < query_strings_3.length; _i++) {
                        elem = query_strings_3[_i];
                        if (elem.keys === 'token') {
                            check_token = 1;
                            token = elem.data;
                            break;
                        }
                    }
                    btnlogin = document.querySelector('.btnlogin');
                    btnlogout = document.querySelector('.btnlogout');
                    if (!(check_token === 1)) return [3 /*break*/, 3];
                    hdr = new Headers;
                    hdr.append('Authorization', token);
                    return [4 /*yield*/, fetch(base_url_main + 'advomin/checkuser', {
                            method: 'GET',
                            headers: hdr
                        })];
                case 1:
                    req = _a.sent();
                    return [4 /*yield*/, req.json()];
                case 2:
                    res = _a.sent();
                    if (req.ok) {
                        btnlogin.classList.add('hidden');
                        btnlogout.classList.remove('hidden');
                    }
                    else {
                        alert(res.message);
                    }
                    return [3 /*break*/, 4];
                case 3:
                    btnlogin.classList.remove('hidden');
                    btnlogout.classList.add('hidden');
                    _a.label = 4;
                case 4: return [2 /*return*/];
            }
        });
    });
}
checkState();
function logout() {
    return __awaiter(this, void 0, void 0, function () {
        var html;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, app.logout()];
                case 1:
                    html = _a.sent();
                    if (html === undefined) {
                        document.querySelector('.body').innerHTML = '<div class="empty">Maaf, halaman tidak ditemukan</div>';
                    }
                    else {
                        document.querySelector('.body').innerHTML = html;
                        location.reload();
                    }
                    return [2 /*return*/];
            }
        });
    });
}
function Injectibles(constructor) {
    return /** @class */ (function (_super) {
        __extends(newConstructor, _super);
        function newConstructor() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        newConstructor.prototype.loadCaseData = function () {
            alert('hello from Injectibles');
        };
        newConstructor.prototype.injectCurrentUrl = function (url) {
            this.current_url = url;
        };
        newConstructor.prototype.checkUserType = function () {
            return __awaiter(this, void 0, void 0, function () {
                var query_strings, check_token, token, _i, query_strings_4, elem, hdr, req, res;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            query_strings = app.getParams();
                            check_token = 0;
                            token = '';
                            for (_i = 0, query_strings_4 = query_strings; _i < query_strings_4.length; _i++) {
                                elem = query_strings_4[_i];
                                if (elem.keys === 'token') {
                                    check_token = 1;
                                    token = elem.data;
                                    break;
                                }
                            }
                            if (!(check_token === 1)) return [3 /*break*/, 3];
                            hdr = new Headers;
                            hdr.append('Authorization', token);
                            return [4 /*yield*/, fetch(base_url_main + 'advomin/checkuser', {
                                    method: 'GET',
                                    headers: hdr
                                })];
                        case 1:
                            req = _a.sent();
                            return [4 /*yield*/, req.json()];
                        case 2:
                            res = _a.sent();
                            if (req.ok) {
                                if (res.data[0].UserType == 2) {
                                    document.querySelector('.admin').classList.remove('hidden');
                                }
                            }
                            _a.label = 3;
                        case 3: return [2 /*return*/];
                    }
                });
            });
        };
        newConstructor.prototype.setBackButton = function (page) {
            var back = document.querySelector('.btnback');
            var home = document.querySelector('.btnhome');
            back.dataset.page = page;
            back.classList.remove('hidden');
            home.classList.add('hidden');
        };
        newConstructor.prototype.loadArticleData = function (url) {
            return __awaiter(this, void 0, void 0, function () {
                var req, res, elems, _i, _a, e;
                return __generator(this, function (_b) {
                    switch (_b.label) {
                        case 0: return [4 /*yield*/, fetch(url)];
                        case 1:
                            req = _b.sent();
                            return [4 /*yield*/, req.json()];
                        case 2:
                            res = _b.sent();
                            if (req.ok) {
                                elems = '';
                                for (_i = 0, _a = res.data; _i < _a.length; _i++) {
                                    e = _a[_i];
                                    elems += "<li>\n            <div class=\"lister\">\n                <div class=\"caption\">" + e.Caption + "</div>\n                <div class=\"article-excerpt\">" + (e.Content).substring(0, 25) + " ...</div>\n                <div class=\"lister-icon\">\n                    <button onclick=\"opens('article-add-edit',[{keys: 'id_article',data: " + e.Id + "}])\">Edit</button>\n                    <button onclick=\"delete_article(" + e.Id + ")\">Hapus</button>\n                </div>\n            </div>\n        </li>";
                                }
                                document.querySelector('.article-list').innerHTML = elems;
                            }
                            return [2 /*return*/];
                    }
                });
            });
        };
        newConstructor.prototype.loadAllArticles = function (url) {
            return __awaiter(this, void 0, void 0, function () {
                var req, res, elems, _i, _a, e;
                return __generator(this, function (_b) {
                    switch (_b.label) {
                        case 0: return [4 /*yield*/, fetch(url)];
                        case 1:
                            req = _b.sent();
                            return [4 /*yield*/, req.json()];
                        case 2:
                            res = _b.sent();
                            if (req.ok) {
                                elems = '';
                                for (_i = 0, _a = res.data; _i < _a.length; _i++) {
                                    e = _a[_i];
                                    elems += "<li>\n            <div class=\"lister\">\n                <div class=\"caption\">" + e.Caption + "</div>\n                <div class=\"article-excerpt\">" + (e.Content).substring(0, 25) + " ...</div>\n                <div class=\"lister-icon\">\n                    <button onclick=\"opens('article-detail',[{keys: 'id_article',data: " + e.Id + "}])\">Selengkapnya</button> \n                </div>\n            </div>\n        </li>";
                                }
                                document.querySelector('.all-article-list').innerHTML = elems;
                            }
                            return [2 /*return*/];
                    }
                });
            });
        };
        newConstructor.prototype.loadArticleDetail = function (url) {
            return __awaiter(this, void 0, void 0, function () {
                var query_strings, id, _i, query_strings_5, elem, req, res;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            query_strings = app.getParams();
                            id = 0;
                            for (_i = 0, query_strings_5 = query_strings; _i < query_strings_5.length; _i++) {
                                elem = query_strings_5[_i];
                                if (elem.keys === 'id_article') {
                                    id = elem.data;
                                    break;
                                }
                            }
                            return [4 /*yield*/, fetch(url + '?id=' + id)];
                        case 1:
                            req = _a.sent();
                            return [4 /*yield*/, req.json()];
                        case 2:
                            res = _a.sent();
                            if (req.ok) {
                                document.querySelector('#caption').innerHTML = res.data[0].Caption;
                                document.querySelector('#content').innerHTML = res.data[0].Content;
                            }
                            return [2 /*return*/];
                    }
                });
            });
        };
        newConstructor.prototype.loadArticleEdit = function (url) {
            return __awaiter(this, void 0, void 0, function () {
                var query_strings, id, _i, query_strings_6, elem, elems, req, res, _a, _b, elem;
                return __generator(this, function (_c) {
                    switch (_c.label) {
                        case 0:
                            query_strings = app.getParams();
                            id = 0;
                            for (_i = 0, query_strings_6 = query_strings; _i < query_strings_6.length; _i++) {
                                elem = query_strings_6[_i];
                                if (elem.keys === 'id_article') {
                                    id = elem.data;
                                    break;
                                }
                            }
                            elems = document.querySelectorAll('.reg');
                            return [4 /*yield*/, fetch(url + '?id=' + id)];
                        case 1:
                            req = _c.sent();
                            return [4 /*yield*/, req.json()];
                        case 2:
                            res = _c.sent();
                            if (req.ok) {
                                for (_a = 0, _b = elems; _a < _b.length; _a++) {
                                    elem = _b[_a];
                                    elem.value = res.data[0][elem.dataset.key];
                                }
                            }
                            return [2 /*return*/];
                    }
                });
            });
        };
        return newConstructor;
    }(constructor));
}

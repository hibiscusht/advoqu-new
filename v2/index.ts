const app = new App()

app.base_url = 'http://localhost/lawfirm/v2/' 

const base_url_main: string = 'http://localhost/lawfirm/'

function filterInjectibles(page: string)
{
    switch(page){
        case 'addcase': 
            (app as any).loadCaseData()
        break;
        case 'article-add-new':
            (app as any).setBackButton('article-add')
        break;
        case 'article-add':
            (app as any).loadArticleData(base_url_main + 'advomin/getarticle')
        break;
        case 'all-articles':
            (app as any).loadAllArticles(base_url_main + 'advomin/getarticle')
        break;
        case 'article-detail':
            (app as any).loadArticleDetail(base_url_main + 'advomin/getarticlebyid')
        break;
        case 'article-add-edit':
            (app as any).loadArticleEdit(base_url_main + 'advomin/getarticlebyid');
            (app as any).setBackButton('article-add')
        break;
        default: 
            (app as any).checkUserType()
        break;
    }
}

async function opens(page: string,options: null | Array<any>)
{

    if(options !== null){

        const query_strings: Array<any> = app.getParams()

        const opt_len: number = options.length

        let new_query_strings: string = '?'

        let stopped_string: string = ''

        let matched_keys: Array<string> = []

       for(let string of query_strings){
           for(let opt of options){
              // jika ada entry di opt yg sama, maka value di string di timpa
              if(opt.keys === string.keys){
                new_query_strings += string.keys + '=' + opt.data + '&'
                stopped_string = string.keys
                matched_keys.push(string.keys)
                break
              } 
           }
           if(string.keys !== stopped_string){
            new_query_strings += string.keys + '=' + string.data + '&'
           }
       }

       let last_matched: string = ''
       for(let opt of options){
          for(let matched of matched_keys){
            if(matched === opt.keys){
                last_matched = opt.keys
                break
            }
          }
          if(opt.keys !== last_matched){
            new_query_strings += opt.keys + '=' + opt.data + '&'
          }
       }

       new_query_strings = new_query_strings.substring(0,new_query_strings.length - 1);

       (app as any).injectCurrentUrl(app.base_url + new_query_strings)
    } else if(options === null && page === '' || page === 'home') {
        const query_strings: Array<any> = app.getParams()
        let new_query_strings: string = ''
        for(let string of query_strings){
            if(string.keys === 'token'){
                new_query_strings = '?' + string.keys + '=' + string.data
                break
            }
         }
         (app as any).injectCurrentUrl(app.base_url + new_query_strings)
    }


    let html = await app.goForward(page)
    if(html === undefined){
        (<HTMLDivElement>document.querySelector('.body')).innerHTML = '<div class="empty">Maaf, halaman tidak ditemukan</div>'
    } else {
        (<HTMLDivElement>document.querySelector('.body')).innerHTML = html
        filterInjectibles(page)
    }
}
 
function reloads()
{
    const current_page: string = app.getCurrentPage()
    opens(current_page,null)
}

reloads()

window.onpopstate = async (state: PopStateEvent) => { 
    let html = await app.goBackward(state.state.page) 
    if(html === undefined){
        (<HTMLDivElement>document.querySelector('.body')).innerHTML = '<div class="empty">Maaf, halaman tidak ditemukan</div>'
    } else {
        (<HTMLDivElement>document.querySelector('.body')).innerHTML = html
        filterInjectibles(state.state.page)
    }
}

async function checkState()
{
    const query_strings: Array<any> = app.getParams()
    let check_token: number = 0
    let token: string = ''
    for(let elem of query_strings){
        if(elem.keys === 'token'){
            check_token = 1
            token = elem.data
            break
        }
    }

    
    let btnlogin = (<HTMLButtonElement>document.querySelector('.btnlogin')) 
    let btnlogout = (<HTMLButtonElement>document.querySelector('.btnlogout'))

    if(check_token === 1){

        const hdr = new Headers
        hdr.append('Authorization',token)

        const req = await fetch(base_url_main + 'advomin/checkuser',{
            method: 'GET',
            headers: hdr
        })

        const res = await req.json()

        if(req.ok){
            btnlogin.classList.add('hidden')
            btnlogout.classList.remove('hidden')
        } else {
            alert(res.message)
        }

        
    } else {
        btnlogin.classList.remove('hidden')
        btnlogout.classList.add('hidden') 
    }
}

checkState()

async function logout()
{
    let html = await app.logout()
    if(html === undefined){
        (<HTMLDivElement>document.querySelector('.body')).innerHTML = '<div class="empty">Maaf, halaman tidak ditemukan</div>'
    } else {
        (<HTMLDivElement>document.querySelector('.body')).innerHTML = html
        location.reload()
    }
  
}
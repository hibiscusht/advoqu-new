@Injectibles
class App 
{
    public base_url: string = ''
    private current_url: string = ''
    public constructor()
    {
        this.current_url = document.URL
    } 
    private cleanUrl(uri: string)
    {
        const raw: Array<string> = uri.split('?')
        const unclean: string = raw[0].substring(this.base_url.length)
        let true_url: string = ''
        let clean: string = ''
        if(unclean === ''){
            clean = uri.substring(this.base_url.length)
        } else {
            true_url = raw[0]
            clean = uri.substring(true_url.length)
        }
         
        return clean
    }
    public getParams()
    {
        const clean_url: string = this.cleanUrl(this.current_url)
        const uri = new URLSearchParams(clean_url)
        let params: Array<any> = []
        uri.forEach((value,key)=>{
            const values = {
                keys: key,
                data: value
            }
            params.push(values)
        })
        return params
    }
    public getCurrentPage()
    {
        const raw: Array<string> = this.current_url.split('?')
        const unclean: string = raw[0].substring(this.base_url.length)
        return unclean
    }
    public async goForward(page: string)
    {
        let data = {
            page: page
        }
        const clean_url: string = this.cleanUrl(this.current_url)
        const url: string = this.base_url + page + clean_url
        history.pushState(data,'',url)
        const page_file: string = page === '' ? 'home' : page
        let q = await fetch(this.base_url + 'pages/' + page_file + '.html')
        let r = q.text()
        if(q.ok){
            return r
        }
    }

    public async goBackward(page: string)
    {
        let data = {
            page: page
        }
        const clean_url: string = this.cleanUrl(this.current_url)
        const url: string = this.base_url + page + clean_url
        history.replaceState(data,'',url)
        const page_file: string = page === '' ? 'home' : page
        let q = await fetch(this.base_url + 'pages/' + page_file + '.html')
        let r = q.text()
        if(q.ok){
            return r
        }
    }

    public async logout()
    {
        const page: string = ''
        let data = {
            page: page
        }
          const url: string = this.base_url 
        history.pushState(data,'',url)
        const page_file: string = page === '' ? 'home' : page
        let q = await fetch(this.base_url + 'pages/' + page_file + '.html')
        let r = q.text()
        if(q.ok){
            return r
        }
    }
    
}
function Injectibles<T extends { new (...args: any[]): any }>(constructor: T): T {
    return class newConstructor extends constructor {
       public loadCaseData()
       {
         alert('hello from Injectibles')
       }
       public injectCurrentUrl(url: string)
       {
         this.current_url = url
       }
       public async checkUserType()
      {
        const query_strings: Array<any> = app.getParams()
        let check_token: number = 0
        let token: string = ''
        for(let elem of query_strings){
            if(elem.keys === 'token'){
                check_token = 1
                token = elem.data
                break
            }
          }
 

          if(check_token === 1){

              const hdr = new Headers
              hdr.append('Authorization',token)

              const req = await fetch(base_url_main + 'advomin/checkuser',{
                  method: 'GET',
                  headers: hdr
               })

              const res = await req.json()

              if(req.ok){
                    if(res.data[0].UserType == 2){
                        (<HTMLDivElement>document.querySelector('.admin')).classList.remove('hidden')
                    }
                }   

              }  
       }
       public setBackButton(page: string)
       {
          const back = (<HTMLButtonElement>document.querySelector('.btnback'))
          const home = (<HTMLButtonElement>document.querySelector('.btnhome'))

          back.dataset.page = page
          back.classList.remove('hidden')
          home.classList.add('hidden')
       }
       public async loadArticleData(url: string)
       {
         const req = await fetch(url)
         const res = await req.json()
         if(req.ok){ 
          let elems = ''
          for(let e of res.data){
            elems +=  `<li>
            <div class="lister">
                <div class="caption">${e.Caption}</div>
                <div class="article-excerpt">${(e.Content).substring(0,25)} ...</div>
                <div class="lister-icon">
                    <button onclick="opens('article-add-edit',[{keys: 'id_article',data: ${e.Id}}])">Edit</button>
                    <button onclick="delete_article(${e.Id})">Hapus</button>
                </div>
            </div>
        </li>`
          } 
           (<HTMLUListElement>document.querySelector('.article-list')).innerHTML = elems
         }
       }
       public async loadAllArticles(url: string)
       {
         const req = await fetch(url)
         const res = await req.json()
         if(req.ok){ 
          let elems = ''
          for(let e of res.data){
            elems +=  `<li>
            <div class="lister">
                <div class="caption">${e.Caption}</div>
                <div class="article-excerpt">${(e.Content).substring(0,25)} ...</div>
                <div class="lister-icon">
                    <button onclick="opens('article-detail',[{keys: 'id_article',data: ${e.Id}}])">Selengkapnya</button> 
                </div>
            </div>
        </li>`
          } 
           (<HTMLUListElement>document.querySelector('.all-article-list')).innerHTML = elems
         }
       }
       public async loadArticleDetail(url: string)
       {
         const query_strings: Array<any> = app.getParams()
         let id: number = 0
         for(let elem of query_strings){
          if(elem.keys === 'id_article'){ 
              id = elem.data
              break
          }
        }
         const req = await fetch(url + '?id=' + id)
         const res = await req.json()
         if(req.ok){  
           (<HTMLElement>document.querySelector('#caption')).innerHTML = res.data[0].Caption;
           (<HTMLElement>document.querySelector('#content')).innerHTML = res.data[0].Content;
         }
       }
       public async loadArticleEdit(url: string)
       {
         const query_strings: Array<any> = app.getParams()
         let id: number = 0
         for(let elem of query_strings){
          if(elem.keys === 'id_article'){ 
              id = elem.data
              break
          }
        }
         const elems = (<NodeList>document.querySelectorAll('.reg'))
         const req = await fetch(url + '?id=' + id)
         const res = await req.json()
         if(req.ok){  
           for(let elem of (elems as any)){
              elem.value = res.data[0][elem.dataset.key]
           } 
         }
       }
    }
}
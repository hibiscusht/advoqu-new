<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Chat extends CI_Controller {

    public function __construct(){ 
        parent::__construct(); 
        $this->load->model("chat_m");
    }  
    
    public function send(){
        $msg = $this->input->get("msg");
        $usr = $this->input->get("usr");
        $target = $this->input->get("target");
        $this->chat_m->addMessage($usr,$msg,$target);
    }

    public function read(){
        $usr = $this->input->get("usr");
        $target = $this->input->get("target");
        echo $this->chat_m->getMessage($usr,$target);
    }

}
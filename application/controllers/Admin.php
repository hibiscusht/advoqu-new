<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

private $abs_path;    

public function __construct(){ 
    parent::__construct(); 
    $this->load->model("admin_m");
    $this->abs_path = dirname(__FILE__,3);
} 


private function fin_uploader($name,$path){
    $file = $_FILES[$name];
    $f = "";
        $_FILES['file']['name'] = $file['name'];
        $_FILES['file']['type'] = $file['type'];
        $_FILES['file']['tmp_name'] = $file['tmp_name'];
        $_FILES['file']['error'] = $file['error'];
        $_FILES['file']['size'] = $file['size'];
        $conf["upload_path"] = $this->abs_path.$path;
        $conf["allowed_types"] = "jpg|jpeg|png|gif";
        $rnd = rand(1,1000000);
        $conf["file_name"] = $rnd."_files_"; 
        $this->load->library("upload",$conf);
        $this->upload->do_upload("file");
        $e = $this->upload->data(); 
       $ef = $e["file_name"];
       return  $ef; 
}

private function views($data){
    $this->load->view("main",$data);
}

public function index(){
    $this->views(array("page" => "home_admin"));
}

public function login(){
    $this->views(array("page" => "login_admin"));
}

public function login_mitra(){
    $this->views(array("page" => "login_mitra"));
}

public function module(){
    $token = $this->input->get("token");
    $res = $this->admin_m->menu($token);
    echo '{"rows":'.json_encode($res).'}';
}

public function klien(){
    $this->views(array("page" => "data_klien"));
}

public function klien_data(){
    $id = $this->input->get("id");
    if($id == ""){
        $res = $this->admin_m->get_client_data();
    } else {
        $res = $this->admin_m->get_client($id);
    }
    
    echo '{"rows":'.$res.'}';
}

public function klien_status(){
    $status = $this->input->get("id");
    $this->admin_m->set_status_client($status);
}

public function klien_update(){
    $token = $this->input->get("token");
    $dt = $this->input->post();
    $this->admin_m->update_klien($dt,$token);
}

public function klien_valid(){
    $status = $this->input->get("id");
    $this->admin_m->set_valid_client($status);
}

public function klien_delete(){
    $id = $this->input->get("id"); 
    $this->admin_m->delete_klien($id);
}

public function profile(){
    $this->views(array("page" => "profil_klien"));
}


 

public function consult_admin(){
    $this->views(array("page" => "consult_admin")); 
}

public function consult_add(){
    $q = $this->input->post("q");
    $a = $this->input->post("a");
    $slug = preg_replace(array("/\s/","/\?/"),array("-",""),substr(strip_tags($q),0,25));
    $data = array("question"=>$q,"answer"=>$a,"slug"=>$slug);
    $this->admin_m->admin_consult_add($data);
}

public function consult_update(){
    $id = $this->input->get("id");
    $q = $this->input->post("q");
    $a = $this->input->post("a"); 
    $slug = preg_replace(array("/\s/","/\?/"),array("-",""),substr(strip_tags($q),0,25));
    $data = array("question"=>$q,"answer"=>$a,"slug"=>$slug);
    $this->admin_m->admin_consult_update($data,$id);
}

public function consult_data(){ 
    $id = $this->input->get("id");
    $res = $this->admin_m->admin_consult_data($id);
    echo '{"rows":'.json_encode($res).'}';
}


public function consult_delete(){
    $id = $this->input->get("id"); 
    $this->admin_m->admin_consult_delete($id);
}

public function kategori(){
    $this->views(array("page" => "kategori")); 
}


public function kategori_data(){ 
    $id = $this->input->get("id");
    $res = $this->admin_m->category_data($id);
    echo '{"rows":'.json_encode($res).'}';
}


public function kategori_add(){
    $data = $this->input->post();
    $this->admin_m->category_add($data);
}


public function kategori_update(){
    $id = $this->input->get("id");
    $data = $this->input->post(); 
    $this->admin_m->category_update($data,$id);
}


public function kategori_delete(){
    $id = $this->input->get("id"); 
    $this->admin_m->category_delete($id);
}


public function kanban(){
    $this->views(array("page" => "kanban"));
}


public function feed_data(){ 
    $id = $this->input->get("id");
    $res = $this->admin_m->feed_data($id);
    echo '{"rows":'.json_encode($res).'}';
}


public function feed_status(){ 
    $id = $this->input->get("id");
    $res = $this->admin_m->feed_status($id);
    echo '{"rows":'.json_encode($res).'}';
}


public function feed_detail(){ 
    $id = $this->input->get("id");
    $where = $this->input->post();
    $res = $this->admin_m->feed_detail($id,$where);
    echo '{"rows":'.json_encode($res).'}';
}
  


public function advonews(){
    $this->views(array("page" => "advonews")); 
}


public function advonews_data(){ 
    $id = $this->input->get("id");
    $limit = $this->input->get("lim");
    $res = $this->admin_m->advonews_data($id,$limit);
    echo '{"rows":'.json_encode($res).'}';
}

public function advonews_add(){
    $data = $this->input->post();
    $title = $this->input->post("title");
    $data["img"] = $this->fin_uploader("img","/public/uploaded");
    $data["slug"] = str_replace(" ","-",substr($title,0,25));
    $this->admin_m->advonews_add($data);
}


public function advonews_update(){
    $data = $this->input->post();
    $title = $this->input->post("title");
    $id = $this->input->get("id");
    if(count($_FILES) > 0){
        $dt = $this->admin_m->advonews_data($id);
        if(count($dt) > 0){
            unlink($this->abs_path."/public/uploaded/".$dt[0]->img);
        }
        $data["img"] = $this->fin_uploader("img","/public/uploaded");
    }
    $data["slug"] = str_replace(" ","-",substr($title,0,25));
    $this->admin_m->advonews_update($data,$id);
}


public function advonews_delete(){
    $data = $this->input->post();
    $id = $this->input->get("id"); 
    $dt = $this->admin_m->advonews_data($id);
    unlink($this->abs_path."/public/uploaded/".$dt[0]->img); 
    $this->admin_m->advonews_delete($id);
}


public function svc(){
    $this->views(array("page" => "svc")); 
}


public function svc_add(){
    $data = $this->input->post();
    $this->admin_m->svc_add($data);
}


public function svc_data(){ 
    $id = $this->input->get("id");
    $res = $this->admin_m->svc_data($id);
    echo '{"rows":'.json_encode($res).'}';
}


public function svc_update(){
    $id = $this->input->get("id");
    $data = $this->input->post(); 
    $this->admin_m->svc_update($data,$id);
}


public function svc_delete(){
    $data = $this->input->post();
    $id = $this->input->get("id");  
    $this->admin_m->svc_delete($id);
}

public function share(){
    $this->views(array("page" => "share")); 
}

public function share_tmp(){
    $this->views(array("page" => "share_tmp")); 
}

public function tmp_data(){ 
    $id = $this->input->get("id");
    $res = $this->admin_m->tmp_data($id);
    echo '{"rows":'.json_encode($res).'}';
}

public function tmp_affirm(){
    $id = $this->input->get("id");
    $res = $this->admin_m->tmp_affirm($id);
}


public function mitra_data(){ 
    $id = $this->input->get("id");
    $res = $this->admin_m->mitra_data($id);
    echo '{"rows":'.json_encode($res).'}';
}


public function sharesvc(){
    $this->views(array("page" => "share_mitra")); 
}

public function sharesvc_add(){
    $data = $this->input->post(); 
    $this->admin_m->sharesvc_add($data);
}

public function sharesvc_update(){
    $id = $this->input->get("id");
    $data = $this->input->post(); 
    $this->admin_m->sharesvc_update($data,$id);
}

public function sharesvc_data(){ 
    $id = $this->input->get("id");
    $res = $this->admin_m->sharesvc_data($id);
    echo '{"rows":'.json_encode($res).'}';
}


public function sharesvc_delete(){
    $id = $this->input->get("id"); 
    $this->admin_m->sharesvc_delete($id);
}


public function testimoni(){
    $this->views(array("page" => "testimoni")); 
}

public function testimoni_add(){
    $data = $this->input->post(); 
    $data["foto"] = $this->fin_uploader("foto","/public/uploaded");
    $this->admin_m->testimoni_add($data);
}

public function testimoni_update(){
    $id = $this->input->get("id");
    $data = $this->input->post(); 
    $this->admin_m->testimoni_update($data,$id);
}

public function testimoni_data(){ 
    $id = $this->input->get("id");
    $res = $this->admin_m->testimoni_data($id);
    echo '{"rows":'.json_encode($res).'}';
}


public function testimoni_delete(){
    $id = $this->input->get("id"); 
    $this->admin_m->testimoni_delete($id);
}

public function profile_mitra(){
    $this->views(array("page" => "profile_mitra")); 
}

public function klien_checkstatus(){
    $token = $this->input->get("token");
    $res = $this->admin_m->status_klien($token);
    echo '{"rows":'.json_encode($res).'}';
}

}

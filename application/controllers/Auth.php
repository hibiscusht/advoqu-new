<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

public function __construct(){ 
    parent::__construct(); 
    $this->load->model("auth_m");
}   

public function register(){
    $data = array();
    foreach($this->input->post() as $key => $val){
        $data[$key] = ($key == "password")? md5($val) : $val;
    }
    $this->auth_m->add_member($data);
}


public function validate(){
    $u = $this->input->post("username");
    $p = md5($this->input->post("password"));
    $token = $this->auth_m->validate_member($u,$p);
    echo $token;
}


public function admin_validate(){
    $u = $this->input->post("username");
    $p = md5($this->input->post("password"));
   $token =  $this->auth_m->validate_admin($u,$p);
   $wkt = date("Y-m-d H:i:s");
   $this->auth_m->logs($token,"login",$wkt);
   echo $token;
}


public function mitra_validate(){
    $u = $this->input->post("username");
    $p = md5($this->input->post("password"));
   $token =  $this->auth_m->validate_mitra($u,$p);
   $wkt = date("Y-m-d H:i:s");
   $this->auth_m->logs($token,"login",$wkt);
   echo $token;
}

public function rbac(){
    $token = $this->input->get("token");
    echo '{"rows":'.$this->auth_m->rbac($token).'}';
}

public function create_mitra(){
    $data = [];
    foreach($this->input->post() as $key => $val){
        if($key == "password"){
            $data[$key] = md5($val);
        } else {
            $data[$key] = $val;
        }
    }
    $data["token"] = md5(rand(1,10000000));
    $this->auth_m->create_mitra($data);
}

}
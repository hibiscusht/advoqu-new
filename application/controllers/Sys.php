<?php
defined('BASEPATH') OR exit('No direct script access allowed');

header("Content-Type:application/json");

class Sys extends CI_Controller {

public function __construct(){ 
    parent::__construct(); 
    $this->load->model("sys_m");
}    

public function read(){
    $table = $this->input->get("table");
    $where = ($this->input->post() != "")? $this->input->post() : "";
    $limit = ($this->input->get("limit") == "")? "2" : $this->input->get("limit");
    $res = $this->sys_m->simple_read($table,$where,$limit);
    echo '{"rows":'.strip_tags($res).'}';
}

public function reads(){
    $table = $this->input->get("table");
    $where = ($this->input->post() != "")? $this->input->post() : "";
    $limit = ($this->input->get("limit") == "")? "1" : $this->input->get("limit");
    $res = $this->sys_m->simple_read($table,$where,$limit);
    echo '{"rows":'.$res.'}';
}

public function readed(){
    $table = $this->input->get("table");
    $where = ($this->input->post() != "")? $this->input->post() : "";
    $limit = $this->input->get("limit"); /* -1,0,1,2,... */
    $res = $this->sys_m->simple_read($table,$where,$limit);
    echo '{"rows":'.$res.'}';
}

public function read_like(){
    $table = $this->input->get("table");
    $like = ($this->input->post() != "")? $this->input->post() : "";
    $res = $this->sys_m->simple_like($table,$like);
    echo '{"rows":'.strip_tags($res).'}';
}

public function create(){
    $table = $this->input->get("table");
    $data = $this->input->post();
    $this->sys_m->simple_create($table,$data);
}


public function open_read(){
    $table = $this->input->get("table");
    $where = ($this->input->get("status") == "1")? "WHERE " : "";
    $end = count($this->input->post());
    foreach($this->input->post() as $k => $v){
            $where .= $k."='".$v."' AND ";
    }
     $where = substr($where,0,strlen($where) - 4);
    $limit = $this->input->get("limit"); /* -1,0,1,2,... */
    $res = $this->sys_m->open_read("SELECT COUNT(id) as total FROM $table $where");
    echo '{"rows":'.$res.'}';
}

}

?>
<?php

namespace App\Controllers\Process;

class Tokenizer 
{

    private static $agent;

    private static $timestamp;

    public function __construct()
    {
        
    }

    /**
     * Generate Token
     * @param user_data array
     * @return token
     */

   public static function generate($userdata)
   {
       self::$agent = 'dt-erp';
       self::$timestamp = date("Y-m-d");
       $agent = base64_encode(self::$agent);
       $timestamp = base64_encode(self::$timestamp);
       $payload = base64_encode(json_encode($userdata));
       return $agent . '.' . $timestamp . '.' . $payload;
   }

   /**
    * Get Logged User If not Expired
    * @param token string
    * @return json string
    * @return value int
    */

 public static function validate($token)
 {
    self::$agent = 'dt-erp';
    self::$timestamp = date("Y-m-d");
    $tkn = explode(".",$token);
    if(count($tkn) > 1){
        $expired = strtotime(self::$timestamp) - (24 * 60 * 60);
        $timestamp = strtotime(base64_decode($tkn[1]));
        $agent = base64_decode($tkn[0]);
        if($timestamp <= $expired){
            return 0; //expired
        } else if($agent != self::$agent) {
            return 1; //invalid request
        } else {
            return json_decode(base64_decode($tkn[2]),true);
        } 
    } else {
        return 1;
    }
    
 }

 /**
  * Generate Password
  * @param password string
  * @return hash string
  */

  public static function generatePasswordHash($password)
  {
      return password_hash($password,PASSWORD_BCRYPT);
  }

  /**
   * Validate Password
   * @param password string
   * @param hash string
   * @return status bool
   */

   public static function validatePasswordHash($password,$hash)
    {
        return password_verify($password,$hash);
    }


}


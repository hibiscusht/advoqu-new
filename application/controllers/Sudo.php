<?php
defined('BASEPATH') OR exit('No direct script access allowed');

date_default_timezone_set("Asia/Bangkok");
header("Content-Type:application/json");

class Sudo extends CI_Controller {

public function __construct(){ 
    parent::__construct(); 
    $this->load->model("sudo_m");
}    

public function index_browser(){
    header("HTTP/1.0 200 OK"); 
    $return = array("status"=>200,"message"=>"success");
    echo json_encode($return);
}

public function Login(){ 
    $pwd = md5($this->input->post("password"));
    $res = $this->sudo_m->open_read("SELECT id FROM users WHERE username = 'root' AND password = '$pwd'");
    if(count($res) > 0){
        $otp = rand(1000,9000);
        $this->sudo_m->open_read("UPDATE users SET otp = $otp WHERE id = ".$res[0]->id);
        header("HTTP/1.0 200 OK"); 
        $data =  array("otp"=>$otp);
        $return = array("status"=>200,"message"=>"success","data"=>$data);
        echo json_encode($return);
    } else {
        header("HTTP/1.0 401 UNAUTHORIZED"); 
        echo '{"status":"401","message":"unauthorized user"}';
    }
}


public function Logout(){ 
    $otp =  $this->input->post("otp");
    $res = $this->sudo_m->open_read("SELECT id FROM users WHERE otp = $otp");
    if(count($res) > 0){ 
        $this->sudo_m->open_read("UPDATE users SET otp = NULL WHERE id = ".$res[0]->id);
        header("HTTP/1.0 200 OK");  
        $return = array("status"=>200,"message"=>"success");
        echo json_encode($return);
    } else {
        header("HTTP/1.0 406 NOT ACCEPTABLE"); 
        echo '{"status":"406","message":"invalid user credential"}';
    }
}

public function GetAdmin(){ 
    $otp =  $this->input->post("otp");
    $res = $this->sudo_m->open_read("SELECT id FROM users WHERE otp = $otp");
    if(count($res) > 0){ 
        $rest = $this->sudo_m->open_read("SELECT id,fullname FROM users WHERE type = 1");
        header("HTTP/1.0 200 OK");  
        $return = array("status"=>200,"message"=>"success","data"=>$rest);
        echo json_encode($return);
    } else {
        header("HTTP/1.0 401 UNAUTHORIZED"); 
        echo '{"status":"401","message":"unauthorized user"}';
    }
}

public function GetModuleAccessId(){ 
    $otp =  $this->input->post("otp");
    $res = $this->sudo_m->open_read("SELECT id FROM users WHERE otp = $otp");
    if(count($res) > 0){ 
        $rest = $this->sudo_m->open_read("SELECT id,name FROM module WHERE user_type = 1");
        header("HTTP/1.0 200 OK");  
        $return = array("status"=>200,"message"=>"success","data"=>$rest);
        echo json_encode($return);
    } else {
        header("HTTP/1.0 401 UNAUTHORIZED"); 
        echo '{"status":"401","message":"unauthorized user"}';
    }
}


public function SetAdminAccess(){ 
    $otp =  $this->input->post("otp");
    $id =  $this->input->post("admin_id");
    $access =  $this->input->post("access_id");
    $res = $this->sudo_m->open_read("SELECT id FROM users WHERE otp = $otp");
    if(count($res) > 0){ 
        $rest = $this->sudo_m->open_read("UPDATE users SET access = '$access' WHERE id = $id");
        header("HTTP/1.0 200 OK");  
        $return = array("status"=>200,"message"=>"success");
        echo json_encode($return);
    } else {
        header("HTTP/1.0 401 UNAUTHORIZED"); 
        echo '{"status":"401","message":"unauthorized user"}';
    }
}


}

?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

public function __construct(){ 
    parent::__construct(); 
}    


private function views($data){
    $this->load->view("main",$data);
}

public function index(){
    $this->views(array("page" => "home"));
}


public function login(){
    $this->views(array("page" => "login"));
}


public function register(){
    $this->views(array("page" => "register"));
}

public function about(){
    $this->views(array("page" => "about"));
}

public function consult($title){
    $this->views(array("page" => "consult","titles"=>$title));
}


public function news($title){
    $this->views(array("page" => "news","titles"=>$title));
}


public function mitra($title){
    $this->views(array("page" => "mitra","titles"=>$title));
}
 


}

?>
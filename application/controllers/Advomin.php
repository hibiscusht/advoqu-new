<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require 'Tokenizer.php';

use App\Controllers\Process\Tokenizer;
 
class Advomin extends CI_Controller {

    public function __construct(){ 
        parent::__construct(); 
        $this->load->model("dtmodel");
    }   

    private function getUserToken()
    {
        $header = getallheaders();
        return $header['authorization'];
    }

    private function setReturn($code,$message,$data)
    {
        $return = [
            'status'=>$code,
            'message'=>$message,
            'data'=>$data
        ];
        header('HTTP/1.0 ' . $code);
        header('Content-Type: application/json');
        echo json_encode($return);
    }

    public function register()
    {
        $post = $this->input->post();
        $Users = $this->dtmodel->schema('users');
        $where = ['where'=>['Email'=>$post['Email']]];
        $res = $Users->findOneOrNew($where);
        if(!$res){
            $this->setReturn(400,'Maaf, akun sudah ada',[]);
        } else {
            $post['Verify'] = rand(9000,9999);
            $post['Password'] = Tokenizer::generatePasswordHash($post['Password']);
            $post['UserType'] = 2;
            $post['Created'] = date('Y-m-d');
            $Users->create($post);
            $this->setReturn(200,'Selamat, pendaftaran anda berhasil. Silakan cek email anda untuk verifikasi',[]);
        }
    }

    public function login()
    {
        $post = $this->input->post();
        $Users = $this->dtmodel->schema('users');
        $where = ['where'=>['Email'=>$post['Email']]];
        $res = $Users->findOne($where);
        if(!$res){
            $this->setReturn(400,'Maaf, akun belum terdaftar',[]);
        } else {
            $cek = Tokenizer::validatePasswordHash($post['Password'],$res[0]->Password);
            if($cek){
                $token = Tokenizer::generate($res);
                $this->setReturn(200,'success',['Token'=>$token]);
            } else {
                $this->setReturn(400,'Maaf,password salah',[]);
            }
         }
    }

    public function checkuser()
    {
        $auth = $this->getUserToken();
        $res = Tokenizer::validate($auth);
        if($res == 0){
            $this->setReturn(403,'whoops, your token has expired',[]);
        } else if($res == 1){
            $this->setReturn(400,'whoops, invalid request',[]);
        } else {
            $this->setReturn(200,'success',$res);
        }
    }

    public function addarticle()
    {
        $auth = $this->getUserToken();
        $res = Tokenizer::validate($auth);
        if($res == 0){
            $this->setReturn(403,'whoops, your token has expired',[]);
        } else if($res == 1){
            $this->setReturn(400,'whoops, invalid request',[]);
        } else {
            $Article = $this->dtmodel->schema('articles');
            $post = $this->input->post();
            $post['UserId'] = $res[0]['Id'];
            $post['Created'] = date("Y-m-d");
            $Article->create($post);
            $this->setReturn(200,'success',[]);
        }
       
    }

    public function getarticle()
    {
        $Article = $this->dtmodel->schema('articles');
        $term = $this->input->get('term');
        $where = [];
        if($term !== ''){
            $where = ['like'=>['Caption'=>$term]];
        }
        $res = $Article->findAll($where);
        $this->setReturn(200,'success',$res);
    }

    public function getarticlebyid()
    {
        $Article = $this->dtmodel->schema('articles');
        $id = $this->input->get('id');
        $res = $Article->findOne(['where'=>['Id'=>$id]]);
        $this->setReturn(200,'success',$res);
    }

    public function deletearticle()
    {
        $Article = $this->dtmodel->schema('articles');
        $id = $this->input->get('id');
        $Article->findOne(['where'=>['Id'=>$id]]);
        $Article->remove();
        $this->setReturn(200,'success',[]);
    }

    public function updatearticle()
    {
        $id = $this->input->get('id');
        $post = $this->input->post();
        $Article = $this->dtmodel->schema('articles');
        $where = ['where'=>['Id'=>$id]];
        $res = $Article->findOne($where);
        if(!$res){
            $this->setReturn(400,'failed',[]);
        } else {
            $Article->update($post);
            $this->setReturn(200,'success',[]);
        }
    }

}
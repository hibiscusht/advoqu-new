<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sys_m extends CI_Model {

public function __construct(){  
}   

public function simple_read($table,$where,$limit){
    $this->db->select("*");
    $this->db->from($table);
    $this->db->where($where);
    if($limit < 0){}
    else if($limit == 0){
      $this->db->limit(25);
    } else if($limit == 1 || $limit == 2){
      $this->db->limit($limit);
    } else {
      $this->db->limit(25,$limit);
    }
    $this->db->order_by("id desc");
    $res = $this->db->get()->result();
    return json_encode($res);
}


public function open_read($sql){ 
  $res = $this->db->query($sql)->result();
  return json_encode($res);
}

public function simple_like($table,$like){
  $this->db->select("*");
  $this->db->from($table);
  $this->db->like($like);
  $res = $this->db->get()->result();
  return json_encode($res);
}

public function simple_create($table,$data){
  $this->db->insert($table,$data);
}

}
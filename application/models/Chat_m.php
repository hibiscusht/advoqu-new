<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Chat_m extends CI_Model {

public function __construct(){
    date_default_timezone_set("Asia/Bangkok");  
}   

public function addMessage($usr,$msg,$tgt){
    $data = array("user"=>$usr,"message"=>$msg,"target"=>$tgt);
    $this->db->insert("chat",$data);
}

public function getMessage($usr,$tgt){
    $this->db->select("*");
    $this->db->from("chat");
    $this->db->where("user",$usr);
    $this->db->or_where("target",$tgt);
    return json_encode($this->db->get()->result());
}


}
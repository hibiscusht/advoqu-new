<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_m extends CI_Model {

public function __construct(){  
}   

public function menu($token){
    //cari jenis user
    $this->db->select("access,type");
    $this->db->from("users");
    $this->db->where("token",$token);
   $user = $this->db->get()->result();

    //panggil module
    //module access
    $access = explode(",",$user[0]->access);
    $this->db->select("*");
    $this->db->from("module");
    $this->db->where("status",1);
    $this->db->where("user_type",$user[0]->type); 
    $this->db->where_in("id",$access); 
   $ret = $this->db->get()->result();

   return $ret;

   
}


public function get_client_data(){

    $this->db->select("a.id AS usr_id, a.*");
    $this->db->from("users a"); 
    $this->db->where("a.type",2); 
    $res = $this->db->get()->result();
    return json_encode($res); 
}

public function get_client($id){

    $this->db->select("a.id AS usr_id, a.*,b.*");
    $this->db->from("users a");
    $this->db->join("profile_klien b","a.token = b.user");
    $this->db->where("a.type",2);
    if($id != ""){
        $this->db->where("a.id",$id);
    }
    $res = $this->db->get()->result();
    return json_encode($res); 
}

public function set_status_client($id){
    //cek status
    $this->db->select("status");
    $this->db->from("users");
    $this->db->where("id",$id);
    $res = $this->db->get()->result();
    $status = ($res[0]->status == "1")? null : "1";
    $dt = array("status"=>$status);
    $this->db->where("id",$id);
    $this->db->update("users",$dt);
}

public function set_valid_client($id){
    //cek status
    $this->db->select("profile_klien.status");
    $this->db->from("users");
    $this->db->join("profile_klien","users.token = profile_klien.user");
    $this->db->where("users.id",$id);
    $res = $this->db->get()->result(); 
    
    $this->db->where("id",$id);
    $this->db->update("users",$dt);
}

public function update_klien($dt,$token){
    //cek status
    $this->db->select("id");
    $this->db->from("profile_klien");
    $this->db->where("user",$token);
    $res = $this->db->get()->result();
    if(count($res) > 0){
        $this->db->where("user",$token);
        $this->db->update("profile_klien",$dt);
    } else {
        $dt["user"] = $token;
        $this->db->insert("profile_klien",$dt);
    }
}


public function delete_klien($id){

    //manggil token klien
    $this->db->select("token");
    $this->db->from("users");
    $this->db->where("id",$id);
    $res = $this->db->get()->result();
    $token = $res[0]->token;

    //hapus klien
    $this->db->where("user",$token);
    $this->db->delete("profile_klien");

    $this->db->where("id",$id);
    $this->db->delete("users");
}

public function admin_consult_add($data){
    $this->db->insert("faq",$data);
}

public function admin_consult_update($data,$id){
    $this->db->where("id",$id);
    $this->db->update("faq",$data);
}

public function admin_consult_data($id = ""){
    $this->db->select("*");
    $this->db->from("faq");
    if($id != ""){
        $this->db->where("id",$id);
    }
    $res = $this->db->get()->result();
    return $res;
}


public function admin_consult_delete($id){
    $this->db->where("id",$id);
    $this->db->delete("faq");
}


public function category_data($id = ""){
    $this->db->select("*");
    $this->db->from("kategori_user");
    if($id != ""){
        $this->db->where("id",$id);
    }
    $res = $this->db->get()->result();
    return $res;
}


public function category_add($data){
    $this->db->insert("kategori_user",$data);
}


public function category_update($data,$id){
    $this->db->where("id",$id);
    $this->db->update("kategori_user",$data);
}


public function category_delete($id){
    $this->db->where("id",$id);
    $this->db->delete("kategori_user");
}


public function feed_data($id = ""){
    $this->db->select("*");
    $this->db->from("feeds");
    if($id != ""){
        $this->db->where("id",$id);
    }
    $res = $this->db->get()->result();
    return $res;
}

public function feed_status($id = ""){
    $this->db->select("*");
    $this->db->from("feed_status");
    if($id != ""){
        $this->db->where("id",$id);
    }
    $res = $this->db->get()->result();
    return $res;
}


public function feed_detail($id = "",$where = ""){
    $this->db->select("*");
    $this->db->from("feed_detail");

    if($id != ""){
        $this->db->where("id",$id);
    }   
    
    
    if($where != ""){
            $this->db->where($where);
        }
  

   
    $res = $this->db->get()->result(); 
    return $res;
}


public function advonews_data($id = "",$limit = ""){
    $this->db->select("*");
    $this->db->from("news");

    if($limit != ""){
        $this->db->limit(25,$limit);
    }


    if($id != ""){
        $this->db->where("id",$id);
    }

    $this->db->order_by("id desc");


    $res = $this->db->get()->result();
    return $res;
}


public function advonews_add($data){
    $this->db->insert("news",$data);
}


public function advonews_update($data,$id){
    $this->db->where("id",$id);
    $this->db->update("news",$data);
}


public function advonews_delete($id){
    $this->db->where("id",$id);
    $this->db->delete("news");
}


public function svc_add($data){
    $this->db->insert("service",$data);
}


public function svc_data($id = ""){
    $this->db->select("*");
    $this->db->from("service");
    if($id != ""){
        $this->db->where("id",$id);
    }
    $res = $this->db->get()->result();
    return $res;
}


public function svc_update($data,$id){
    $this->db->where("id",$id);
    $this->db->update("service",$data);
}


public function svc_delete($id){
    $this->db->where("id",$id);
    $this->db->delete("service");
}


public function tmp_data($id = "",$limit = ""){
    $this->db->select("*");
    $this->db->from("tmp_mitra");

    if($limit != ""){
        $this->db->limit(25,$limit);
    }


    if($id != ""){
        $this->db->where("id",$id);
    }

    $this->db->order_by("id desc");


    $res = $this->db->get()->result();
    return $res;
}

public function tmp_affirm($id){
    //panggil data
    $this->db->select("*");
    $this->db->from("tmp_mitra");
    $this->db->where("id",$id);
    $data = $this->db->get()->result();

    //pindahkan ke user
    $user["username"] = $data[0]->username;
    $user["password"] = $data[0]->password;
    $user["token"] = $data[0]->token;
    $user["fullname"] = $data[0]->fullname;
    $user["status"] = 1;
    $user["type"] = 3;
    $user["cat"] = 1;
    $this->db->insert("users",$user);

    //pindahkan ke mitra
    $mitra["email"] = $data[0]->username;
    $mitra["user"] = $data[0]->token;
    $mitra["geo_loc"] = $data[0]->geo_loc;
    $mitra["geo_latlng"] = $data[0]->geo_latlng;
    $this->db->insert("mitra",$mitra);

    //hapus tmp
    
    $this->db->where("id",$id);
    $this->db->delete("tmp_mitra");

}


public function mitra_data($id = "",$limit = ""){
    $this->db->select("*");
    $this->db->from("mitra");

    if($limit != ""){
        $this->db->limit(25,$limit);
    }


    if($id != ""){
        $this->db->where("id",$id);
    }

    $this->db->order_by("id desc");


    $res = $this->db->get()->result();
    return $res;
}

public function sharesvc_add($data){
    $this->db->insert("layanan",$data);
}

public function sharesvc_update($data,$id){
    $this->db->where("id",$id);
    $this->db->update("layanan",$data);
}


public function sharesvc_data($id = "",$limit = ""){
    $this->db->select("*");
    $this->db->from("layanan");

    if($limit != ""){
        $this->db->limit(25,$limit);
    }


    if($id != ""){
        $this->db->where("id",$id);
    }

    $this->db->order_by("id desc");


    $res = $this->db->get()->result();
    return $res;
}

public function sharesvc_delete($id){
    $this->db->where("id",$id);
    $this->db->delete("layanan");
}

public function testimoni_add($data){
    $this->db->insert("testimoni",$data);
}

public function testimoni_update($data,$id){
    $this->db->where("id",$id);
    $this->db->update("testimoni",$data);
}


public function testimoni_data($id = "",$limit = ""){
    $this->db->select("*");
    $this->db->from("testimoni");

    if($limit != ""){
        $this->db->limit(25,$limit);
    }


    if($id != ""){
        $this->db->where("id",$id);
    }

    $this->db->order_by("id desc");


    $res = $this->db->get()->result();
    return $res;
}

public function testimoni_delete($id){
    $this->db->where("id",$id);
    $this->db->delete("testimoni");
}

public function status_klien($token){
    $this->db->select("*");
    $this->db->from("profile_klien");
    return $this->db->get()->result();
}

}
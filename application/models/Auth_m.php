<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_m extends CI_Model {

public function __construct(){
    date_default_timezone_set("Asia/Bangkok");  
}   

public function logs($token,$act,$wkt){
    $data = array("wkt"=>$wkt,"act"=>$act,"token"=>$token);
    $this->db->insert("logs",$data); 
}

public function add_member($data){
    $data["token"] = md5(rand(1,1000000));
    $data["type"] = 2;
    $this->db->insert("users",$data);
}

public function validate_member($u,$p){
     $this->db->select("token");
     $this->db->from("users");
     $this->db->where(array("username"=>$u,"password"=>$p,"type" => 2,"status"=>1));
     $res = $this->db->get()->result();
     if(count($res) > 0){
         return $res[0]->token;
     } else {
         return "invalid";
     }
}


public function validate_mitra($u,$p){
    $this->db->select("token");
    $this->db->from("users");
    $this->db->where(array("username"=>$u,"password"=>$p,"type" => 3,"status"=>1));
    $res = $this->db->get()->result();
    if(count($res) > 0){
        return $res[0]->token;
    } else {
        return "invalid";
    }
}


public function validate_admin($u,$p){
    $this->db->select("token");
    $this->db->from("users");
    $this->db->where(array("username"=>$u,"password"=>$p,"type" => 1,"status"=>1));
    $res = $this->db->get()->result();
    if(count($res) > 0){
        return $res[0]->token;
    } else {
        return "invalid";
    }
}


public function rbac($token){
    $this->db->select("type,fullname");
    $this->db->from("users");
    $this->db->where("token",$token);
    $res = $this->db->get()->result();
    return json_encode($res);
}

public function create_mitra($data){
    $this->db->insert("tmp_mitra",$data);
}

}

<?php  
switch($page){
    case "login": require "public/front/login.php";   break; 
    case "home": require "public/front/index.php";   break; 
    case "register": require "public/back/register.php";   break;
    case "home_admin": require "public/back/home_admin.php";   break; 
    case "login_admin": require "public/back/index.php";   break;
    case "login_mitra": require "public/back/login_mitra.php";   break;
    case "data_klien": require "public/back/data_klien.php";   break;
    case "about": require "public/front/about.php";   break; 
    case "profil_klien": require "public/back/profil_klien.php";   break;
    case "kanban": require "public/back/kanban.php";   break;
    case "consult": require "public/front/consult.php";   break;
    case "consult_admin": require "public/back/consult_admin.php";   break;
    case "kategori": require "public/back/kategori.php";   break; 
    case "news": require "public/front/news.php";   break; 
    case "advonews": require "public/back/news.php";   break;
    case "svc": require "public/back/svc.php";   break;
    case "mitra": require "public/front/mitra.php";   break;
    case "share": require "public/back/mitra.php";   break;
    case "share_tmp": require "public/back/mitra_tmp.php";   break;
    case "share_mitra": require "public/back/share_mitra.php";   break;
    case "profile_mitra": require "public/back/profile_mitra.php";   break;
    case "testimoni": require "public/back/testimoni.php";   break;
}

?>
<?php 
$pg = "news";
require "public/setup.php";  
require "header.php";


switch($titles){
    case "search": 
        
        $qterm = $this->input->get("q");

        ?>


<div role="main" class="main">
				<div class="container">
					<div class="row pt-5 pb-4">

						<div class="col">

							<h1 class="mb-0">Hasil Advofind untuk "<?php echo $qterm; ?>"</h1>
							<div class="divider divider-primary divider-small mb-4">
								<hr class="mr-auto">
							</div>
                        <span class="advonews">
						<i class="fa fa-spin fa-spinner fa-fw"></i>
						</span>
						

						 
							 
						</div>

					</div>

				</div>
			</div>


<script>
   
   function excerpt(str){
    let regex = /(<([^>]+)>)/gi;
    let set = str.replace(regex,""); 
    return (set.substring(0,25) + "...");
  }
         async function load_news(){ 
             let fd = new FormData();
             fd.append("title","<?php echo $this->input->get("q"); ?>");
           let dt = await fetch("<?php echo $url; ?>/sys/read_like?table=news",{
               method:"POST",
               body: fd
           });
	        let dta = await dt.json();
	        if(dt.ok){ 
		let k = "";
		for(let i in dta.rows){
			let aut = await fetch("<?php echo $url; ?>/auth/rbac?token=" +  dta.rows[i].author );
     		 let auth = await aut.json();
			let tgl = new Intl.DateTimeFormat("id-ID",{ dateStyle: 'full', timeStyle: 'short' }).format(new Date(dta.rows[i].updated));
			let ext = /\.jpg|png|gif|jpeg|JPG|JPEG|PNG|GIF/g;  
			let gambar = ((dta.rows[i].img).match(ext) != null)? "<?php echo $url; ?>/public/uploaded/" + dta.rows[i].img  : "<?php echo $url; ?>/public/back/img/advoqu_logo2.png";
            k += "	<div class='row mt-4'> 	<div class='col'> 	<span class='thumb-info thumb-info-side-image thumb-info-no-zoom border mt-4'> 	<span class='thumb-info-side-image-wrapper p-0 d-none d-md-block'> 	<a title='' href='<?php echo $url; ?>/kabaradvoqu/" + dta.rows[i].slug + "'> 	<img src='" + gambar + "' class='img-fluid' alt='' style='width: 195px;'> 	</a>	</span> <span class='thumb-info-caption'> 	<span class='thumb-info-caption-text py-0 px-4'>  	<h2 class='mb-3 mt-2'><a title='' class='text-dark' href='<?php echo $url; ?>/kabaradvoqu/" + dta.rows[i].slug + "'>" + dta.rows[i].title  + "</a></h2> 	<span class='post-meta'> 	<span>" + tgl + "| <a href='#'>" + auth.rows[0].fullname  + "</a></span> 	</span> 	<p class='text-3 px-0 px-md-3'>" + excerpt(dta.rows[i].content)  + "</p> 	<a class='mt-3' href='<?php echo $url; ?>/kabaradvoqu/" + dta.rows[i].slug + "'>Read More <i class='fas fa-long-arrow-alt-right'></i></a>	</span> 	</span> 	</span> 	</div> </div>";
		}
		document.querySelector(".advonews").innerHTML = (k == "")? "<i class='fa fa-search'></i> tidak ada hasil ditemukan " : k;
	}
         }

         load_news();
</script>

        <?php
        
        break;
    case "home": ?>
    
<style>
@media (max-width: 700px){
	.page-link {
		font-size: 0.85em !important;
		padding: 1em !important;

	}
	.page-link .fa {
		font-size: 0.85em !important;
		padding: 0.1em !important
	}
}
</style>

    <div role="main" class="main">
				<div class="container">
					<div class="row pt-5 pb-4">

						<div class="col">

							<h1 class="mb-0">Kabar Advoqu</h1>
							<div class="divider divider-primary divider-small mb-4">
								<hr class="mr-auto">
							</div>
                        <span class="advonews">
						<i class="fa fa-spin fa-spinner fa-fw"></i>
						</span>
						

						<div class="section-5 container mt-4 head mypaging">
        <br>
		<nav aria-label="Page navigation">
			<ul class="pagination pagination-lg mb-1">
			<li class="page-item"><a class="page-link" style="cursor: pointer" onclick="scrolls('left')"><i class="fa fa-arrow-left"></i></a></li>
			<li class="page-item"><a class="page-link pages active" style="cursor: pointer">1</a></li>
			<li class="page-item"><a class="page-link pages" style="cursor: pointer">2</a></li>
			<li class="page-item"><a class="page-link pages" style="cursor: pointer">3</a></li>
			<li class="page-item"><a class="page-link pages" style="cursor: pointer">4</a></li>
			<li class="page-item"><a class="page-link pages" style="cursor: pointer">5</a></li>
			<li class="page-item"><a class="page-link next-page" style="cursor: pointer"><i class="fa fa-arrow-right" onclick="scrolls('right')"></i></a></li>
									</ul>
								</nav>
							</div>
							 
						</div>

					</div>

				</div>
			</div>


     <script>



//paging area
        

function scrolls(pos){
            let pages = document.querySelectorAll(".pages");
            let ult_l = parseInt(pages[0].innerHTML);
            let ult_r = parseInt(pages[pages.length - 1].innerHTML);
            if(pos == "right"){
                let lefty = 0;
            for(let i = 0; i < pages.length; i++){
                let cur = ult_r + (i + 1);
                if(i == 0){
                    lefty = cur;
                }
                pages[i].setAttribute("onclick","load_news(" + ((cur - 1) * 25) + ");laster(" + cur + ")");
                pages[i].innerHTML = cur;

                if(pages[i].innerHTML == lefty){
                        pages[i].classList.add("active");
                    } else {
                        pages[i].classList.remove("active");
                    }
            }
            load_news(((lefty - 1) * 25));
            } else {
                let righty = 0;
            for(let i = 0; i < pages.length; i++){
                let curs = (ult_l - 5) + 0;
                let cur = (curs < 1)? (i + 1) : (ult_l - 5) + i;
                if(i == 4){
                    righty = (curs < 1)? 0 : cur;
                }
                pages[i].setAttribute("onclick","load_news(" + ((cur - 1) * 25) + ");laster(" + cur + ")");
                pages[i].innerHTML = cur;

                if(pages[i].innerHTML == righty){
                        pages[i].classList.add("active");
                    } else {
                        pages[i].classList.remove("active");
                    }
            } 
            let res = (righty == 0)? 0 : ((righty - 1) * 25);
            load_news(res);
            }
            
        }

        function laster(val){
            let elems = document.querySelectorAll(".pages");

             
            for(let i in elems){
                if(typeof elems[i].style != "undefined"){
                     if(elems[i].innerHTML == val){
                        elems[i].classList.add("active");
                    } else {
                        elems[i].classList.remove("active");
                    }
                }
            }
        
        }

        function paging(){

            let pages = document.querySelectorAll(".pages");
            for(let i = 0; i < pages.length; i++){
                let cur = parseInt(pages[i].innerHTML);
                if(cur == 1){
                    pages[i].setAttribute("onclick","load_news(0); laster(1)");
                    pages[i].classList.add("active");
                } else {
                    pages[i].setAttribute("onclick","load_news(" + ((cur - 1) * 25) + ");laster(" + cur + ")");
                    pages[i].classList.remove("active");
                }

            }
            
        }

        paging();

		///paging area



         
function excerpt(str){
    let regex = /(<([^>]+)>)/gi;
    let set = str.replace(regex,""); 
    return (set.substring(0,25) + "...");
  }
         async function load_news(lim){
			 document.querySelector(".advonews").innerHTML = "<i class='fa fa-spin fa-spinner fa-fw'></i>";
           let dt = await fetch("<?php echo $url; ?>/sys/readed?table=news&limit=" + lim);
	        let dta = await dt.json();
	        if(dt.ok){ 
		let k = "";
		for(let i in dta.rows){
			let aut = await fetch("<?php echo $url; ?>/auth/rbac?token=" +  dta.rows[i].author );
     		 let auth = await aut.json();
			let tgl = new Intl.DateTimeFormat("id-ID",{ dateStyle: 'full', timeStyle: 'short' }).format(new Date(dta.rows[i].updated));
			let ext = /\.jpg|png|gif|jpeg|JPG|JPEG|PNG|GIF/g;  
			let gambar = ((dta.rows[i].img).match(ext) != null)? "<?php echo $url; ?>/public/uploaded/" + dta.rows[i].img  : "<?php echo $url; ?>/public/back/img/advoqu_logo2.png";
            k += "	<div class='row mt-4'> 	<div class='col'> 	<span class='thumb-info thumb-info-side-image thumb-info-no-zoom border mt-4'> 	<span class='thumb-info-side-image-wrapper p-0 d-none d-md-block'> 	<a title='' href='<?php echo $url; ?>/kabaradvoqu/" + dta.rows[i].slug + "'> 	<img src='" + gambar + "' class='img-fluid' alt='' style='width: 195px;'> 	</a>	</span> <span class='thumb-info-caption'> 	<span class='thumb-info-caption-text py-0 px-4'>  	<h2 class='mb-3 mt-2'><a title='' class='text-dark' href='<?php echo $url; ?>/kabaradvoqu/" + dta.rows[i].slug + "'>" + dta.rows[i].title  + "</a></h2> 	<span class='post-meta'> 	<span>" + tgl + "| <a href='#'>" + auth.rows[0].fullname  + "</a></span> 	</span> 	<p class='text-3 px-0 px-md-3'>" + excerpt(dta.rows[i].content)  + "</p> 	<a class='mt-3' href='<?php echo $url; ?>/kabaradvoqu/" + dta.rows[i].slug + "'>Read More <i class='fas fa-long-arrow-alt-right'></i></a>	</span> 	</span> 	</span> 	</div> </div>";
		}
		document.querySelector(".advonews").innerHTML = (k == "")? "tidak ada kabar lagi" : k;
	}
         }

         load_news(0);
     </script>       
    
    
    <?php break;
    default: ?>

<div role="main" class="main">
				<div class="container">
					<div class="row pt-5">

						<div class="col">

							<div class="blog-posts single-post">

								<article class="post post-large blog-single-post">

									<div class="post-date">
										<span class="day"></span>
										<span class="month"></span>
									</div>

									<div class="post-content">

										<h1 class="title"></h1>

										<div class="divider divider-primary divider-small mb-4">
											<hr class="mr-auto">
										</div>

										<div class="post-meta">
											<span><i class="far fa-user"></i> By <a href="#" class="auth"></a> </span>
											<span><i class="far fa-eye"></i> 99+</span>
											<span></span>
										</div>

										<img src="" class="img-fluid float-left mb-1 mt-2 mr-4 img" alt="" style="width: 195px;" >

										<span class="content"></span>
		 									<br/>
		 								<div class="col-md-9" style="clear: both; text-align: justify">DISCLAIMER <hr>
										 Artikel/Tulisan ini sudah mendapat izin dari Penulis untuk ditampilkan di web ini dan tulisan ini merupakan sarana pendidikan dan atau penyuluhan hukum bagi masyarakat, bahwa : <strong>"Seluruh informasi dan data yang ada disini hanya bersifat umum, tidak mengikat dan disediakan hanya untuk tujuan pendidikan saja, tidak dan tidak mempunyai akibat hukum apapun juga"</strong>.</div>

									

									
									</div>
								</article>

							</div>

						</div>

					</div>

				</div>
			</div>

<script>
      async function load_news(){
          let fd = new FormData();
          fd.append("slug","<?php echo $titles; ?>");
           let dt = await fetch("<?php echo $url; ?>/sys/reads?table=news",{
               method: "POST",
               body: fd
           });
	        let dta = await dt.json();
	        if(dt.ok){
		
        let tgl = new Date(dta.rows[0].updated);
        document.querySelector(".day").innerHTML = tgl.getDate();
        let bln = "";
        switch(tgl.getMonth()){
            case 0: bln = "Jan"; break;
            case 1: bln = "Feb"; break;
            case 2: bln = "Mar"; break;
            case 3: bln = "Apr"; break;
            case 4: bln = "Mei"; break;
            case 5: bln = "Jun"; break;
            case 6: bln = "Jul"; break;
            case 7: bln = "Ags"; break;
            case 8: bln = "Sep"; break;
            case 9: bln = "Okt"; break;
            case 10: bln = "Nov"; break;
            case 11: bln = "Des"; break;
        }
        document.querySelector(".month").innerHTML = bln;
        let aut = await fetch("<?php echo $url; ?>/auth/rbac?token=" +  dta.rows[0].author );
        let auth = await aut.json();
		document.querySelector(".auth").innerHTML = auth.rows[0].fullname;
        document.querySelector(".title").innerHTML = dta.rows[0].title;
		let ext = /\.jpg|png|gif|jpeg|JPG|JPEG|PNG|GIF/g;  
		let gambar = ((dta.rows[0].img).match(ext) != null)? "<?php echo $url; ?>/public/uploaded/" + dta.rows[0].img  : "<?php echo $url; ?>/public/back/img/advoqu_logo2.png";
        document.querySelector(".img").src = gambar;
		document.querySelector(".content").innerHTML = dta.rows[0].content;
	}
         }

         load_news();
    </script>

    <?php break;
}


?>
 


<?php require "footer.php"; ?>
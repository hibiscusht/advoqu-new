<?php 
$pg = "consult";
require "public/setup.php";  
require "header.php"; 
switch($titles){
    case "home": ?>

<div role="main" class="main">
				<div class="container">
<div class="row pt-5">
<div class="col-lg-12 mb-4 mb-lg-0">							
							
<span id="faq" style="display: none">

								<div class="card card-default">
									<div class="card-header bg-color-primary" id="collapsePrimaryHeading{num}">
										<h4 class="card-title m-0">
											<a class="accordion-toggle text-color-light" data-toggle="collapse" data-target="#collapsePrimary{num}" aria-expanded="true" aria-controls="collapsePrimary{num}">
												{tanya}
											</a>
										</h4>
									</div>
									<div id="collapsePrimary{num}" class="collapse" aria-labelledby="collapsePrimaryHeading{num}" data-parent="#accordionPrimary">
										<div class="card-body">
											<p class="mb-0">{jawab} <a href="{link}"><span style="color: #007bff"><i class="fa fa-external-link-square-alt"></i></span></a></p>
										
										</div>
									</div>
                                </div>
</span>                            
<div class="accordion" id="accordionPrimary"></div> <br>
<?php if($this->input->get("token") == ""){ ?>

	<h3>	<a href="<?php echo $url; ?>/main/login?status=y"><i class="fa fa-arrow-circle-right"></i> Login untuk melihat lebih banyak pertanyaan </a> </h3>
						
<?php } ?></div>
    </div>
    </div>
    </div>
<?php
        break;
    default: ?>
	<style>
	#jdl {
		border-radius: 5px !important
	}
	#jdl p {
		color: white;
		font-weight: bold;
		margin: auto;
	}
	#isi p {
		color: black
	}
	</style>
<div class="container py-2">
	<div class="row">
	<div class="col" style="padding-top: 5%">
	<div class="row">
	 <div class="col pb-3">
	 <div class="alert alert-dark" id="jdl" style="font-size: 1.5em; line-height: 2;"></div>
	 </div>
	</div>
	<div class="row">
	<div class="col pb-3">
	<div class="alert alert-info" id="isi" style="font-size: 1em;"></div>
	</div>
	</div>
	</div>
       </div>
	   </div>

<link rel="stylesheet" href="<?php echo $frontbase; ?>/chat/chat.css?v=<?php echo rand(1,100000); ?>">
<script src="<?php echo $frontbase; ?>/chat/chat.js?v=<?php echo rand(1,100000); ?>"></script>
<?php if($this->input->get("token") == ""){} else { ?>
	 <center>  <button class="btn btn-modern btn-primary"  data-toggle="modal" data-target="#largeModal" onclick="unchat()">
	   <i class="fa fa-comment"></i>	Kontak Advomin
									</button>
</center> <?php } ?>
<div class="modal fade" id="largeModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
			<h4 class="modal-title" id="largeModalLabel"><i class="fa fa-comment"></i> Kontak Advomin</h4>
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			<div class="modal-body">
			
			
			<div class="col-lg-6 col-sm-offset-4 frame" style="margin: auto">
            <ul id="chat-body"></ul>
            <div>
                <div class="msj-rta macro">                        
                    <div class="text text-r" style="background:whitesmoke !important">
                        <input class="mytext" style="width: 100%" placeholder="Type a message" id="txt"/>
                    </div> 

                </div>
                <div style="padding: 5px;background: white;margin-top: 5px;margin-left: 5px; border-radius: 5px; cursor: pointer" onclick="submits()">
                    <span style="margin-top: 25%; color: #0088cc" class="fa fa-fw fa-paper-plane fa-2x"></span>
                </div>                
            </div>
        </div>       
			
			
			
			</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
												</div>
											</div>
										</div>
									</div>


	   <?php break;    
}

?>

<script>
  
function unchat(){
	resetChat();
insertChat("you", "Selamat Datang di Advoqu", 0);  
}

function submits(){
	insertChat("me", "hello world again", 0);
	insertChat("yout", "yes hello world again", 0);
}


async function load_article(){
	let token = "<?php echo $this->input->get("token"); ?>";
	let slug = "<?php echo $titles; ?>"; 
		let fd = new FormData();
	fd.append("slug",slug);
	let dt = await fetch("<?php echo $url; ?>/sys/reads?table=faq",{
		method: "POST",
		body: fd
	});
	let dta = await dt.json();
	if(dt.ok){
		document.querySelector("#jdl").innerHTML =  dta.rows[0].question;
		document.querySelector("#isi").innerHTML =  dta.rows[0].answer;
	} 
}

load_article();


async function load_curhat(){
	document.querySelector("#accordionPrimary").innerHTML = "<i class='fa fa-spin fa-spinner fa-fw'></i> memuat curhat...";
	let tok = "<?php echo $this->input->get("token"); ?>";
	let lim = (tok == "")? "" : "&limit=25";
	let tkn = (tok == "")? "" : "?token=" + tok;
	let c = await fetch("<?php echo $url; ?>/sys/read?table=faq" + lim);
	let d = await c.json();
	if(c.ok){
		let k = "";
		let u = "";
		for(let i in d.rows){
		 k = (document.querySelector("#faq").innerHTML).replace(/{num}/g,i);
		 k = k.replace(/{tanya}/g,d.rows[i].question);
		 k = k.replace(/{jawab}/g,(d.rows[i].answer).substring(0,25) + "...");
		 k = k.replace(/{link}/g,"<?php echo $url; ?>/curhatkasus/" + d.rows[i].slug + tkn);
		 u += k;
		}
		document.querySelector("#accordionPrimary").innerHTML = u;
	}
}

<?php if($titles != "home"){} else { ?> load_curhat(); <?php } ?>
</script>
      
<?php require "footer.php"; ?>
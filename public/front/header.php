<?php

$active1 = "";
$active2 = "";
$active3 = "";
$active4 = "";
$active5 = "";
$active6 = "";
$active7 = "";
$active8 = "";
$active9 = "";

switch($pg){
    case "login": $active7 = "active"; break;
	case "home": $active1 = "active"; break;
	case "about": $active2 = "active"; break;
	case "consult": $active8 = "active"; break;
	case "news": $active3 = "active"; break;
	case "mitra": $active9 = "active"; break;
}


$tkn = $this->input->get("token");
$tok = ($tkn == "")? "" : "?token=$tkn";

?>
<!DOCTYPE html>
<html data-style-switcher-options="{'borderRadius': '0', 'changeLogo': false, 'colorPrimary': '#CFA968', 'colorSecondary': '#EAD9BB', 'colorTertiary': '#775926', 'colorQuaternary': '#313234'}">
	
<!-- Mirrored from portotheme.com/html/porto/8.1.0/demo-law-firm.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 24 Nov 2020 17:00:52 GMT -->
<head>

		<!-- Basic -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">	

		<title>R Subekti Law Office | ADVOQU</title>	

		<meta property="og:title" content="Advoqu" />
		<meta property="og:description" content="Advoqu | Layanan Hukum dalam Genggaman" />
		<meta property="og:image" content="<?php echo $backbase; ?>/img/advoqu_logo.png" />

		<meta name="theme-color" content="#2265c9">

		<!-- Favicon -->
		<link rel="shortcut icon" href="<?php echo $backbase; ?>/img/advicon.ico" type="image/x-icon" />
		<link rel="apple-touch-icon" href="<?php echo $backbase; ?>/img/advicon.ico">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

		<!-- Web Fonts  -->		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light&amp;display=swap" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="<?php echo $frontbase; ?>/vendor/bootstrap/css/bootstrap.min.css">		<link rel="stylesheet" href="<?php echo $frontbase; ?>/vendor/fontawesome-free/css/all.min.css">		<link rel="stylesheet" href="<?php echo $frontbase; ?>/vendor/animate/animate.compat.css">		<link rel="stylesheet" href="<?php echo $frontbase; ?>/vendor/simple-line-icons/css/simple-line-icons.min.css">		<link rel="stylesheet" href="<?php echo $frontbase; ?>/vendor/owl.carousel/assets/owl.carousel.min.css">		<link rel="stylesheet" href="<?php echo $frontbase; ?>/vendor/owl.carousel/assets/owl.theme.default.min.css">		<link rel="stylesheet" href="<?php echo $frontbase; ?>/vendor/magnific-popup/magnific-popup.min.css">

		<script src="<?php echo $frontbase; ?>/vendor/jquery/jquery.min.js"></script>	

		<!-- Theme CSS -->
		<link rel="stylesheet" href="<?php echo $frontbase; ?>/css/theme.css">
		<link rel="stylesheet" href="<?php echo $frontbase; ?>/css/theme-elements.css">
		<link rel="stylesheet" href="<?php echo $frontbase; ?>/css/theme-blog.css">
		<link rel="stylesheet" href="<?php echo $frontbase; ?>/css/theme-shop.css">

		<!-- Current Page CSS -->
		<link rel="stylesheet" href="<?php echo $frontbase; ?>/vendor/rs-plugin/css/settings.css">
		<link rel="stylesheet" href="<?php echo $frontbase; ?>/vendor/rs-plugin/css/layers.css">
		<link rel="stylesheet" href="<?php echo $frontbase; ?>/vendor/rs-plugin/css/navigation.css">
		
		<!-- Demo CSS -->
		<link rel="stylesheet" href="<?php echo $frontbase; ?>/css/demos/demo-law-firm.css">

		<!-- Skin CSS -->
		<link rel="stylesheet" href="<?php echo $frontbase; ?>/css/skins/skin-law-firm.css?v=<?php echo rand(1,1000); ?>">		<script src="master/style-switcher/style.switcher.localstorage.js"></script> 

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="<?php echo $frontbase; ?>/css/custom.css">

		<!-- Head Libs -->
		<script src="<?php echo $frontbase; ?>/vendor/modernizr/modernizr.min.js"></script>


<style>
.tparrows {
	opacity: 0.1
}
.tparrows:hover {
	opacity: 1
}
</style>

	</head>
	<body>

		<div class="body">
			<header id="header" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyStartAt': 98, 'stickySetTop': '-98px', 'stickyChangeLogo': true}">
				<div class="header-body border-color-primary border-top-0 box-shadow-none">
					<div class="header-container container z-index-2">
						<div class="header-row">
							<div class="header-column">
								<div class="header-row">
									<div class="header-logo">
										<a href="<?php echo $url; ?>">
											<img alt="Porto" width="54" height="54" data-sticky-width="37" data-sticky-height="37" data-sticky-top="86" src="<?php echo $backbase; ?>/img/advoqu_logo.png">
										</a>
									</div>
								</div>
							</div>
							<div class="header-column justify-content-end">
								<div class="header-row h-100">
									<ul class="header-extra-info d-flex h-100 align-items-center">
										<li class="align-items-center h-100 py-4 header-border-right pr-4 d-none d-md-inline-flex">
											<div class="header-extra-info-text h-100 py-2">
												<div class="feature-box feature-box-style-2 align-items-center">
													<div class="feature-box-icon">
														<i class="far fa-envelope text-7 p-relative"></i>
													</div>
													<div class="feature-box-info pl-1">
														<label>SEND US AN EMAIL</label>
														<strong><a href="mailto:advoqu2021@gmail.com">advoqu2021@gmail.com</a></strong>
													</div>
												</div>
											</div>
										</li>
										<li class="align-items-center h-100 py-4">
											<div class="header-extra-info-text h-100 py-2">
												<div class="feature-box feature-box-style-2 align-items-center">
													<div class="feature-box-icon">
														<i class="fab fa-whatsapp text-7 p-relative"></i>
													</div>
													<div class="feature-box-info pl-1">
														<label>HUBUNGI KAMI</label>
														<strong><a href="https://wa.me/6287726556110" target="_BLANK">087726556110</a></strong>
													</div>
												</div>
											</div>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="header-nav-bar bg-primary" data-sticky-header-style="{'minResolution': 991}" data-sticky-header-style-active="{'background-color': 'transparent'}" data-sticky-header-style-deactive="{'background-color': '#0088cc'}">
						<div class="container">
							<div class="header-row">
								<div class="header-column">
									<div class="header-row">
										<div class="header-column">
											<div class="header-nav header-nav-stripe header-nav-divisor header-nav-force-light-text justify-content-start" data-sticky-header-style="{'minResolution': 991}" data-sticky-header-style-active="{'margin-left': '150px'}" data-sticky-header-style-deactive="{'margin-left': '0'}">
												<div class="header-nav-main header-nav-main-square header-nav-main-effect-1 header-nav-main-sub-effect-1">
													<nav class="collapse">
														<ul class="nav nav-pills" id="mainNav">
															<li class="dropdown dropdown-full-color dropdown-light">
																<a class="dropdown-item <?php echo $active1; ?>" href="<?php echo $url; ?><?php echo $tok; ?>">
																	Home
																</a>
															</li>
                                                            <li class="dropdown dropdown-full-color dropdown-light
                                                            ">
                                                                <a class="dropdown-item
                                                                <?php echo $active2; ?>" href="<?php echo $url; ?>/main/about<?php echo $tok; ?>">
																	Siapa Kami
																</a>
															</li>
														 	<li class="dropdown dropdown-full-color dropdown-light">
                                                                <a class="dropdown-item
                                                                <?php echo $active3; ?>" href="<?php echo $url; ?>/kabaradvoqu/home<?php echo $tok; ?>">
																	Dari Kami
																</a>
															</li>
														<!--	<li class="dropdown dropdown-full-color dropdown-light">
                                                                <a class="dropdown-item
                                                                <?php echo $active4; ?>" href="demo-law-firm-practice-areas.html">
																	Practice Areas
																</a>
															</li>										
															<li class="dropdown dropdown-full-color dropdown-light">
                                                                <a class="dropdown-item
                                                                <?php echo $active5; ?>" href="demo-law-firm-news.html">
																	News
																</a>
															</li> -->
															<li class="dropdown dropdown-full-color dropdown-light">
                                                                <a class="dropdown-item
                                                                <?php echo $active6; ?>" href="demo-law-firm-contact-us.html">
																Ketentuan Pengguna
																</a>
															</li>
															<li class="dropdown dropdown-full-color dropdown-light">
                                                                <a class="dropdown-item
                                                                <?php echo $active9; ?>" href="<?php echo $url; ?>/advoshare/home<?php echo $tok; ?>">
															   Mitra Advoqu
																</a>
															</li>
															<li class="dropdown dropdown-full-color dropdown-light">
                                                                <a class="dropdown-item
                                                                <?php echo $active8; ?>" href="<?php echo $url; ?>/curhatkasus/home<?php echo $tok; ?>">
																Curhat Kasus
																</a>
															</li>
				<?php 
				if($tkn == ""){
				?>											
															<li class="dropdown dropdown-full-color dropdown-light">
                                                                <a class="dropdown-item
                                                                <?php echo $active7; ?>" href="<?php echo $url; ?>/main/login">
																	Login
																</a>
															</li>
<?php } else { ?>
	<li class="dropdown dropdown-full-color dropdown-light">
     <a class="dropdown-item" href="<?php echo $url; ?>/admin/index?token=<?php echo $tkn; ?>">
				<i class="fa fa-link-external"></i> Profile
																</a>
															</li>
<?php } ?>
														</ul>
													</nav>
												</div>
											</div>
										</div>
										<div class="header-column">
											<div class="header-row justify-content-end">
												<div class="header-nav-features header-nav-features-no-border w-75">
												 
														<div class="simple-search input-group w-100">
															<input class="form-control border-0 border-radius-0 text-2" id="headerSearch" name="q" type="search" value="" placeholder="cari dengan Advofind..." onkeydown="find(this.id)" >
															<span class="input-group-append bg-light border-0 border-radius-0">
																<button class="btn" type="button" onclick="find2('headerSearch')">
																	<i class="fa fa-search header-nav-top-icon"></i>
																</button>
															</span>
														</div>
													 
												</div>
											</div>
										</div>
										<button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main nav">
											<i class="fas fa-bars"></i>
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</header>


<script>
function find(id){ 
	let q = document.querySelector("#" + id).value;
	if(event.key == "Enter"){
		location.href = "<?php echo $url; ?>/kabaradvoqu/search?q=" + q.replace(/\s/gi,"+");
	}
}

function find2(id){ 
	console.log(document.querySelector("#" + id));
	let q = document.querySelector("#" + id).value; 
		location.href = "<?php echo $url; ?>/kabaradvoqu/search?q=" + q.replace(/\s/gi,"+"); 
}
</script>			
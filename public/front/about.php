<?php 
$pg = "about";
require "public/setup.php";  
require "header.php"; ?>
<div role="main" class="main">
				<div class="container">
					<div class="row pt-5">
						<div class="col-lg-7">
							<h1 class="mb-0">Advoqu Melayani Anda</h1>
							<div class="divider divider-primary divider-small mb-4">
								<hr class="mr-auto">
							</div>
							<p class="lead mb-4 mt-4">Advoqu bergerak sebagai platform penyedia jasa legal secara online yang membantu para pencari keadilan dan juga masyarakat yang sedang menghadapi berbagai jenis permasalahan hukum, mulai dari kredit bermasalah, perpajakan, hak kekayaan intelektual, perizinan usaha, pendirian perusahaan, perkawinan, hutang piutang, waris, sengketa tanah, bisnis, kontrak usaha, pidana, ketenagakerjaan dan lainnya. Kami membantu Anda mendapatkan panduan dan pencerahan hukum dari mitra kantor hukum kami yang kredibel, dengan harga yang terukur dan transparan. 

</p>

							<img width="205" class="img-fluid float-left mr-4 mb-4 mt-1" alt="" src="img/office/our-office-8.jpg">

							<p>Jangan ragu lagi, pastikan Anda saat ini memiliki kantor  hukum pribadi, karena Advoqu telah hadir, mewujudkan Layanan Hukum Dalam Genggaman.</p>

							<p><strong>Layanan Advoqu:</strong>
<ul style="list-style-type: none; padding-left: 1%">
    <li><i class="fa fa-phone fa-1x" style="font-weight: bold; color: #2265c9"></i>&nbsp;&nbsp;&nbsp;Konsultasi Hukum via Telepon </li>
    <li><i class="fa fa-users fa-1x" style="font-weight: bold; color: #2265c9"></i>&nbsp;&nbsp;&nbsp;Konsultasi Tatap Muka </li>
    <li><i class="fa fa-hammer fa-1x" style="font-weight: bold; color: #2265c9"></i>&nbsp;&nbsp;&nbsp;Pembuatan Legal Opinion dan Review Dokumen Hukum </li>
    <li><i class="fa fa-balance-scale-right fa-1x" style="font-weight: bold; color: #2265c9"></i>&nbsp;&nbsp;&nbsp;Pendampingan Hukum </li>

    </p>

							<h2 class="mt-5 mb-3">Hadirnya Advoqu </h2>

							<p>Law Office R. Subekti & Partners, sebagai salah satu kantor hukum  terpercaya di Indonesia, melihat adanya kebutuhan yang tinggi dari masyarakat Indonesia untuk mendapatkan layanan konsultasi hukum dari para advokat dan konsultan hukum di Indonesia secara mudah, cepat dan terpercaya. Oleh karena itu sejak 2020, Law Office R. Subekti & Partners meminta CV. Rahilah International ( pengembang aplikasi platform digital dan konsultan keamanan informasi )  untuk mendirikan Advoqu.
Advoqu diharapkan mampu menjadi media atau platform bagi masyarakat dan mitra kantor hukum untuk bisa saling bertemu, berkomunikasi, berdiskusi, mencari solusi terbaik dalam menyelesaikan konflik atau permasalahan hukum yang sedang dihadapi dengan jauh lebih efektif dan efisien tanpa dibatasi ruang dan waktu. Kehadiran Advoqu diharapkan mampu berperan penting dalam memastikan terlayaninya hak-hak hukum masyarakat dan menjangkau masyarakat secara lebih luas.</p>

						</div>
					<!--	<div class="col-lg-4 col-lg-offset-1">

							<h4 class="mt-5 mb-0">Our Commitment</h4>
							<div class="divider divider-primary divider-small mb-4">
								<hr class="mr-auto">
							</div>

							<div class="embed-responsive embed-responsive-16by9 mb-4">
								<iframe src="http://player.vimeo.com/video/162724070?title=0&amp;byline=0&amp;portrait=0" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
							</div>

							<blockquote class="blockquote-secondary">
								<p class="text-4">"Fusce lacllentesque eget tempor tellus ellentesque pelleinia tempor malesuada."</p>
								<footer>John Doe <cite> - PARTNER</cite></footer>
							</blockquote>

							<h4 class="mt-5">Our History</h4>

							<div class="divider divider-primary divider-small mb-4">
								<hr class="mr-auto">
							</div>

							<ul class="list list-unstyled list-primary list-borders">
								<li class="pt-2 pb-2"><strong class="text-color-primary text-4">2020 - </strong> Moves its headquarters to a new building</li>
								<li class="pt-2 pb-2"><strong class="text-color-primary text-4">2014 - </strong> Porto creates its new brand</li>
								<li class="pt-2 pb-2"><strong class="text-color-primary text-4">2006 - </strong> Porto Office opens it doors in London</li>
								<li class="pt-2 pb-2"><strong class="text-color-primary text-4">2003 - </strong> Inauguration of the new office</li>
								<li class="pt-2 pb-2"><strong class="text-color-primary text-4">2001 - </strong> Porto goes into business</li>
							</ul>

						</div>-->
					</div>
				</div>
            </div>
            
<?php require "footer.php"; ?>
<?php 
$pg = "mitra";
require "public/setup.php";  
require "header.php"; ?>
<style>
.hidden {
	display: none
}
.caption {
	font-size: 3.5em;
}
@media (max-width: 500px){
	.caption {
		font-size: 1.5em;
	}
}
#map_canvas {
   height: 350px;
   width: 85%;
}
</style>
	<div role="main" class="main">
				<div class="container">

<?php 

switch($titles){
	default:

?>

					<div class="row pt-5 detail">

						<div class="col-md-9">

							<div class="row mb-4">
								<div class="col-md-4 text-center">
									<img src="img/team/team-22.jpg" class="img-fluid mb-4" alt="">
								</div>
								<div class="col-md-8">
									<small>Kantor Hukum</small>
									<h1 class="mt-0 mb-0 details"></h1>
									<p class="lead details"></p>
									<div class="divider divider-primary divider-small mb-4">
										<hr class="mr-auto">
									</div>
									<ul class="list list-icons">
										<li><i class="fas fa-phone"></i> <strong>Phone:</strong> </li>
										<li><i class="far fa-envelope"></i> <strong>Email:</strong>  <span class="details"></span></li>
										<li><i class="far fa-envelope"></i> <strong>Alamat:</strong>  <span class="details"></span></li>
									</ul>
									<hr>
									<span class="thumb-info-social-icons p-0 mt-0">
										<a href="http://www.facebook.com/" target="_blank"><i class="fab fa-facebook-f"></i><span>Facebook</span></a>
										<a href="http://www.twitter.com/"><i class="fab fa-twitter"></i><span>Twitter</span></a>
										<a href="http://www.linkedin.com/"><i class="fab fa-linkedin-in"></i><span>Linkedin</span></a>
									</span>
								</div>
							</div>
							<div class="row mt-4">
								<div class="col-md-12">
									 
								</div>
							</div>
						</div>

					<!--	<div class="col-md-3">
							<aside class="sidebar">
								<h4 class="mb-3">Contact Me</h4>
								<p>Contact me or give us a call to discover how we can help.</p>

								<div class="divider divider-primary divider-small mb-4">
									<hr class="mr-auto">
								</div>

								<form class="contact-form" action="https://portotheme.com/html/porto/8.1.0/php/contact-form.php" method="POST">
									<div class="form-row">
										<div class="form-group col">
											<label>Your name *</label>
											<input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" required>
										</div>
									</div>
									<div class="form-row">
										<div class="form-group col">
											<label>Your email address *</label>
											<input type="email" value="" data-msg-required="Please enter your email address." data-msg-email="Please enter a valid email address." maxlength="100" class="form-control" name="email" required>
										</div>
									</div>
									<div class="form-row">
										<div class="form-group col">
											<label>Subject</label>
											<input type="text" value="" data-msg-required="Please enter the subject." maxlength="100" class="form-control" name="subject" required>
										</div>
									</div>
									<div class="form-row">
										<div class="form-group col">
											<label>Message *</label>
											<textarea maxlength="5000" data-msg-required="Please enter your message." rows="3" class="form-control" name="message" required></textarea>
										</div>
									</div>
									<div class="form-row">
										<div class="form-group col">
											<input type="submit" value="Send Message" class="btn btn-lg btn-primary" data-loading-text="Loading...">

											<div class="contact-form-success alert alert-success d-none">
												Message has been sent to us.
											</div>

											<div class="contact-form-error alert alert-danger d-none">
												Error sending your message.
												<span class="mail-error-message text-1 d-block"></span>
											</div>
										</div>
									</div>
								</form>

							</aside> 
						</div> -->
					 
				</div> 

<script>
async function load_detail(){
	let id = "<?php echo $titles; ?>";
	let fd = new FormData();
	fd.append("id",id);
	let dt = await fetch("<?php echo $url; ?>/sys/reads?table=mitra",{
		method:"POST",
		body: fd
	});
	let dta = await dt.json();
	if(dt.ok){
		let fw = new FormData();
      fw.append("token",dta.rows[0].user);
      let dx = await fetch("<?php echo $url; ?>/sys/reads?table=users",{
        method: "POST",
        body: fw
      });
      let dxa = await dx.json();
	  let types = "";
      switch(dxa.rows[0].cat){
        case "1": types = "<i class='fa fa-star'></i>"; break;
        case "2": types = "<i class='fa fa-star'></i><i class='fa fa-star'></i>"; break;
        case "3": types = "<i class='fa fa-star'></i><i class='fa fa-star'></i><i class='fa fa-star'></i>"; break;
        case "4": types = "<i class='fa fa-star'></i><i class='fa fa-star'></i><i class='fa fa-star'></i><i class='fa fa-star'></i>"; break;
      }
	  
	   document.querySelectorAll(".details")[0].innerHTML = dxa.rows[0].fullname;
	   document.querySelectorAll(".details")[1].innerHTML = types;
	   document.querySelectorAll(".details")[2].innerHTML = dta.rows[0].email;
	  document.querySelectorAll(".details")[3].innerHTML = dta.rows[0].geo_loc;
	  
	}
}

load_detail();
</script>


<?php break; 
case "home": ?>
<!--------------------------------->
				<div class="row pt-5 login-mitra hidden">
				terimakasih sdh mendaftar
				</div>
<!--------------------------------->
<div class="mitra">
				<div class="row pt-5">
				<p class="caption" style="width: 100%; color: #0000ff; text-shadow: 0px 2px 4px #0b3c75; padding: 2%; background: #71aaff; text-align: center">Mari Bergabung menjadi Mitra Advoqu</p> 
				<img src="<?php echo $frontbase; ?>/img/mitra.jpg" style="margin: auto; width: 50%"/>
				</div>

				<div class="row pt-5">

				<div class="col-lg-7">
							<h1 class="mb-0">Silakan Lengkapi Isian Berikut ini</h1>
							<div class="divider divider-primary divider-small mb-4">
								<hr class="mr-auto">
							</div>
							<p class="lead mb-5 mt-4"> </p>

								
								<div class="row">
									<div class="form-group col">
										<input type="email"  placeholder="Alamat Email" class="form-control required" id="u" onblur="checkmail()" data-key="username">
									</div>
								</div>
								<div class="row">
									<div class="form-group col">
										<input type="password"  placeholder="Password" class="form-control required pass" data-key="password">
									</div>
								</div> 
								<div class="row">
									<div class="form-group col">
										<input type="email"  placeholder="Nama Kantor Hukum" class="form-control required" data-key="fullname">
									</div>
								</div>
								<div class="row">
								<div class="form-group col">
								<div id="map_canvas"><i class="fa fa-spinner fa-spin fa-fw"></i> menunggu Google Maps</div><br/>
								<input type="hidden" class="required" id="marked" data-key="geo_latlng"/>
								<input type="text" class="form-control required" id="coded" disabled placeholder="geser marker untuk memilih lokasi Kantor Hukum" data-key="geo_loc"/> </div>
								</div>
								<div class="row">
									<div class="form-group col">
										<button type="button" value="Login" class="btnx btn btn-primary btn-lg mb-5" data-loading-text="Loading..." onclick="validate()">Kirim</button>
									</div>
								</div>
						 

						</div>

						<div class="col-lg-4 col-lg-offset-1">

							 

							<h4 class="pt-4 mb-0">Ketentuan Umum Mitra Advoqu</h4>
							<div class="divider divider-primary divider-small mb-4">
								<hr class="mr-auto">
							</div>

							<ul class="list list-icons list-dark mt-4">
								<li><i class="far fa-check-square"></i> Pendaftaran akan berlaku setelah diverifikasi Admin</li>
								<li><i class="far fa-check-square"></i> Verifikasi maksimal 1 x 24 jam</li>
								<li><i class="far fa-check-square"></i> Hanya Kantor Hukum yang dapat menjadi Mitra Advoqu</li>
								<li><i class="far fa-check-square"></i> Kantor Hukum yang telah diverifikasi akan dapat memberikan layanan hukum setelah divalidasi Admin</li>
								<li><i class="far fa-check-square"></i> Kantor Hukum yang telah divalidasi hanya dapat memberikan layanan hukum pada <strong>jenis layanan hukum dan wilayah kerja</strong> yang dipilih pada waktu mendaftar pertama kali</li>
								<li><i class="far fa-check-square"></i> Ketentuan lain bisa dilihat di halaman Dashboard akun Mitra Advoqu</li>

							</ul>

						</div>

				</div>

</div>
<?php break; 
}

?>
</div> <!--container end -->
</div> <!--main end -->
<script>

		function checkmail(){
	    document.getElementById("u").style.border="solid 1px #ced4da";
        let check = document.getElementById("u").value;
        let mail = RegExp(".*@.*\..*","g");
        let chk = mail.test(check);
        if(!chk){ 
            document.getElementById("u").focus();
			document.getElementById("u").style.border="solid 1px red";
        }
    }

	 function chk(){
		let req = document.querySelectorAll(".required");
		let locked = 0;
		for(let i = 0; i < req.length; i++){
			if(req[i].value == ""){
				req[i].style.border = "1px solid red";
				locked++;
			} else {
				req[i].style.border = "1px solid #ced4da";
			}
		}
		if(locked > 0){
			return false;
		} else {
			return true;
		} 
	}


	async function validate(){
		let btn = document.querySelector(".btnx");
		btn.setAttribute("disabled",true);
		btn.innerHTML = "<i class='fa fa-spin fa-spinner fa-fw'></i> mengirim...";
		let fd = new FormData();
		let elem = document.querySelectorAll(".required");
		for(let i = 0; i < elem.length; i++){
			fd.append(elem[i].dataset.key,elem[i].value);
		}
		let status = chk();
		if(status){
			let dta = await fetch("<?php echo $url; ?>/auth/create_mitra",{
			method: "POST",
			body: fd
		});
		if(dta.ok){
		document.querySelector(".mitra").classList.add("hidden");
		document.querySelector(".login-mitra").classList.remove("hidden");
		}
		} else {
		btn.removeAttribute("disabled");
		btn.innerHTML = "Kirim";
		}
	
	}
    </script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDwqoMDt8aFQji_MDMqbsyV0j2VMJeMbRw"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            // Creating map object
            var map = new google.maps.Map(document.getElementById('map_canvas'), {
                zoom: 10,
                center: new google.maps.LatLng(-7.8183754,110.3520262),
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            // creates a draggable marker to the given coords
            var vMarker = new google.maps.Marker({
                position: new google.maps.LatLng(-7.8183754,110.3520262),
                draggable: true
            });

            // adds a listener to the marker
            // gets the coords when drag event ends
            // then updates the input with the new coords
            google.maps.event.addListener(vMarker, 'dragend', function (evt) {
                var lat = evt.latLng.lat().toFixed(6);
                var lng = evt.latLng.lng().toFixed(6);
				$("#marked").val(lat + "," + lng);

			var latlng = new google.maps.LatLng(lat, lng);
			var geocoder = new google.maps.Geocoder();
            geocoder.geocode({ 'latLng': latlng }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[1]) {
						$("#coded").val(results[1].formatted_address); 
                    }
                }
            });

                map.panTo(evt.latLng);
            });

            // centers the map on markers coords
            map.setCenter(vMarker.position);

            // adds the marker on the map
            vMarker.setMap(map);
        });

		</script>
            
<?php require "footer.php"; ?>
<?php 
$pg = "home";
require "public/setup.php";  
require "header.php"; ?>


<style>
.mitra {  
}

@media (max-width: 700px){
	.mitra { 
		height: 375px;
	}
}
</style>

			<div role="main" class="main">
				<div class="slider-container rev_slider_wrapper" style="height: 650px;">
					<div id="revolutionSlider" class="slider rev_slider manual" data-version="5.4.8">
						<ul>
							<li data-transition="fade" data-title="Advocate Team" data-thumb="<?php echo $frontbase; ?>/img/img/demos/law-firm/slides/slide-law-firm-1.jpg">

								<img src="<?php echo $frontbase; ?>/img/img/demos/law-firm/slides/slide-law-firm-1.jpg"  
									alt=""
									data-bgposition="center center" 
									data-bgfit="cover" 
									data-bgrepeat="no-repeat"
									class="rev-slidebg">

								<div class="tp-caption top-label"
									data-x="right" data-hoffset="100"
									data-y="center" data-voffset="['-95','-95','-95','-130']"
									data-start="1000"
									data-fontSize="['24','24','24','45']"
									data-lineHeight="['29','29','29','50']"
									style="z-index: 5"
									data-transform_in="y:[-300%];opacity:0;s:500;">Selamat Datang Di</div>

								<div class="tp-caption main-label"
									data-x="right" data-hoffset="100"
									data-y="center" data-voffset="['-45','-45','-45','-55']"
									data-start="1500"
									data-whitespace="nowrap"		
									data-fontSize="['62','62','62','85']"
									data-lineHeight="['67','67','67','90']"				 
									data-transform_in="y:[100%];s:500;"
									data-transform_out="opacity:0;s:500;"
									style="z-index: 5"
									data-mask_in="x:0px;y:0px;">ADVOQU</div>

								<div class="tp-caption bottom-label"
									data-x="right" data-hoffset="100"
									data-y="center" data-voffset="['5','5','5','20']"
									data-start="2000"
									data-fontSize="['20','20','25','40']"
									data-lineHeight="['25','25','30','45']"
									style="z-index: 5"
									data-transform_in="y:[100%];opacity:0;s:500;">Sistem Informasi Layanan Hukum</div>

								<a class="tp-caption btn btn-primary btn-lg"
									href="http://themeforest.net/item/porto-responsive-html5-template/4106987"
									data-x="right" data-hoffset="100"
									data-y="center" data-voffset="['80','80','80','145']"
									data-start="2500"
									data-whitespace="nowrap"
									data-fontSize="['16','16','21','45']"
									data-lineHeight="['20','20','26','50']"
									data-paddingtop="['10','10','10','20']"
									data-paddingbottom="['10','10','10','20']"	
									data-paddingleft="['25','25','25','55']" 
									data-paddingright="['25','25','25','55']"						 
									data-transform_in="y:[100%];s:500;"
									data-transform_out="opacity:0;s:500;"
									style="z-index: 5"
									data-mask_in="x:0px;y:0px;">Kami Siap Membantu</a>
								
							</li>
							<li data-transition="fade" data-title="Practice Areas" data-thumb="<?php echo $frontbase; ?>/img/img/demos/law-firm/slides/slide-law-firm-2-thumb.jpg">

								<img src="<?php echo $frontbase; ?>/img/img/demos/law-firm/slides/slide-law-firm-2.jpg"  
									alt=""
									data-bgposition="center center" 
									data-bgfit="cover" 
									data-bgrepeat="no-repeat" 
									class="rev-slidebg">

								<div class="tp-caption main-label"
									data-x="right" data-hoffset="100"
									data-y="center" data-voffset="['-215','-215','-215','-270']"
									data-start="1000"
									data-fontSize="['40','40','40','55']"
									data-lineHeight="['45','45','45','60']"
									style="z-index: 5;"
									data-transform_in="y:[-300%];opacity:0;s:500;">ADVOQU MELAYANI</div>

								<div class="tp-caption tp-caption-overlay-opacity bottom-label"
									data-x="right" data-hoffset="100"
									data-y="center" data-voffset="['-150','-150','-150','-185']"
									data-start="1500"
									data-fontSize="['20','20','20','40']"
									data-lineHeight="['25','25','25','45']"
									data-transform_in="x:[-100%];opacity:0;s:500;"><i class="fas fa-check"></i>Layanan Individu</div>

								<div class="tp-caption tp-caption-overlay-opacity bottom-label"
									data-x="right" data-hoffset="100"
									data-y="center" data-voffset="['-100','-100','-100','-110']"
									data-start="1800"
									data-fontSize="['20','20','20','40']"
									data-lineHeight="['25','25','25','45']"
									data-transform_in="x:[-100%];opacity:0;s:500;"><i class="fas fa-check"></i>Layanan Perusahaan</div>

								<div class="tp-caption tp-caption-overlay-opacity bottom-label"
									data-x="right" data-hoffset="100"
									data-y="center" data-voffset="['-50','-50','-50','-40']"
									data-start="2100"
									data-fontSize="['20','20','20','40']"
									data-lineHeight="['25','25','25','45']"
									data-transform_in="x:[-100%];opacity:0;s:500;"><i class="fas fa-check"></i>Layanan Kelompok Masyarakat</div>

								<div class="tp-caption tp-caption-overlay-opacity bottom-label"
									data-x="right" data-hoffset="100"
									data-y="center" data-voffset="['0','0','0','35']"
									data-start="2400"
									data-fontSize="['20','20','20','40']"
									data-lineHeight="['25','25','25','45']"
									data-transform_in="x:[-100%];opacity:0;s:500;"><i class="fas fa-check"></i>Layanan Komunitas</div>

								<div class="tp-caption tp-caption-overlay-opacity bottom-label"
									data-x="right" data-hoffset="100"
									data-y="center" data-voffset="['50','50','50','105']"
									data-start="2700"
									data-fontSize="['20','20','20','40']"
									data-lineHeight="['25','25','25','45']"
									data-transform_in="x:[-100%];opacity:0;s:500;"><i class="fas fa-check"></i>Layanan Badan Hukum Non Profit</div>

								<div class="tp-caption tp-caption-overlay-opacity bottom-label"
									data-x="right" data-hoffset="100"
									data-y="center" data-voffset="['100','100','100','180']"
									data-start="3000"
									data-fontSize="['20','20','20','40']"
									data-lineHeight="['25','25','25','45']"
									data-transform_in="x:[-100%];opacity:0;s:500;"><i class="fas fa-check"></i>Layanan Lainnya</div>

								<a class="tp-caption btn btn-primary btn-lg"
									data-hash
									data-hash-offset="85"
									href="#practice-areas"
									data-x="right" data-hoffset="100"
									data-y="center" data-voffset="['175','175','175','290']"
									data-start="3500"
									data-whitespace="nowrap"		
									data-fontSize="['16','16','21','45']"
									data-lineHeight="['20','20','26','50']"
									data-paddingtop="['10','10','10','16']"
									data-paddingbottom="['10','10','10','16']"	
									data-paddingleft="['25','25','25','35']" 
									data-paddingright="['25','25','25','35']"
									data-transform_in="y:[100%];s:500;"
									data-transform_out="opacity:0;s:500;"
									style="z-index: 5"
									data-mask_in="x:0px;y:0px;">Lebih Lanjut <i class="fas fa-long-arrow-alt-right"></i></a>

							</li>
							<li data-transition="fade" data-title="Welcome to Porto" data-thumb="<?php echo $frontbase; ?>/img/img/demos/law-firm/slides/slide-law-firm-3.jpg">

								<img src="<?php echo $frontbase; ?>/img/img/demos/law-firm/slides/slide-law-firm-3.jpg"  
									alt=""
									data-bgposition="center center" 
									data-bgfit="cover" 
									data-bgrepeat="no-repeat" 
									class="rev-slidebg">

								<div class="tp-caption top-label"
									data-x="left" data-hoffset="100"
									data-y="center" data-voffset="['-95','-95','-95','-130']"
									data-start="1000"
									data-fontSize="['24','24','24','45']"
									data-lineHeight="['29','29','29','50']"
									style="z-index: 5"
									data-transform_in="y:[-300%];opacity:0;s:500;">ADVOQU</div>

								<div class="tp-caption main-label"
									data-x="left" data-hoffset="100"
									data-y="center" data-voffset="['-45','-45','-45','-55']"
									data-start="1500"
									data-whitespace="nowrap"		
									data-fontSize="['62','62','62','85']"
									data-lineHeight="['67','67','67','90']"				 
									data-transform_in="y:[100%];s:500;"
									data-transform_out="opacity:0;s:500;"
									style="z-index: 5"
									data-mask_in="x:0px;y:0px;">Layanan Hukum</div>

								<div class="tp-caption bottom-label"
									data-x="left" data-hoffset="100"
									data-y="center" data-voffset="['5','5','5','20']"
									data-start="2000"
									data-fontSize="['20','20','25','40']"
									data-lineHeight="['25','25','30','45']"
									style="z-index: 5"
									data-transform_in="y:[100%];opacity:0;s:500;">Dalam Genggaman</div>

								<a class="tp-caption btn btn-primary btn-lg"
									href="<?php echo $url; ?>/main/register"
									data-x="left" data-hoffset="100"
									data-y="center" data-voffset="['80','80','80','145']"
									data-start="2500"
									data-whitespace="nowrap"
									data-fontSize="['16','16','21','45']"
									data-lineHeight="['20','20','26','50']"
									data-paddingtop="['10','10','10','20']"
									data-paddingbottom="['10','10','10','20']"	
									data-paddingleft="['25','25','25','55']" 
									data-paddingright="['25','25','25','55']"						 
									data-transform_in="y:[100%];s:500;"
									data-transform_out="opacity:0;s:500;"
									style="z-index: 5"
									data-mask_in="x:0px;y:0px;">Daftar Sekarang</a>

							</li>
						</ul>
					</div>
				</div>
				<section class="section section-default section-no-border mt-0">
					<div class="container pt-3 pb-4">
						<div class="row justify-content-around">
							<div class="col-lg-7 mb-4 mb-lg-0">
								<h2 class="mb-0">Advoqu Melayani Anda</h2>
								<div class="divider divider-primary divider-small mb-4">
									<hr class="mr-auto">
								</div>
								<p class="mt-4">Advoqu bergerak sebagai platform penyedia jasa legal secara online yang membantu para pencari keadilan dan juga masyarakat yang sedang menghadapi berbagai jenis permasalahan hukum...</p>

								<a class="mt-3" href="<?php echo $url; ?>/main/about">Lebih Lanjut <i class="fas fa-long-arrow-alt-right"></i></a>
							</div>
							<div class="col-lg-4">
								<h4 class="mb-0">Layanan Hukum Dalam Genggaman</h4>
								<div class="divider divider-primary divider-small mb-4">
									<hr class="mr-auto">
								</div>
								<p class="mt-4 mb-0">Semboyan ADVOQU menjanjikan layanan hukum yang transparan dan akuntabel serta dapat diakses kapan saja dan di mana saja</p>
							</div>
						</div>
					</div>
				</section>
<?php if($tkn != ""){ ?>
				<section class="section section-background section-default section-no-border mt-0" style="background-image: url(<?php echo $frontbase; ?>/img/img/demos/law-firm/contact/contact-background.jpg); background-position: 50% 100%; background-size: cover;">
					<div class="container">
						<div class="row justify-content-end">
							<div class="col-lg-6">
								<h2 class="mt-5 mb-0">Request a Free Consultation</h2>
								<p>Consult with our experienced team for complete solutions to your legal issues.</p>
								<div class="divider divider-primary divider-small mb-4">
									<hr class="mr-auto">
								</div>
								<form class="contact-form" action="https://portotheme.com/html/porto/8.1.0/php/contact-form.php" method="POST">
									<div class="form-row">
										<div class="form-group col-sm-6">
											<input type="text" value="" placeholder="Your name" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" required>
										</div>
										<div class="form-group col-sm-6">
											<input type="email" value="" placeholder="Your email address *" data-msg-required="Please enter your email address." data-msg-email="Please enter a valid email address." maxlength="100" class="form-control" name="email" required>
										</div>
									</div>
									<div class="form-row">
										<div class="form-group col">
											<input type="text" value="" placeholder="Subject" data-msg-required="Please enter the subject." maxlength="100" class="form-control" name="subject" required>
										</div>
									</div>
									<div class="form-row">
										<div class="form-group col">
											<textarea maxlength="5000" placeholder="Message *" data-msg-required="Please enter your message." rows="3" class="form-control" name="message" required></textarea>
										</div>
									</div>
									<div class="form-row">
										<div class="form-group col">
											<input type="submit" value="Send Message" class="btn btn-primary mb-5" data-loading-text="Loading...">

											<div class="contact-form-success alert alert-success d-none">
												Message has been sent to us.
											</div>

											<div class="contact-form-error alert alert-danger d-none">
												Error sending your message.
												<span class="mail-error-message text-1 d-block"></span>
											</div>
										</div>
									</div>
								</form>

							</div>
						</div>
					</div>
				</section> 

				<?php } ?>


				<div class="container" id="practice-areas">
					<div class="row">
						<div class="col-lg-12 text-center">
							<h2 class="mt-4 mb-0">Advoqu Melayani</h2>
							<div class="divider divider-primary divider-small divider-small-center mb-4">
								<hr>
							</div>
						</div>
					</div>

					<div class="row mt-4 svc-1">
					 
					</div>

					<div class="row mt-lg-3 mb-4 svc-2">
						 
					</div>
				</div>

				<div class="container-fluid">
					<div class="row">
						<div class="col-lg-6 p-0">
							<section class="section section-primary match-height border-0" style="background-image: url(<?php echo $frontbase; ?>/img/fancy2.jpg);">
								<div class="row justify-content-end ml-lg-5">
									<div class="col-half-section col-half-section-right">
										<h2 class="mb-0">Testimonials</h2>
										<div class="divider divider-light divider-small mb-4">
											<hr class="mr-auto">
										</div>

										<div class="owl-carousel owl-theme mb-0" data-plugin-options="{'items': 1, 'margin': 10, 'loop': false, 'nav': false, 'dots': true}" id="testi">
											
										</div> 

									</div>
								</div>
							</section>
						</div>
						<div class="col-lg-6 p-0 visible-md visible-lg">
							<section class="parallax section section-parallax match-height" data-plugin-parallax data-plugin-options="{'speed': 1.5, 'horizontalPosition': '100%'}" data-image-src="<?php echo $frontbase; ?>/img/img/demos/law-firm/parallax/parallax-law-firm.jpg" style="min-height: 450px;">
							</section>
						</div>
					</div>

					<div class="container">
						<div class="row">
							<div class="col-lg-12 text-center">
								<h2 class="mt-4 mb-0">Mitra Advoqu</h2>
								<div class="divider divider-primary divider-small divider-small-center mb-4">
									<hr>
								</div>
							</div>
						</div>
						<div class="row mt-4">
							<div class="owl-carousel owl-theme owl-team-custom show-nav-title mb-0" data-plugin-options="{'items': 4, 'margin': 10, 'loop': false, 'nav': true, 'dots': false}" id="mitra">
								
							<!-- -->
							</div>
						</div>
					</div>

				</div>

				<section class="parallax section section-text-light section-parallax section-center mt-5" data-plugin-parallax data-plugin-options="{'speed': 1.5}" data-image-src="<?php echo $frontbase; ?>/img/img/demos/law-firm/parallax/parallax-law-firm-2.jpg">
					<div class="container">
						<div class="row counters counters-text-light">
							<div class="col-lg-3 col-sm-6">
								<div class="counter mb-4 mt-4">
									<i class="icon-user-following icons"></i>
									<strong data-to="30000" data-append="+">0</strong>
									<label>Happy Clients</label>
								</div>
							</div>
							<div class="col-lg-3 col-sm-6">
								<div class="counter mb-4 mt-4">
									<i class="icon-diamond icons"></i>
									<strong data-to="15">0</strong>
									<label>Years in Business</label>
								</div>
							</div>
							<div class="col-lg-3 col-sm-6">
								<div class="counter mb-4 mt-4">
									<i class="icon-badge icons"></i>
									<strong data-to="3">0</strong>
									<label>Awards</label>
								</div>
							</div>
							<div class="col-lg-3 col-sm-6">
								<div class="counter mb-4 mt-4">
									<i class="icon-paper-plane icons"></i>
									<strong data-to="178">0</strong>
									<label>Successful Stories</label>
								</div>
							</div>
						</div>
					</div>
				</section>

				<div class="container">
					<div class="row">
						<div class="col-lg-12 text-center">
							<h2 class="mt-4 mb-0">Kabar Advoqu</h2>
							<div class="divider divider-primary divider-small divider-small-center mb-4">
								<hr>
							</div>
						</div>
					</div>
					<div class="row advonews">
					
					</div>
				</div>

			
			</div>

			<script src="<?php echo $frontbase; ?>/vendor/owl.carousel/owl.carousel.min.js"></script>
<script>

async function load_svc(){
	let dt = await fetch("<?php echo $url; ?>/admin/svc_data");
	let dta = await dt.json();
	if(dt.ok){
		let k = "";
		let l = "";
		for(let i in dta.rows){
			if(i < 4){
				k += "<div class='col-lg-4'><div class='feature-box feature-box-style-2 mb-4 appear-animation' data-appear-animation='fadeInUp' data-appear-animation-delay='0'><div class='feature-box-icon w-auto mt-3'>									<img src='<?php echo $frontbase; ?>/img/icon/" + dta.rows[i].icon + "' alt='' style='width: 50px'/>				</div>								<div class='feature-box-info ml-3'>									<h4 class='mb-2'>" + dta.rows[i].name + "</h4>									<p>" + dta.rows[i].descs + "</p>									<a class='mt-3' href='<?php echo $url; ?>/kabaradvoqu/" + dta.rows[i].slug + "'>Learn More <i class='fas fa-long-arrow-alt-right'></i></a>								</div>							</div>						</div>";
			} else {
				l += "<div class='col-lg-4'><div class='feature-box feature-box-style-2 mb-4 appear-animation' data-appear-animation='fadeInUp' data-appear-animation-delay='0'><div class='feature-box-icon w-auto mt-3'>									<img src='<?php echo $frontbase; ?>/img/icon/" + dta.rows[i].icon + "' alt='' style='width: 50px'/>				</div>								<div class='feature-box-info ml-3'>									<h4 class='mb-2'>" + dta.rows[i].name + "</h4>									<p>" + dta.rows[i].descs + "</p>									<a class='mt-3' href='<?php echo $url; ?>/kabaradvoqu/" + dta.rows[i].slug + "'>Learn More <i class='fas fa-long-arrow-alt-right'></i></a>								</div>							</div>						</div>";
			}
		}
		document.querySelector(".svc-1").innerHTML = k;
		document.querySelector(".svc-2").innerHTML = l;
	}
	
}

load_svc();

function excerpt(str){
    let regex = /(<([^>]+)>)/gi;
    let set = str.replace(regex,""); 
    return (set.substring(0,25) + "...");
  }

async function load_news(){
	let dt = await fetch("<?php echo $url; ?>/sys/read?table=news");
	let dta = await dt.json();
	if(dt.ok){ 
		let k = "";
		for(let i in dta.rows){
			let aut = await fetch("<?php echo $url; ?>/auth/rbac?token=" +  dta.rows[i].author );
     		 let auth = await aut.json();
			let tgl = new Intl.DateTimeFormat("id-ID",{ dateStyle: 'full', timeStyle: 'short' }).format(new Date(dta.rows[i].updated));
			let ext = /\.jpg|png|gif|jpeg|JPG|JPEG|PNG|GIF/g;  
			let gambar = ((dta.rows[i].img).match(ext) != null)? "<?php echo $url; ?>/public/uploaded/" + dta.rows[i].img  : "<?php echo $url; ?>/public/back/img/advoqu_logo2.png";
			k += "<div class='col-lg-6'> <span class='thumb-info thumb-info-side-image thumb-info-no-zoom border mb-5'> <span class='thumb-info-side-image-wrapper p-0 d-none d-sm-block'> <a title='' href='<?php echo $url; ?>/kabaradvoqu/" + dta.rows[i].slug + "'> <img src='" + gambar + "' class='img-fluid' alt='' style='width: 195px;'> </a> </span> <span class='thumb-info-caption'> <span class='thumb-info-caption-text py-0 px-4'> <h2 class='mb-3 mt-3'><a title='' class='text-dark'  href='<?php echo $url; ?>/kabaradvoqu/" + dta.rows[i].slug + "'>" + dta.rows[i].title  + "</a></h2> <span class='post-meta'> 	<span>" + tgl  + " | <a href='#'>" + auth.rows[0].fullname  + "</a></span> </span> <p class='text-3 pt-0'>" + excerpt(dta.rows[i].content)  + "</p> <a class='mt-3' href='<?php echo $url; ?>/kabaradvoqu/" + dta.rows[i].slug + "'>Read More <i class='fas fa-long-arrow-alt-right'></i></a></span> </span> </span> </div>";
		}
		document.querySelector(".advonews").innerHTML = k;
	}
}

load_news();

async function load_mitra(){
	let dt = await fetch("<?php echo $url; ?>/admin/mitra_data");
	let dta = await dt.json();
	if(dt.ok){
		let k = "";
		for(let i in dta.rows){
			let logo = "";
			if(dta.rows[i].img_brand == null){
					logo = "public/back/img/advoqu_logo2.png";
			} else {
					logo = "public/uploaded/" + dta.rows[i].img_brand;
			}

			let fw = new FormData();
      fw.append("token",dta.rows[i].user);
      let dx = await fetch("<?php echo $url; ?>/sys/reads?table=users",{
        method: "POST",
        body: fw
      });
      let dxa = await dx.json();

	  let types = "";
      switch(dxa.rows[0].cat){
        case "1": types = "<i class='fa fa-star'></i>"; break;
        case "2": types = "<i class='fa fa-star'></i><i class='fa fa-star'></i>"; break;
        case "3": types = "<i class='fa fa-star'></i><i class='fa fa-star'></i><i class='fa fa-star'></i>"; break;
        case "4": types = "<i class='fa fa-star'></i><i class='fa fa-star'></i><i class='fa fa-star'></i><i class='fa fa-star'></i>"; break;
      }

			k += "<div class='text-center mb-4'> <a href='<?php echo $url; ?>/advoshare/" + dta.rows[i].id + "'> 	<img src='<?php echo $url; ?>/" + logo + "' class='img-fluid mitra' alt=''> 	</a> 	<h4 class='mt-3 mb-0'>" + dxa.rows[0].fullname + "</h4> <p class='mb-0'>" + types + "</p> 	<span class='thumb-info-social-icons mt-2 pb-0'> 	<a href='http://www.facebook.com/' target='_blank'><i class='fab fa-facebook-f'></i><span>Facebook</span></a> 	<a href='http://www.twitter.com/'><i class='fab fa-twitter'></i><span>Twitter</span></a> 	<a href='http://www.linkedin.com/'><i class='fab fa-linkedin-in'></i><span>Linkedin</span></a> 	</p> 	</div>";
		}
		if(dta.rows.length < 5){
			document.querySelector("#mitra").innerHTML = k;
			document.querySelector("#mitra").innerHTML += k;
			document.querySelector("#mitra").innerHTML += k;
			document.querySelector("#mitra").innerHTML += k;
			document.querySelector("#mitra").innerHTML += k;
			document.querySelector("#mitra").innerHTML += k;
			document.querySelector("#mitra").innerHTML += k;
			document.querySelector("#mitra").innerHTML += k;
			document.querySelector("#mitra").innerHTML += k;
			document.querySelector("#mitra").innerHTML += k;
		} else {
			document.querySelector("#mitra").innerHTML = k;
		}
		

		$(function() {
			$('[data-plugin-carousel]:not(.manual), .owl-carousel:not(.manual)').each(function() {
				var $this = $(this),
					opts;

				var pluginOptions = theme.fn.getOptions($this.data('plugin-options'));
				if (pluginOptions)
					opts = pluginOptions;

				$this.themePluginCarousel(opts);
			});
		});
	}
	
}

load_mitra();

async function load_testi(){
	 
	let dt = await fetch("<?php echo $url; ?>/admin/testimoni_data");
	let dta = await dt.json();
	if(dt.ok){
		let k = "";
		for(let i in dta.rows){
			k += "<div><div class='testimonial testimonial-style-3 testimonial-trasnparent-background testimonial-alternarive-font'> 	<blockquote class='text-light'> 	<p class='text-light' style='font-weight: bold; color: #2a444e !important; text-shadow: 0px 2px 4px #ffffff;'>" + dta.rows[i].testi + "</p> 	</blockquote> 	<div class='testimonial-author'> 	<div  class='testimonial-author-thumbnail'> 	<img src='<?php echo $url; ?>/public/uploaded/" + dta.rows[i].foto + "' class='img-fluid rounded-circle' alt=''> 	</div> 	<p><strong>" + dta.rows[i].nama + "</strong><span class='text-light'>" + dta.rows[i].jabatan  + "</span></p> 	</div> </div> </div>";
		}
		document.querySelector("#testi").innerHTML = k;
	}
}

load_testi();

</script>


<?php require "footer.php"; ?>

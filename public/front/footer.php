
			<footer id="footer" class="border-top-0">
				<div class="container py-4">
					<div class="row py-5">
						<div class="col-md-4 mb-4 mb-lg-0">
							<a href="index.html" class="logo pr-0 pr-lg-3 mb-3">
								<img alt="Advoqu" src="<?php echo $backbase; ?>/img/advoqu_logo.png" class="opacity-7 top-auto bottom-10" height="33">
							</a>
							<p class="mt-2 mb-2">R Subekti Law Office | ADVOQU</p>
							<p class="mb-0"><a href="#" class="btn-flat btn-xs text-color-light"><strong class="text-2">VIEW MORE</strong><i class="fas fa-angle-right p-relative top-1 pl-2"></i></a></p>
						</div>
						<div class="col-md-3 mb-4 mb-lg-0">
							<h5 class="text-3 mb-3">STATISTIK KUNJUNGAN</h5>
							<ul class="list list-icons list-icons-lg">
								<li class="mb-1"><i class="fa fa-chart-area text-color-primary"></i><p class="m-0">Total Pengunjung <span class="vis"></span></p></li>
								<li class="mb-1"><i class="fa fa-chart-bar text-color-primary"></i><p class="m-0"><p class="m-0"> Pengunjung Hari Ini <span class="vis"></span></p></li>
								<li class="mb-1">  </li>
							</ul>
						</div>
						<div class="col-md-2 mb-4 mb-lg-0">
							<h5 class="text-3 mb-3">CONTACT US</h5>
							<ul class="list list-icons list-icons-lg">
								<li class="mb-1"><i class="far fa-dot-circle text-color-primary"></i><p class="m-0">Jalan Ngeksigondo</p></li>
								<li class="mb-1"><i class="fab fa-whatsapp text-color-primary"></i><p class="m-0"><a href="#">087726556110</a></p></li>
								<li class="mb-1"><i class="far fa-envelope text-color-primary"></i><p class="m-0"><a href="mailto:advoqu2021@gmail.com">advoqu2021@gmail.com</a></p></li>
							</ul>
						</div>
						<div class="col-md-3">
							<h5 class="text-3 mb-3">FOLLOW US</h5>
							<ul class="header-social-icons social-icons">
								<li class="social-icons-facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
								<li class="social-icons-twitter"><a href="http://www.twitter.com/" target="_blank" title="Twitter"><i class="fab fa-twitter"></i></a></li>
								<li class="social-icons-linkedin"><a href="http://www.linkedin.com/" target="_blank" title="Linkedin"><i class="fab fa-linkedin-in"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="footer-copyright footer-copyright-style-2">
					<div class="container py-2">
						<div class="row py-4">
							<div class="col d-flex align-items-center justify-content-center">
								<p>© Copyright 2021. Advoqu.</p>
							</div>
						</div>
					</div>
				</div>
			</footer>
		</div>

		<!-- Vendor -->
		<script src="<?php echo $frontbase; ?>/vendor/jquery.appear/jquery.appear.min.js"></script>		<script src="<?php echo $frontbase; ?>/vendor/jquery.easing/jquery.easing.min.js"></script>		<script src="<?php echo $frontbase; ?>/vendor/jquery.cookie/jquery.cookie.min.js"></script>		<script src="master/style-switcher/style.switcher.js" id="styleSwitcherScript" data-base-path="" data-skin-src="master/less/skin-law-firm.less"></script>		<script src="<?php echo $frontbase; ?>/vendor/popper/umd/popper.min.js"></script>		<script src="<?php echo $frontbase; ?>/vendor/bootstrap/js/bootstrap.min.js"></script>		<script src="<?php echo $frontbase; ?>/vendor/common/common.min.js"></script>		<script src="<?php echo $frontbase; ?>/vendor/jquery.validation/jquery.validate.min.js"></script>		<script src="<?php echo $frontbase; ?>/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>		<script src="<?php echo $frontbase; ?>/vendor/jquery.gmap/jquery.gmap.min.js"></script>		<script src="<?php echo $frontbase; ?>/vendor/jquery.lazyload/jquery.lazyload.min.js"></script>		<script src="<?php echo $frontbase; ?>/vendor/isotope/jquery.isotope.min.js"></script>			<script src="<?php echo $frontbase; ?>/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>		<script src="<?php echo $frontbase; ?>/vendor/vide/jquery.vide.min.js"></script>		<script src="<?php echo $frontbase; ?>/vendor/vivus/vivus.min.js"></script>
		<!--(remove-empty-lines-end)-->

		<!-- Theme Base, Components and Settings -->
		<script src="<?php echo $frontbase; ?>/js/theme.js"></script>

		<!-- Current Page Vendor and Views -->
		<script src="<?php echo $frontbase; ?>/vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>		<script src="<?php echo $frontbase; ?>/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

		<!-- Current Page Vendor and Views -->
		<script src="<?php echo $frontbase; ?>/js/views/view.contact.js"></script>

		<!-- Demo -->
		<script src="<?php echo $frontbase; ?>/js/demos/demo-law-firm.js"></script>	


		<!-- Theme Initialization Files -->
		<script src="<?php echo $frontbase; ?>/js/theme.js"></script>
		<script src="<?php echo $frontbase; ?>/js/theme.init.js?v=<?php echo rand(1,1000); ?>"></script>



		<script>
		async function watcher(){
			let d = new Date();
			let tgl = (d.getDate() < 10)? "0" + d.getDate() : d.getDate();
			let bln = ((d.getMonth() + 1) < 10)? "0" + (d.getMonth() + 1) : (d.getMonth() + 1);
			let now = d.getFullYear() + "-" + bln + "-" + tgl;
			let fd = new FormData();
			fd.append("visitor","<?php echo $_SERVER["REMOTE_ADDR"]; ?>");
			fd.append("visited",now);
			let dt = await fetch("<?php echo $url; ?>/sys/create?table=visitor",{
				method: "POST",
				body: fd
			});
			let visitor = document.querySelectorAll(".vis");
			let dx = await fetch("<?php echo $url; ?>/sys/open_read?table=visitor"); 
			let dxa = await dx.json();
			if(dx.ok){
				
				visitor[0].innerHTML = dxa.rows[0].total;
			
			}
			let fds = new FormData();
			fds.append("visited",now);
			let dxs = await fetch("<?php echo $url; ?>/sys/open_read?table=visitor&status=1",{
				method: "POST",
				body: fds
			}); 
			let dxas = await dxs.json();
			if(dxs.ok){
				
				visitor[1].innerHTML = dxas.rows[0].total;
			
			}
		}
		watcher();
		</script>
		<!--<a href="index.html#demos" class="go-to-demos"><i class="fas fa-arrow-left"></i> More Demos</a>-->

	</body>

<!-- Mirrored from portotheme.com/html/porto/8.1.0/demo-law-firm.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 24 Nov 2020 17:01:10 GMT -->
</html>
<?php 
$pg = "login";
require "public/setup.php";  
require "header.php"; ?>
<div role="main" class="main">
				<div class="container">
					<div class="row pt-5">
						<div class="col-lg-7">
							<h1 class="mb-0">Silakan Login</h1>
							<div class="divider divider-primary divider-small mb-4">
								<hr class="mr-auto">
							</div>
							<p class="lead mb-5 mt-4">Gunakan Email dan Password anda yang terdaftar</p>

							 
								<div class="contact-form-success alert alert-success d-none mt-4">
									<strong>Success!</strong> Your message has been sent to us.
								</div>

								<div class="contact-form-error alert alert-danger d-none mt-4 err">
								<center> <i class="fa fa-minus-circle fa-2x"></i></center> Username/Password salah. Silakan coba lagi atau hubungi Admin jika anda kesulitan login</label>
								</div>
								
								<div class="row">
									<div class="form-group col">
										<input type="email" id="u" placeholder="Your E-mail"  class="form-control" name="email" required>
									</div>
								</div>
								<div class="row">
									<div class="form-group col">
										<input type="password" id="p" placeholder="Password" class="form-control" name="subject" required>
									</div>
								</div> 
								<div class="row">
									<div class="form-group col">
										<button type="button" value="Login" class="btnx btn btn-primary btn-lg mb-5" data-loading-text="Loading..." onclick="validate()">Login</button>
									</div>
								</div>
						 

						</div>
						<div class="col-lg-4 col-lg-offset-1">

							<h4 class="mb-0">Daftar</h4>
							<div class="divider divider-primary divider-small mb-4">
								<hr class="mr-auto">
							</div>

							<ul class="list list-icons list-icons-style-3 mt-4 mb-4">
								<li><i class="fas fa-sign-in-alt"></i> 
								<a href="<?php echo $url; ?>/main/register"><button class="btn btn-primary">Daftar Sekarang</button></a>
							</ul>

							<h4 class="pt-4 mb-0">Ketentuan</h4>
							<div class="divider divider-primary divider-small mb-4">
								<hr class="mr-auto">
							</div>

							<ul class="list list-icons list-dark mt-4">
								<li><i class="far fa-check-square"></i> Pendaftaran akan berlaku setelah diverifikasi Admin</li>
								<li><i class="far fa-check-square"></i> Verifikasi maksimal 1 x 24 jam</li>
							</ul>

						</div>
					</div>
				</div>

				<!-- Google Maps - Go to the bottom of the page to change settings and map location. -->
				<div id="googlemaps" class="google-map google-map-footer"></div>
			</div>

<script>
async function validate(){
	let btn = document.getElementsByClassName("btnx")[0];
	btn.innerHTML = "<i class='fa fa-spin fa-cog'></i> processing...";
	btn.setAttribute("disabled",true);
	document.getElementsByClassName("err")[0].classList.add("d-none");
	let fd = new FormData();
	fd.append("username",document.getElementById("u").value);
	fd.append("password",document.getElementById("p").value);
	let a = await fetch("<?php echo $url; ?>/auth/validate",{
		method: "POST",
		body: fd
	});
	let b = await a.text();
	if(a.ok){
		if(b == "invalid"){
			document.getElementsByClassName("err")[0].classList.remove("d-none");
			btn.removeAttribute("disabled");
			btn.innerHTML = "Login";
		} else {
			let curhat = "<?php echo $this->input->get("status"); ?>";
			if(curhat == "y"){
				location.href = "<?php echo $url; ?>/curhatkasus/home?token=" + b;
			} else {
				location.href = "<?php echo $url; ?>/curhatkasus/home?token=" + b;
			}
				}
	}
}
</script>

<?php require "footer.php"; ?>
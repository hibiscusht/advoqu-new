<?php  
require "public/setup.php";  
require "header.php"; ?>

     <!-- BEGIN: Content-->
     <div class="app-content content">
      <div class="content-overlay"></div>
      <div class="content-wrapper">
        <div class="content-header row">

        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title mb-0">Mitra Advoqu</h3>
            <div class="row breadcrumbs-top">
              <div class="breadcrumb-wrapper col-12">
                <ol class="breadcrumb">
                  
                  <li class="breadcrumb-item">Home
                  </li>
                  <li class="breadcrumb-item active">Mitra Advoqu
                  </li>
                </ol>
              </div>
            </div>
            </div>
        </div>
        <div class="content-body">

        <a href="<?php echo $url; ?>/admin/share_tmp?token=<?php echo $this->input->get("token"); ?>"><button class="btn btn-success">Verifikasi Mitra Advoqu</button></a>

<br/> <br/>

        <table class="table table-striped table-bordered zero-configuration">
                            <thead>
                                <tr>
                                <th>Aksi</th>
                                <th>Jenis Mitra</th>
                                <th>Nama Kantor Hukum</th>
                                <th>Username/Email</th>
                                <th>Alamat Kantor Hukum</th>
                                </tr>
                            </thead>
                            <tbody id="d-kat">
                        </tbody>
                    </table>

        </div>
      </div>
    </div>
    <!-- END: Content-->

 

<script>


function clrall(){
  let d = document.querySelectorAll(".clr");
  for(let i = 0; i < d.length; i++){
    d[i].value = "";
  }
  
  document.querySelector("#kat_id").value = "";
}

async function hapus(id){
    let sw = await Swal.fire({title:"Hapus Kategori",text:"Yakin hapus kategori?",type:"question",showCancelButton:!0,confirmButtonColor:"#3085d6",cancelButtonColor:"#d33",confirmButtonText:"Lanjutkan",confirmButtonClass:"btn btn-primary",cancelButtonClass:"btn btn-danger ml-1",buttonsStyling:!1});

// console.log(typeof sw.value);

  if(typeof sw.value !== "undefined"){
   document.getElementById(id).innerHTML = "<i class='fa fa-circle-o-notch fa-fw fa-spin'></i>";
   let j = id.split("-");
   let val = await fetch("<?php echo $url; ?>/admin/kategori_delete?id=" + j[1]);
   if(val.ok){
     Swal.fire({type:"success",title:"Berhasil",text:"Data telah dihapus",confirmButtonClass:"btn btn-success"})
     .then(function(){
       location.reload();
     });
   }
 }  
}

 

async function load_data(){
  let usr = await fetch("<?php echo $url; ?>/admin/mitra_data");
  let usr_dt = await usr.json();
  if(usr.ok){
    let k = "";
    for(let i in usr_dt.rows){

      let fw = new FormData();
      fw.append("token",usr_dt.rows[i].user);
      let dx = await fetch("<?php echo $url; ?>/sys/reads?table=users",{
        method: "POST",
        body: fw
      });
      let dxa = await dx.json();

      let types = "";
      switch(dxa.rows[0].cat){
        case "1": types = "<span class='badge' style='background: grey'>Primarium</span>"; break;
        case "2": types = "<span class='badge' style='background: green'>Deuterium</span>"; break;
        case "3": types = "<span class='badge' style='background: orange'>Tertium</span>"; break;
        case "4": types = "<span class='badge' style='background: gold'>Ultimum</span>"; break;
      }

    
      k += "<tr><td> </td><td>" + types + "</td><td>" + dxa.rows[0].fullname + "</td></td><td>" + usr_dt.rows[i].email + "</td><td>" + usr_dt.rows[i].geo_loc + "</td></tr>";
    }
    document.getElementById("d-kat").innerHTML = k;
    $(".zero-configuration").DataTable();
  } 
}
load_data();

async function load_module(){
  let mod = await fetch("<?php echo $url; ?>/admin/module?token=<?php echo $this->input->get("token"); ?>");
  let data = await mod.json();
  if(mod.ok){
    let li = "";
    for(let i in data.rows){
        li += "<li class='nav-item'><a href='<?php echo $url; ?>/" + data.rows[i].link + "?token=<?php echo $this->input->get("token"); ?>'><i class='fa fa-gear'></i><span class='menu-item'>" + data.rows[i].name + "</span></a></li>";
    }
    document.getElementById("main-menu-navigation").innerHTML = li;
  }
}
load_module();
</script>
        

<?php require "footer.php"; ?>
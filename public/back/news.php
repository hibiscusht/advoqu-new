<?php  
require "public/setup.php";  
require "header.php"; ?>


<link rel="stylesheet" type="text/css" href="<?php echo $backbase; ?>/app-assets/vendors/css/forms/quill/katex.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $backbase; ?>/app-assets/vendors/css/forms/quill/monokai-sublime.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $backbase; ?>/app-assets/vendors/css/forms/quill/quill.snow.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $backbase; ?>/app-assets/vendors/css/forms/quill/quill.bubble.css">

     <!-- BEGIN: Content-->
     <div class="app-content content">
      <div class="content-overlay"></div>
      <div class="content-wrapper">
        <div class="content-header row">

        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title mb-0">Data Kabar Advoqu</h3>
            <div class="row breadcrumbs-top">
              <div class="breadcrumb-wrapper col-12">
                <ol class="breadcrumb">
                  
                  <li class="breadcrumb-item">Home
                  </li>
                  <li class="breadcrumb-item active">Data Kabar Advoqu
                  </li>
                </ol>
              </div>
            </div>
            </div>
        </div>
        
<style>
@media (max-width: 700px){
	.page-link {
		font-size: 0.85em !important;
		padding: 1em !important;

	}
	.page-link .fa {
		font-size: 0.85em !important;
		padding: 0.1em !important
	}
}
</style>
        <div class="content-body">

        <button class="btn btn-success" data-toggle='modal' data-target='#bootstrap' onclick="opener(); clrall()">Tambah Data</button>

        <br/> <br/>

        <table class="table table-striped table-bordered zero-configuration">
                            <thead>
                                <tr>
                                <th>Aksi</th>
                                    <th>Gambar</th>
                                    <th>Judul</th> 
                                    <th>Penulis</th> 
                                </tr>
                            </thead>
                            <tbody id="d-faq">
                        </tbody>
                    </table>


        </div>

        <div class="section-5 container mt-4 head mypaging">
        <br>
		<nav aria-label="Page navigation">
			<ul class="pagination pagination-lg mb-1">
			<li class="page-item"><a class="page-link" style="cursor: pointer" onclick="scrolls('left')"><i class="fa fa-arrow-left"></i></a></li>
			<li class="page-item"><a class="page-link pages active" style="cursor: pointer">1</a></li>
			<li class="page-item"><a class="page-link pages" style="cursor: pointer">2</a></li>
			<li class="page-item"><a class="page-link pages" style="cursor: pointer">3</a></li>
			<li class="page-item"><a class="page-link pages" style="cursor: pointer">4</a></li>
			<li class="page-item"><a class="page-link pages" style="cursor: pointer">5</a></li>
			<li class="page-item"><a class="page-link next-page" style="cursor: pointer"><i class="fa fa-arrow-right" onclick="scrolls('right')"></i></a></li>
									</ul>
								</nav>
							</div>
      </div>
    </div>
    <!-- END: Content-->

    <script>



//paging area
        
function scrolls(pos){
            let pages = document.querySelectorAll(".pages");
            let ult_l = parseInt(pages[0].innerHTML);
            let ult_r = parseInt(pages[pages.length - 1].innerHTML);
            if(pos == "right"){
                let lefty = 0;
            for(let i = 0; i < pages.length; i++){
                let cur = ult_r + (i + 1);
                if(i == 0){
                    lefty = cur;
                }
                pages[i].setAttribute("onclick","load_data(" + ((cur - 1) * 25) + ");laster(" + cur + ")");
                pages[i].innerHTML = cur;

                if(pages[i].innerHTML == lefty){
                        pages[i].classList.add("active");
                    } else {
                        pages[i].classList.remove("active");
                    }
            }
            load_data(((lefty - 1) * 25));
            } else {
                let righty = 0;
            for(let i = 0; i < pages.length; i++){
                let curs = (ult_l - 5) + 0;
                let cur = (curs < 1)? (i + 1) : (ult_l - 5) + i;
                if(i == 4){
                    righty = (curs < 1)? 0 : cur;
                }
                pages[i].setAttribute("onclick","load_data(" + ((cur - 1) * 25) + ");laster(" + cur + ")");
                pages[i].innerHTML = cur;

                if(pages[i].innerHTML == righty){
                        pages[i].classList.add("active");
                    } else {
                        pages[i].classList.remove("active");
                    }
            }
            let res = (righty == 0)? 0 : ((righty - 1) * 25);
            load_data(res);
            }
            
        }

        function laster(val){
            let elems = document.querySelectorAll(".pages");

             
            for(let i in elems){
                if(typeof elems[i].style != "undefined"){
                     if(elems[i].innerHTML == val){
                        elems[i].classList.add("active");
                    } else {
                        elems[i].classList.remove("active");
                    }
                }
            }
        
        }

        function paging(){

            let pages = document.querySelectorAll(".pages");
            for(let i = 0; i < pages.length; i++){
                let cur = parseInt(pages[i].innerHTML);
                if(cur == 1){
                    pages[i].setAttribute("onclick","load_data(0); laster(1)");
                    pages[i].classList.add("active");
                } else {
                    pages[i].setAttribute("onclick","load_data(" + ((cur - 1) * 25) + ");laster(" + cur + ")");
                    pages[i].classList.remove("active");
                }

            }
            
        }

        paging();

		///paging area

</script>

    
<!-- Modal -->
<div class="modal fade text-left" id="bootstrap" tabindex="-1" role="dialog" aria-labelledby="myModalLabel35" aria-hidden="true">
									  <div class="modal-dialog" role="document">
										<div class="modal-content" style="height: 700px">
										  <div class="modal-header">
											<h3 class="modal-title" id="myModalLabel35"> Data Kabar Advoqu</h3>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											  <span aria-hidden="true">&times;</span>
											</button>
										  </div>
											<div class="modal-body" style="overflow-y: auto; height: 400px">
											 <div>
                    <input type="hidden" id="usr_id"/>
                    <input type="hidden" class="clr" data-key="author" value="<?php echo $this->input->get("token");  ?>"/>
         <div class="form-group">
         <label>Judul</label>
         <input type="text" class="form-control clr" data-key="title"/>
         </div>   

          <div class="form-group">
         <label>Isi</label>
         <div class="toolbar2">
         <span class="ql-formats">
													<select class="ql-header">
														<option value="1">Heading</option>
														<option value="2">Subheading</option>
														<option selected>Normal</option>
													</select>
													<select class="ql-font">
														<option selected>Sailec Light</option>
														<option value="sofia">Sofia Pro</option>
														<option value="slabo">Slabo 27px</option>
														<option value="roboto">Roboto Slab</option>
														<option value="inconsolata">Inconsolata</option>
														<option value="ubuntu">Ubuntu Mono</option>
													</select>
												</span>
												<span class="ql-formats">
													<button class="ql-bold"></button>
													<button class="ql-italic"></button>
													<button class="ql-underline"></button>
												</span>
												<span class="ql-formats">
													<button class="ql-list" value="ordered"></button>
													<button class="ql-list" value="bullet"></button>
												</span>
												<span class="ql-formats">
													<button class="ql-link"></button>
													<button class="ql-image"></button>
													<button class="ql-video"></button>
												</span>
												<span class="ql-formats">
													<button class="ql-formula"></button>
													<button class="ql-code-block"></button>
												</span>
												<span class="ql-formats">
													<button class="ql-clean"></button>
												</span>
         </div>
         <div id="editor2" style="height: 150px" class="clr" data-key="content"></div>
         </div>             
         <div class="form-group">
         <label>Gambar</label>
         <input type="file" class="form-control clr" data-key="img"/>
         </div>   

											</div>
											<div class="modal-footer">

                                            <button type="button" class="btnx btn btn-outline-secondary btn-lg" onclick="save_data()">Simpan</button>

												<input type="reset" class="btn btn-outline-secondary btn-lg" data-dismiss="modal" value="Tutup">
											
											</div>
										 
										</div>
									  </div>
									</div>



    <!-- BEGIN: Page Vendor JS-->
    <script src="<?php echo $backbase; ?>/app-assets/vendors/js/forms/quill/highlight.min.js"></script>
    <script src="<?php echo $backbase; ?>/app-assets/vendors/js/forms/quill/quill.js"></script>
    <script src="<?php echo $backbase; ?>/app-assets/vendors/js/forms/quill/katex.min.js"></script>
    <!-- END: Page Vendor JS-->

<script>


function clrall(){
  let d = document.querySelectorAll(".clr .ql-editor");
  for(let i = 0; i < d.length; i++){
    d[i].innerHTML = "";
  }
  let di = document.querySelectorAll(".clr");
  for(let j = 0; j < di.length; j++){ 
    if(di[j].dataset.key != "author"){
      di[j].value = ""; 
    }
  }
  document.querySelector("#usr_id").value = "";
}

function opener(){
   
        new Quill("#editor2", {
		modules: {
		toolbar: '.toolbar2'
		},
		theme: 'snow'
		});      
}


async function hapus(id){
    let sw = await Swal.fire({title:"Hapus Kabar Advoqu",text:"Yakin hapus kabar advoqu?",type:"question",showCancelButton:!0,confirmButtonColor:"#3085d6",cancelButtonColor:"#d33",confirmButtonText:"Lanjutkan",confirmButtonClass:"btn btn-primary",cancelButtonClass:"btn btn-danger ml-1",buttonsStyling:!1});

// console.log(typeof sw.value);

  if(typeof sw.value !== "undefined"){
   document.getElementById(id).innerHTML = "<i class='fa fa-circle-o-notch fa-fw fa-spin'></i>";
   let j = id.split("-");
   let val = await fetch("<?php echo $url; ?>/admin/advonews_delete?id=" + j[1]);
   if(val.ok){
     Swal.fire({type:"success",title:"Berhasil",text:"Data telah dihapus",confirmButtonClass:"btn btn-success"})
     .then(function(){
       location.reload();
     });
   }
 }  
}


async function edit(id){
    let ids = id.split("-");
    let usr = await fetch("<?php echo $url; ?>/admin/advonews_data?id=" + ids[1]);
    let usr_dt = await usr.json();
    opener();
    clrall();
    document.querySelector("#usr_id").value = ids[1];
    let elems = document.querySelectorAll(".clr");
    for(let i = 0; i < elems.length; i++){
      let keys = elems[i].dataset.key;
      elems[i].value = (keys == "img")? "" : usr_dt.rows[0][keys];
    }
    document.querySelector("#editor2 .ql-editor").innerHTML = usr_dt.rows[0].content; 
}

async function save_data(){
  document.querySelector(".btnx").setAttribute("disabled","");
    document.querySelector(".btnx").innerHTML = "<i class='fa fa-gear fa-spin'></i> menyimpan...";
  let edit = document.querySelector("#usr_id").value;
  let fd = new FormData();
 
    let elems = document.querySelectorAll(".clr");
    for(let i = 0; i < elems.length; i++){
      if(elems[i].dataset.key == "img"){
        if(elems[i].value == ""){} else {
          fd.append(elems[i].dataset.key,elems[i].files[0]);
        }
      } else {
        if(elems[i].dataset.key == "content"){
          fd.append(elems[i].dataset.key,document.querySelector(".clr .ql-editor").innerHTML);
        } else {
          fd.append(elems[i].dataset.key,elems[i].value);
        }
      }
    }

    let url = "";
    if(edit == ""){
      url = "<?php echo $url; ?>/admin/advonews_add";
      } else {
        url = "<?php echo $url; ?>/admin/advonews_update?id=" + edit;
      }

      let dt = await fetch(url,{
        method: "POST",
        body: fd
      });
      if(dt.ok){
        location.reload();
              }
}

async function load_data(limit){
  document.querySelector("#d-faq").innerHTML = "<tr><td colspan=5><i class='fa fa-spin fa-spinner fa-fw'></i></td></tr>";
  let usr = await fetch("<?php echo $url; ?>/admin/advonews_data?lim=" + limit);
  let usr_dt = await usr.json();
  if(usr.ok){
    let k = "";
    for(let i in usr_dt.rows){

      let aut = await fetch("<?php echo $url; ?>/auth/rbac?token=" +  usr_dt.rows[i].author );
      let auth = await aut.json();
    
      k += "<tr><td><button class='btn btn-sm btn-info' title='Edit' style='cursor: pointer' data-toggle='modal' data-target='#bootstrap'  onclick='edit(this.id)' id='edit-" + usr_dt.rows[i].id + "'><i class='fa fa-pencil'></i></button>&nbsp;&nbsp;<button class='btn btn-sm btn-danger' title='Hapus' style='cursor: pointer' onclick='hapus(this.id)' id='del-" + usr_dt.rows[i].id + "'><i class='fa fa-trash-o'></i></button></td><td><a href='<?php echo $url; ?>/public/uploaded/" + usr_dt.rows[i].img + "' target='_BLANK'><i class='fa fa-external-link'></i> Buka</a></td><td>" + usr_dt.rows[i].title + "</td><td>" + auth.rows[0].fullname + "</td></tr>";
    }
    document.getElementById("d-faq").innerHTML = k;
   // $(".zero-configuration").DataTable();
  } 
}
load_data(0);

async function load_module(){
  let mod = await fetch("<?php echo $url; ?>/admin/module?token=<?php echo $this->input->get("token"); ?>");
  let data = await mod.json();
  if(mod.ok){
    let li = "";
    for(let i in data.rows){
        li += "<li class='nav-item'><a href='<?php echo $url; ?>/" + data.rows[i].link + "?token=<?php echo $this->input->get("token"); ?>'><i class='fa fa-gear'></i><span class='menu-item'>" + data.rows[i].name + "</span></a></li>";
    }
    document.getElementById("main-menu-navigation").innerHTML = li;
  }
}
load_module();
</script>

<?php require "footer.php"; ?>
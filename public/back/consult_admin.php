<?php  
require "public/setup.php";  
require "header.php"; ?>


<link rel="stylesheet" type="text/css" href="<?php echo $backbase; ?>/app-assets/vendors/css/forms/quill/katex.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $backbase; ?>/app-assets/vendors/css/forms/quill/monokai-sublime.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $backbase; ?>/app-assets/vendors/css/forms/quill/quill.snow.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $backbase; ?>/app-assets/vendors/css/forms/quill/quill.bubble.css">

     <!-- BEGIN: Content-->
     <div class="app-content content">
      <div class="content-overlay"></div>
      <div class="content-wrapper">
        <div class="content-header row">

        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title mb-0">Data Curhat Kasus</h3>
            <div class="row breadcrumbs-top">
              <div class="breadcrumb-wrapper col-12">
                <ol class="breadcrumb">
                  
                  <li class="breadcrumb-item">Home
                  </li>
                  <li class="breadcrumb-item active">Data Curhat Kasus
                  </li>
                </ol>
              </div>
            </div>
            </div>
        </div>
        <div class="content-body">

        <button class="btn btn-success" data-toggle='modal' data-target='#bootstrap' onclick="opener(); clrall()">Tambah Data</button>

        <br/> <br/>

        <table class="table table-striped table-bordered zero-configuration">
                            <thead>
                                <tr>
                                <th>Aksi</th>
                                    <th>Pertanyaan</th>
                                    <th>Jawaban</th> 
                                </tr>
                            </thead>
                            <tbody id="d-faq">
                        </tbody>
                    </table>


        </div>
      </div>
    </div>
    <!-- END: Content-->

    
<!-- Modal -->
<div class="modal fade text-left" id="bootstrap" tabindex="-1" role="dialog" aria-labelledby="myModalLabel35" aria-hidden="true">
									  <div class="modal-dialog" role="document">
										<div class="modal-content" style="height: 700px">
										  <div class="modal-header">
											<h3 class="modal-title" id="myModalLabel35"> Data Curhat Kasus</h3>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											  <span aria-hidden="true">&times;</span>
											</button>
										  </div>
											<div class="modal-body" style="overflow-y: auto; height: 400px">
											 <div>
                    <input type="hidden" id="usr_id" />
         <div class="form-group">
         <label>Pertanyaan</label>
         <div class="toolbar1">
         <span class="ql-formats">
													<select class="ql-header">
														<option value="1">Heading</option>
														<option value="2">Subheading</option>
														<option selected>Normal</option>
													</select>
													<select class="ql-font">
														<option selected>Sailec Light</option>
														<option value="sofia">Sofia Pro</option>
														<option value="slabo">Slabo 27px</option>
														<option value="roboto">Roboto Slab</option>
														<option value="inconsolata">Inconsolata</option>
														<option value="ubuntu">Ubuntu Mono</option>
													</select>
												</span>
												<span class="ql-formats">
													<button class="ql-bold"></button>
													<button class="ql-italic"></button>
													<button class="ql-underline"></button>
												</span>
												<span class="ql-formats">
													<button class="ql-list" value="ordered"></button>
													<button class="ql-list" value="bullet"></button>
												</span>
												<span class="ql-formats">
													<button class="ql-link"></button>
													<button class="ql-image"></button>
													<button class="ql-video"></button>
												</span>
												<span class="ql-formats">
													<button class="ql-formula"></button>
													<button class="ql-code-block"></button>
												</span>
												<span class="ql-formats">
													<button class="ql-clean"></button>
												</span>
         </div>
         <div id="editor1" style="height: 150px" class="clr"></div>
         </div>   

          <div class="form-group">
         <label>Jawaban</label>
         <div class="toolbar2">
         <span class="ql-formats">
													<select class="ql-header">
														<option value="1">Heading</option>
														<option value="2">Subheading</option>
														<option selected>Normal</option>
													</select>
													<select class="ql-font">
														<option selected>Sailec Light</option>
														<option value="sofia">Sofia Pro</option>
														<option value="slabo">Slabo 27px</option>
														<option value="roboto">Roboto Slab</option>
														<option value="inconsolata">Inconsolata</option>
														<option value="ubuntu">Ubuntu Mono</option>
													</select>
												</span>
												<span class="ql-formats">
													<button class="ql-bold"></button>
													<button class="ql-italic"></button>
													<button class="ql-underline"></button>
												</span>
												<span class="ql-formats">
													<button class="ql-list" value="ordered"></button>
													<button class="ql-list" value="bullet"></button>
												</span>
												<span class="ql-formats">
													<button class="ql-link"></button>
													<button class="ql-image"></button>
													<button class="ql-video"></button>
												</span>
												<span class="ql-formats">
													<button class="ql-formula"></button>
													<button class="ql-code-block"></button>
												</span>
												<span class="ql-formats">
													<button class="ql-clean"></button>
												</span>
         </div>
         <div id="editor2" style="height: 150px" class="clr"></div>
         </div>             


											</div>
											<div class="modal-footer">

                                            <button type="button" class="btnx btn btn-outline-secondary btn-lg" onclick="save_data()">Simpan</button>

												<input type="reset" class="btn btn-outline-secondary btn-lg" data-dismiss="modal" value="Tutup">
											
											</div>
										 
										</div>
									  </div>
									</div>



    <!-- BEGIN: Page Vendor JS-->
    <script src="<?php echo $backbase; ?>/app-assets/vendors/js/forms/quill/highlight.min.js"></script>
    <script src="<?php echo $backbase; ?>/app-assets/vendors/js/forms/quill/quill.js"></script>
    <script src="<?php echo $backbase; ?>/app-assets/vendors/js/forms/quill/katex.min.js"></script>
    <!-- END: Page Vendor JS-->
<script>

$(document).ready(function(){
 
});

function clrall(){
  let d = document.querySelectorAll(".clr .ql-editor");
  for(let i = 0; i < d.length; i++){
    d[i].innerHTML = "";
  }
  document.querySelector("#usr_id").value = "";
}

function opener(){
   new Quill("#editor1", {
		modules: {
		toolbar: '.toolbar1'
		},
		theme: 'snow'
		});

        new Quill("#editor2", {
		modules: {
		toolbar: '.toolbar2'
		},
		theme: 'snow'
		});      
}


async function hapus(id){
    let sw = await Swal.fire({title:"Hapus Curhat Kasus",text:"Yakin hapus curhat kasus?",type:"question",showCancelButton:!0,confirmButtonColor:"#3085d6",cancelButtonColor:"#d33",confirmButtonText:"Lanjutkan",confirmButtonClass:"btn btn-primary",cancelButtonClass:"btn btn-danger ml-1",buttonsStyling:!1});

// console.log(typeof sw.value);

  if(typeof sw.value !== "undefined"){
   document.getElementById(id).innerHTML = "<i class='fa fa-circle-o-notch fa-fw fa-spin'></i>";
   let j = id.split("-");
   let val = await fetch("<?php echo $url; ?>/admin/consult_delete?id=" + j[1]);
   if(val.ok){
     Swal.fire({type:"success",title:"Berhasil",text:"Data telah dihapus",confirmButtonClass:"btn btn-success"})
     .then(function(){
       location.reload();
     });
   }
 }  
}

async function edit(id){
    let ids = id.split("-");
    let usr = await fetch("<?php echo $url; ?>/admin/consult_data?id=" + ids[1]);
    let usr_dt = await usr.json();
    opener();
    clrall();
    document.querySelector("#usr_id").value = ids[1];
    document.querySelector("#editor1 .ql-editor").innerHTML = usr_dt.rows[0].question;
    document.querySelector("#editor2 .ql-editor").innerHTML = usr_dt.rows[0].answer;
}

async function save_data(){
    document.querySelector(".btnx").setAttribute("disabled","");
    document.querySelector(".btnx").innerHTML = "<i class='fa fa-gear fa-spin'></i> menyimpan...";
    let fd = new FormData();
    fd.append("q",document.querySelector("#editor1 .ql-editor").innerHTML);
    fd.append("a",document.querySelector("#editor2 .ql-editor").innerHTML);
    let edit = document.querySelector("#usr_id").value;
    let url = "<?php echo $url; ?>/admin/consult_add";
    if(edit != ""){
        url = "<?php echo $url; ?>/admin/consult_update?id=" + edit; 
    }
    let acts = await fetch(url,{
        method: "POST",
        body: fd
    });
    if(acts.ok){
        location.reload();
    }
}

async function load_data(){
  let usr = await fetch("<?php echo $url; ?>/admin/consult_data");
  let usr_dt = await usr.json();
  if(usr.ok){
    let k = "";
    for(let i in usr_dt.rows){
    
      k += "<tr><td><button class='btn btn-sm btn-info' title='Edit' style='cursor: pointer' data-toggle='modal' data-target='#bootstrap'  onclick='edit(this.id)' id='edit-" + usr_dt.rows[i].id + "'><i class='fa fa-pencil'></i></button>&nbsp;&nbsp;<button class='btn btn-sm btn-danger' title='Hapus' style='cursor: pointer' onclick='hapus(this.id)' id='del-" + usr_dt.rows[i].id + "'><i class='fa fa-trash-o'></i></button></td><td>" + usr_dt.rows[i].question + "</td><td>" + usr_dt.rows[i].answer + "</td></tr>";
    }
    document.getElementById("d-faq").innerHTML = k;
    $(".zero-configuration").DataTable();
  } 
}
load_data();


async function load_module(){
  let mod = await fetch("<?php echo $url; ?>/admin/module?token=<?php echo $this->input->get("token"); ?>");
  let data = await mod.json();
  if(mod.ok){
    let li = "";
    for(let i in data.rows){
        li += "<li class='nav-item'><a href='<?php echo $url; ?>/" + data.rows[i].link + "?token=<?php echo $this->input->get("token"); ?>'><i class='fa fa-gear'></i><span class='menu-item'>" + data.rows[i].name + "</span></a></li>";
    }
    document.getElementById("main-menu-navigation").innerHTML = li;
  }
}
load_module();
</script>

<?php require "footer.php"; ?>
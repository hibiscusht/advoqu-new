<?php  
require "public/setup.php";  
require "header.php"; ?>

     <!-- BEGIN: Content-->
     <div class="app-content content">
      <div class="content-overlay"></div>
      <div class="content-wrapper">
        <div class="content-header row">

        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title mb-0">Tarif Layanan Mitra Advoqu</h3>
            <div class="row breadcrumbs-top">
              <div class="breadcrumb-wrapper col-12">
                <ol class="breadcrumb">
                  
                  <li class="breadcrumb-item">Home
                  </li>
                  <li class="breadcrumb-item active">Tarif Layanan Mitra Advoqu
                  </li>
                </ol>
              </div>
            </div>
            </div>
        </div>
        <div class="content-body">
 
        <button class="btn btn-success" data-toggle='modal' data-target='#bootstrap' onclick="clrall()">Tambah Data</button>

<br/> <br/>

<table class="table table-striped table-bordered zero-configuration">
                    <thead>
                        <tr>
                        <th>Aksi</th>
                        <th>Kategori Mitra</th>
                        <th>Nama Layanan</th>
                        <th>Share Mitra Advoqu</th>
                        <th>Share Advoqu</th>
                        <th>Tarif untuk Klien</th>
                        </tr>
                    </thead>
                    <tbody id="d-kat">
                </tbody>
            </table>

</div>
</div>
</div>
<!-- END: Content-->


    
<!-- Modal -->
<div class="modal fade text-left" id="bootstrap" tabindex="-1" role="dialog" aria-labelledby="myModalLabel35" aria-hidden="true">
									  <div class="modal-dialog" role="document">
										<div class="modal-content">
										  <div class="modal-header">
											<h3 class="modal-title" id="myModalLabel35">Tarif Layanan Mitra Advoqu</h3>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											  <span aria-hidden="true">&times;</span>
											</button>
										  </div>
											<div class="modal-body kategori" style="overflow-y: auto;">
										 
                    <input type="hidden" id="kat_id" />
         <div class="form-group">
         <label>Kategori Mitra</label>
       <select class="form-control clr type" data-key="type"> 
       <option value="">--silakan pilih--</option>
       </select>
         </div>  
         <div class="form-group">
         <label>Nama Layanan</label>
         <select class="form-control clr service" data-key="service"> 
         <option value="">--silakan pilih--</option>
       </select></div>
        
         <div class="form-group">
         <label>Share Mitra Advoqu</label>
        <input type="text"  class="form-control clr calc" data-key="share_mitra" onblur="calc()" />
         </div>  
         <div class="form-group">
         <label>Share Advoqu</label>
        <input type="text"  class="form-control clr calc" data-key="share_advoqu" onblur="calc()" />
         </div>             
         <div class="form-group">
         <label>Tarif untuk Klien</label>
        <input type="text" class="form-control clr real_tarif" data-key="real_tarif" disabled/>
         </div>  

											</div>
											<div class="modal-footer">

                                            <button type="button" class="btnx btn btn-outline-secondary btn-lg" onclick="save_data()">Simpan</button>

												<input type="reset" class="btn btn-outline-secondary btn-lg" data-dismiss="modal" value="Tutup">
											
											</div>
										 
										</div>
									  </div>
									</div>

<script>

function calc(){
  let elem = document.querySelectorAll(".calc");
  let c = 0;
  for(let i = 0; i < elem.length; i++){
    let value = elem[i].value == "" ? 0 : elem[i].value;
    c = c + parseInt(value);
  }
  document.querySelector(".real_tarif").value = c;
}


function clrall(){
  let d = document.querySelectorAll(".clr");
  for(let i = 0; i < d.length; i++){
    d[i].value = "";
  }
  document.querySelector("#kat_id").value = "";
  load_type();
  load_svc();
   
}

async function load_type(){
  let dt = await fetch("<?php echo $url; ?>/sys/readed?table=kategori_user");
  let dta = await dt.json();
  if(dt.ok){
    let k = "<option value=''>--silakan pilih--</option>";
    for(let i in dta.rows){
      k += "<option value='" + dta.rows[i].id + "'>" + dta.rows[i].name + "</option>";
    }
    document.querySelector(".type").innerHTML = k;
  }
}


async function load_svc(){
  let dt = await fetch("<?php echo $url; ?>/sys/readed?table=service");
  let dta = await dt.json();
  if(dt.ok){
    let k = "<option value=''>--silakan pilih--</option>";
    for(let i in dta.rows){
      k += "<option value='" + dta.rows[i].id + "'>" + dta.rows[i].name + "</option>";
    }
    document.querySelector(".service").innerHTML = k;
  }
}


async function hapus(id){
    let sw = await Swal.fire({title:"Hapus Layanan",text:"Yakin hapus layanan?",type:"question",showCancelButton:!0,confirmButtonColor:"#3085d6",cancelButtonColor:"#d33",confirmButtonText:"Lanjutkan",confirmButtonClass:"btn btn-primary",cancelButtonClass:"btn btn-danger ml-1",buttonsStyling:!1});

// console.log(typeof sw.value);

  if(typeof sw.value !== "undefined"){
   document.getElementById(id).innerHTML = "<i class='fa fa-circle-o-notch fa-fw fa-spin'></i>";
   let j = id.split("-");
   let val = await fetch("<?php echo $url; ?>/admin/sharesvc_delete?id=" + j[1]);
   if(val.ok){
     Swal.fire({type:"success",title:"Berhasil",text:"Data telah dihapus",confirmButtonClass:"btn btn-success"})
     .then(function(){
       location.reload();
     });
   }
 }  
}


async function edit(id){
   clrall();
    let ids = id.split("-");
    let usr = await fetch("<?php echo $url; ?>/admin/sharesvc_data?id=" + ids[1]);
    let usr_dt = await usr.json();  
   if(usr.ok){
    document.querySelector("#kat_id").value = ids[1];
    let elem = document.querySelectorAll(".clr");
    for(let i = 0; i < elem.length; i++){
     elem[i].value = usr_dt.rows[0][elem[i].dataset.key];
    }
   }
   
}


async function save_data(){
    document.querySelector(".btnx").setAttribute("disabled","");
    document.querySelector(".btnx").innerHTML = "<i class='fa fa-gear fa-spin'></i> menyimpan...";
    let fd = new FormData(); 
    let elem = document.querySelectorAll(".clr");
    for(let i = 0; i < elem.length; i++){
      fd.append(elem[i].dataset.key,elem[i].value);
    }
    let edit = document.querySelector("#kat_id").value;
    let url = "<?php echo $url; ?>/admin/sharesvc_add";
    if(edit != ""){
        url = "<?php echo $url; ?>/admin/sharesvc_update?id=" + edit; 
    }
    let acts = await fetch(url,{
        method: "POST",
        body: fd
    });
    if(acts.ok){
        location.reload();
    }
}

async function load_data(){
  let usr = await fetch("<?php echo $url; ?>/admin/sharesvc_data");
  let usr_dt = await usr.json();
  if(usr.ok){
    let k = "";
    for(let i in usr_dt.rows){

      let fty = new FormData();
      fty.append("id",usr_dt.rows[i].type);
      let ty = await fetch("<?php echo $url; ?>/sys/reads?table=kategori_user",{
        method: "POST",
        body: fty
      });
      let dty = await ty.json();

      let fsv = new FormData();
      fsv.append("id",usr_dt.rows[i].service);
      let sv = await fetch("<?php echo $url; ?>/sys/reads?table=service",{
        method: "POST",
        body: fsv
      });
      let dsv = await sv.json();
    
      k += "<tr><td><button class='btn btn-sm btn-info' title='Edit' style='cursor: pointer' data-toggle='modal' data-target='#bootstrap'  onclick='edit(this.id)' id='edit-" + usr_dt.rows[i].id + "'><i class='fa fa-pencil'></i></button>&nbsp;&nbsp;<button class='btn btn-sm btn-danger' title='Hapus' style='cursor: pointer' onclick='hapus(this.id)' id='del-" + usr_dt.rows[i].id + "'><i class='fa fa-trash-o'></i></button></td><td>" + dty.rows[0].name + "</td><td>" + dsv.rows[0].name + "</td><td>" + usr_dt.rows[i].share_mitra + "</td><td>" + usr_dt.rows[i].share_advoqu + "</td><td>" + usr_dt.rows[i].real_tarif + "</td></tr>";
    }
    document.getElementById("d-kat").innerHTML = k;
    $(".zero-configuration").DataTable();
  } 
}
load_data();

async function load_module(){
  let mod = await fetch("<?php echo $url; ?>/admin/module?token=<?php echo $this->input->get("token"); ?>");
  let data = await mod.json();
  if(mod.ok){
    let li = "";
    for(let i in data.rows){
        li += "<li class='nav-item'><a href='<?php echo $url; ?>/" + data.rows[i].link + "?token=<?php echo $this->input->get("token"); ?>'><i class='fa fa-gear'></i><span class='menu-item'>" + data.rows[i].name + "</span></a></li>";
    }
    document.getElementById("main-menu-navigation").innerHTML = li;
  }
}
load_module();
</script>
        

<?php require "footer.php"; ?>
    
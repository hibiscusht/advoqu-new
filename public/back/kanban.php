<?php  
require "public/setup.php";  
require "header.php"; ?>

<style>
.hidden {
    display: none;
}

.kanban-elems {
  padding: 3%;
  border: none;
  border-radius: 8px;
  background: lightgreen;
  color: #557d44;
  font-weight: bold
}
</style>


<link rel="stylesheet" type="text/css" href="<?php echo $backbase; ?>/app-assets/vendors/css/forms/quill/quill.snow.css">

<link rel="stylesheet" type="text/css" href="<?php echo $backbase; ?>/app-assets/vendors/css/jkanban/jkanban.min.css">

<link rel="stylesheet" type="text/css" href="<?php echo $backbase; ?>/app-assets/css/pages/app-kanban.css">

<link rel="stylesheet" type="text/css" href="<?php echo $backbase; ?>/app-assets/vendors/css/pickers/pickadate/pickadate.css">


     <!-- BEGIN: Content-->
     <div class="app-content content">
      <div class="content-overlay"></div>
      <div class="content-wrapper">
        <div class="content-header row">

        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title mb-0">Data Advofeed</h3>
            <div class="row breadcrumbs-top">
              <div class="breadcrumb-wrapper col-12">
                <ol class="breadcrumb">
                  
                  <li class="breadcrumb-item">Home
                  </li>
                  <li class="breadcrumb-item active">Data Advofeed
                  </li>
                </ol>
              </div>
            </div>
            </div>
        </div>
        <div class="content-body">
 
<span id="feed">

<table class="table table-striped table-bordered zero-configuration">
                            <thead>
                                <tr>
                                <th>Aksi</th>
                                    <th>Klien</th>
                                    <th>Mitra Advoshare</th>
                                </tr>
                            </thead>
                            <tbody id="d-feed">
                        </tbody>
                    </table>
</span>

 

 <span id="kanban" class="hidden">       
        <!-- Basic Kanban App -->
<div class="kanban-overlay"></div>
<section id="kanban-wrapper">
  <div class="row">
    <div class="col-12">
      <button type="button" class="btn btn-primary mb-1" onclick="unload_feed()">
         <i class='fa fa-arrow-left'></i> Kembali
      </button>
      <div id="kanban-app"></div>
    </div>
  </div>

</section>

</span>


<!--/ Sample Project kanban -->
        </div>
      </div>
    </div>
    <!-- END: Content-->

<script>

function unload_feed(){
    document.querySelector("#feed").classList.remove("hidden");
    document.querySelector("#kanban").classList.add("hidden");
} 

 
let kanbans = "";


function trash(id){
  kanbans.removeElement(id);
}

async function load_feeds(id){

    document.querySelector("#feed").classList.add("hidden");
    document.querySelector("#kanban").classList.remove("hidden");
    let feeder = document.querySelector("#" + id).dataset.feeder; 

    document.querySelector("#kanban-app").innerHTML = "loading...";

    let fe = await fetch("<?php echo $url; ?>/admin/feed_status");
    let fed = await fe.json();
    if(fe.ok){
        let board = [];




    for(let bi in fed.rows){

      let items = [];
      if(typeof fed.rows[bi] != "undefined"){

        let fd = new FormData();
      fd.append("feed_status",fed.rows[bi].id);
      fd.append("feeder",feeder);
      let det = await fetch("<?php echo $url; ?>/admin/feed_detail",{
        method: "POST",
        body: fd
      });
      let detl = await det.json();

      if(detl.rows.length > 0){

        for(let j in detl.rows){
        if(typeof detl.rows[j] != "undefined"){
          let titles = "<h5 class='kanban-elems'><i class='fa fa-trash-o' onclick='trash(this.id)' id='" + j + "-el' style='cursor: pointer'></i> " + detl.rows[j].kasus + "</h5><hr/>" + detl.rows[j].ket;
          items.push( {
                        'id':j + "-el",
                        'title': titles,
                    });
      }
      }
 

      }
      }

      board.push(  {
                'id' : '_' + bi,
                'title'  :  fed.rows[bi].name,
                'item'  : items
            });
 
    }

    document.querySelector("#kanban-app").innerHTML = "";

    kanbans = new jKanban({
        element:'#kanban-app',
        boards  : board
    });
    }
   


} 

async function load_data(){
  let fd = new FormData();
  fd.append("partner","<?php echo $this->input->get("token"); ?>");
    let dt = await fetch("<?php echo $url; ?>/sys/reads?table=feeds",{
      method: "POST",
      body: fd
    });
    let dta = await dt.json();
    if(dt.ok){
        let k = "";
        for(let i in dta.rows){
            let kl = await fetch("<?php echo $url; ?>/auth/rbac?token=" + dta.rows[i].klien);
            let kln = await kl.json();
            let pr = await fetch("<?php echo $url; ?>/auth/rbac?token=" + dta.rows[i].partner);
            let prt = await pr.json();
            k += "<tr><td><button class='btn btn-success' onclick='load_feeds(this.id)' id='f-" + dta.rows[i].id + "' data-feeder='" + dta.rows[i].feed_detail + "'>Lihat Feed</button></td><td>" + kln.rows[0].fullname + "</td><td>" + prt.rows[0].fullname + "</td></tr>";
        }       
        document.querySelector("#d-feed").innerHTML = k;
        $(".zero-configuration").DataTable();
    }
}

load_data();

async function load_module(){
  let mod = await fetch("<?php echo $url; ?>/admin/module?token=<?php echo $this->input->get("token"); ?>");
  let data = await mod.json();
  if(mod.ok){
    let li = "";
    for(let i in data.rows){
        li += "<li class='nav-item'><a href='<?php echo $url; ?>/" + data.rows[i].link + "?token=<?php echo $this->input->get("token"); ?>'><i class='fa fa-gear'></i><span class='menu-item'>" + data.rows[i].name + "</span></a></li>";
    }
    document.getElementById("main-menu-navigation").innerHTML = li;
  }
}
load_module();

</script>

<script src="<?php echo $backbase; ?>/app-assets/vendors/js/pickers/pickadate/picker.js"></script>
    <script src="<?php echo $backbase; ?>/app-assets/vendors/js/pickers/pickadate/picker.date.js"></script>
<script src="<?php echo $backbase; ?>/app-assets/vendors/js/forms/quill/quill.js"></script>
 <script src="<?php echo $backbase; ?>/app-assets/vendors/js/jkanban/jkanban.min.js"></script>
 <!--<script src="<?php echo $backbase; ?>/app-assets/js/scripts/pages/app-kanban.js"></script>-->
        
<script>


</script>

<?php require "footer.php"; ?>
<?php  
require "public/setup.php";  
require "header.php"; ?>


<link rel="stylesheet" type="text/css" href="<?php echo $backbase; ?>/app-assets/vendors/css/forms/quill/katex.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $backbase; ?>/app-assets/vendors/css/forms/quill/monokai-sublime.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $backbase; ?>/app-assets/vendors/css/forms/quill/quill.snow.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $backbase; ?>/app-assets/vendors/css/forms/quill/quill.bubble.css">

     <!-- BEGIN: Content-->
     <div class="app-content content">
      <div class="content-overlay"></div>
      <div class="content-wrapper">
        <div class="content-header row">

        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title mb-0">Data Testimoni</h3>
            <div class="row breadcrumbs-top">
              <div class="breadcrumb-wrapper col-12">
                <ol class="breadcrumb">
                  
                  <li class="breadcrumb-item">Home
                  </li>
                  <li class="breadcrumb-item active">Data Testimoni
                  </li>
                </ol>
              </div>
            </div>
            </div>
        </div>
        <div class="content-body">

<button class="btn btn-success" data-toggle='modal' data-target='#bootstrap' onclick="clrall()">Tambah Data</button>

<br/> <br/>

<table class="table table-striped table-bordered zero-configuration">
                    <thead>
                        <tr>
                        <th>Aksi</th>
                            <th>Foto</th>
                            <th>Nama</th> 
                            <th>Jabatan</th> 
                            <th>Testimoni</th> 
                        </tr>
                    </thead>
                    <tbody id="d-faq">
                </tbody>
            </table>


</div>
<script>
    
function clrall(){ 
  let di = document.querySelectorAll(".clr");
  for(let j = 0; j < di.length; j++){ 
    if(di[j].dataset.key != "author"){
      di[j].value = ""; 
    }
  }
  document.querySelector("#usr_id").value = "";
}
</script>

    
<!-- Modal -->
<div class="modal fade text-left" id="bootstrap" tabindex="-1" role="dialog" aria-labelledby="myModalLabel35" aria-hidden="true">
									  <div class="modal-dialog" role="document">
										<div class="modal-content" style="height: 700px">
										  <div class="modal-header">
											<h3 class="modal-title" id="myModalLabel35"> Data Testimoni</h3>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											  <span aria-hidden="true">&times;</span>
											</button>
										  </div>
											<div class="modal-body" style="overflow-y: auto; height: 400px">
											
                    <input type="hidden" id="usr_id"/> 
                    <div class="form-group">
         <label>Nama</label>
         <input type="text" class="form-control clr" data-key="nama"/>
         </div>   
         <div class="form-group">
         <label>Jabatan</label>
         <input type="text" class="form-control clr" data-key="jabatan"/>
         </div>   

          <div class="form-group">
         <label>Testimoni</label>
         <textarea style="height: 150px" class="form-control clr" data-key="testi"></textarea>
         </div>             
         <div class="form-group">
         <label>Gambar</label>
         <input type="file" class="form-control clr" data-key="foto"/>
         </div>   

											</div>
											<div class="modal-footer">

                                            <button type="button" class="btnx btn btn-outline-secondary btn-lg" onclick="save_data()">Simpan</button>

												<input type="reset" class="btn btn-outline-secondary btn-lg" data-dismiss="modal" value="Tutup">
											
											</div>
										 
										</div>
									  </div>
									</div>



    <!-- BEGIN: Page Vendor JS-->
    <!-- END: Page Vendor JS-->

    <script>

        

async function hapus(id){
    let sw = await Swal.fire({title:"Hapus Kabar Advoqu",text:"Yakin hapus kabar advoqu?",type:"question",showCancelButton:!0,confirmButtonColor:"#3085d6",cancelButtonColor:"#d33",confirmButtonText:"Lanjutkan",confirmButtonClass:"btn btn-primary",cancelButtonClass:"btn btn-danger ml-1",buttonsStyling:!1});

// console.log(typeof sw.value);

  if(typeof sw.value !== "undefined"){
   document.getElementById(id).innerHTML = "<i class='fa fa-circle-o-notch fa-fw fa-spin'></i>";
   let j = id.split("-");
   let val = await fetch("<?php echo $url; ?>/admin/testimoni_delete?id=" + j[1]);
   if(val.ok){
     Swal.fire({type:"success",title:"Berhasil",text:"Data telah dihapus",confirmButtonClass:"btn btn-success"})
     .then(function(){
       location.reload();
     });
   }
 }  
}


async function edit(id){
    let ids = id.split("-");
    let usr = await fetch("<?php echo $url; ?>/admin/testimoni_data?id=" + ids[1]);
    let usr_dt = await usr.json(); 
    clrall();
    document.querySelector("#usr_id").value = ids[1];
    let elems = document.querySelectorAll(".clr");
    for(let i = 0; i < elems.length; i++){
      let keys = elems[i].dataset.key;
      elems[i].value = (keys == "foto")? "" : usr_dt.rows[0][keys];
    }
}

async function save_data(){
    document.querySelector(".btnx").setAttribute("disabled","");
    document.querySelector(".btnx").innerHTML = "<i class='fa fa-gear fa-spin'></i> menyimpan...";
    let fd = new FormData();
    let edit = document.querySelector("#usr_id").value;
    let elem = document.querySelectorAll(".clr");
    let foto_start = true;
    for(let i = 0; i < elem.length; i++){
        let form = elem[i];
        if(edit != ""){
            if(form.dataset.key == "foto"){
                if(form.value != ""){
                    fd.append("foto",form.files[0]);
                }
            }
            else {
            fd.append(form.dataset.key,form.value);
            }
        } else { 
            if(form.dataset.key == "foto"){
                if(form.value != ""){
                    fd.append("foto",form.files[0]);
                } else {
                    foto_start = false;
                }
            }
            else {
            fd.append(form.dataset.key,form.value);
            }
        }
    }

    if(foto_start){
        let url = (edit == "")? "<?php echo $url; ?>/admin/testimoni_add" : "<?php echo $url; ?>/admin/testimoni_update?id=" + edit;

        let dt = await fetch(url,{
            method: "POST",
            body: fd
        });
        if(dt.ok){
          location.reload();
        }

     } else {
        alert("foto testimoni wajib diisi");
        document.querySelector(".btnx").removeAttribute("disabled");
    document.querySelector(".btnx").innerHTML = "Simpan";
    }
   
}        


async function load_data(){
  document.querySelector("#d-faq").innerHTML = "<tr><td colspan=5><i class='fa fa-spin fa-spinner fa-fw'></i></td></tr>";
  let usr = await fetch("<?php echo $url; ?>/admin/testimoni_data");
  let usr_dt = await usr.json();
  if(usr.ok){
    let k = "";
    for(let i in usr_dt.rows){
 
      k += "<tr><td><button class='btn btn-sm btn-info' title='Edit' style='cursor: pointer' data-toggle='modal' data-target='#bootstrap'  onclick='edit(this.id)' id='edit-" + usr_dt.rows[i].id + "'><i class='fa fa-pencil'></i></button>&nbsp;&nbsp;<button class='btn btn-sm btn-danger' title='Hapus' style='cursor: pointer' onclick='hapus(this.id)' id='del-" + usr_dt.rows[i].id + "'><i class='fa fa-trash-o'></i></button></td><td><a href='<?php echo $url; ?>/public/uploaded/" + usr_dt.rows[i].foto + "' target='_BLANK'><i class='fa fa-external-link'></i> Buka</a></td><td>" + usr_dt.rows[i].nama + "</td><td>" + usr_dt.rows[i].jabatan + "</td><td>" + (usr_dt.rows[i].testi).substring(0,25) + "...</td></tr>";
    }
    document.getElementById("d-faq").innerHTML = k;
   $(".zero-configuration").DataTable();
  } 
}
load_data();
        
async function load_module(){
  let mod = await fetch("<?php echo $url; ?>/admin/module?token=<?php echo $this->input->get("token"); ?>");
  let data = await mod.json();
  if(mod.ok){
    let li = "";
    for(let i in data.rows){
        li += "<li class='nav-item'><a href='<?php echo $url; ?>/" + data.rows[i].link + "?token=<?php echo $this->input->get("token"); ?>'><i class='fa fa-gear'></i><span class='menu-item'>" + data.rows[i].name + "</span></a></li>";
    }
    document.getElementById("main-menu-navigation").innerHTML = li;
  }
}
load_module();
</script>

<?php require "footer.php"; ?>
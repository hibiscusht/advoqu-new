<?php  
require "public/setup.php";  
require "header.php"; ?>

     <!-- BEGIN: Content-->
     <div class="app-content content">
      <div class="content-overlay"></div>
      <div class="content-wrapper">
        <div class="content-header row">

        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title mb-0">Kategori Klien & Mitra</h3>
            <div class="row breadcrumbs-top">
              <div class="breadcrumb-wrapper col-12">
                <ol class="breadcrumb">
                  
                  <li class="breadcrumb-item">Home
                  </li>
                  <li class="breadcrumb-item active">Kategori Klien & Mitra
                  </li>
                </ol>
              </div>
            </div>
            </div>
        </div>
        <div class="content-body">

        <button class="btn btn-success" data-toggle='modal' data-target='#bootstrap' onclick="clrall()">Tambah Data</button>

<br/> <br/>

        <table class="table table-striped table-bordered zero-configuration">
                            <thead>
                                <tr>
                                <th>Aksi</th>
                                    <th>Kategori</th>
                                </tr>
                            </thead>
                            <tbody id="d-kat">
                        </tbody>
                    </table>

        </div>
      </div>
    </div>
    <!-- END: Content-->


    
<!-- Modal -->
<div class="modal fade text-left" id="bootstrap" tabindex="-1" role="dialog" aria-labelledby="myModalLabel35" aria-hidden="true">
									  <div class="modal-dialog" role="document">
										<div class="modal-content">
										  <div class="modal-header">
											<h3 class="modal-title" id="myModalLabel35"> Kategori Klien & Mitra</h3>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											  <span aria-hidden="true">&times;</span>
											</button>
										  </div>
											<div class="modal-body kategori" style="overflow-y: auto;">
											 <div>
                    <input type="hidden" id="kat_id" />
         <div class="form-group">
         <label>Kategori</label>
        <input type="text" id="kat" class="form-control clr" />
         </div>             


											</div>
											<div class="modal-footer">

                                            <button type="button" class="btnx btn btn-outline-secondary btn-lg" onclick="save_data()">Simpan</button>

												<input type="reset" class="btn btn-outline-secondary btn-lg" data-dismiss="modal" value="Tutup">
											
											</div>
										 
										</div>
									  </div>
									</div>


<script>


function clrall(){
  let d = document.querySelectorAll(".clr");
  for(let i = 0; i < d.length; i++){
    d[i].value = "";
  }
  
  document.querySelector("#kat_id").value = "";
}

async function hapus(id){
    let sw = await Swal.fire({title:"Hapus Kategori",text:"Yakin hapus kategori?",type:"question",showCancelButton:!0,confirmButtonColor:"#3085d6",cancelButtonColor:"#d33",confirmButtonText:"Lanjutkan",confirmButtonClass:"btn btn-primary",cancelButtonClass:"btn btn-danger ml-1",buttonsStyling:!1});

// console.log(typeof sw.value);

  if(typeof sw.value !== "undefined"){
   document.getElementById(id).innerHTML = "<i class='fa fa-circle-o-notch fa-fw fa-spin'></i>";
   let j = id.split("-");
   let val = await fetch("<?php echo $url; ?>/admin/kategori_delete?id=" + j[1]);
   if(val.ok){
     Swal.fire({type:"success",title:"Berhasil",text:"Data telah dihapus",confirmButtonClass:"btn btn-success"})
     .then(function(){
       location.reload();
     });
   }
 }  
}


async function edit(id){
    let ids = id.split("-");
    let usr = await fetch("<?php echo $url; ?>/admin/kategori_data?id=" + ids[1]);
    let usr_dt = await usr.json(); 
    clrall();
    document.querySelector("#kat_id").value = ids[1];
    document.querySelector("#kat").value = usr_dt.rows[0].name; 
}


async function save_data(){
    document.querySelector(".btnx").setAttribute("disabled","");
    document.querySelector(".btnx").innerHTML = "<i class='fa fa-gear fa-spin'></i> menyimpan...";
    let fd = new FormData();
    fd.append("name",document.querySelector("#kat").value); 
    let edit = document.querySelector("#kat_id").value;
    let url = "<?php echo $url; ?>/admin/kategori_add";
    if(edit != ""){
        url = "<?php echo $url; ?>/admin/kategori_update?id=" + edit; 
    }
    let acts = await fetch(url,{
        method: "POST",
        body: fd
    });
    if(acts.ok){
        location.reload();
    }
}

async function load_data(){
  let usr = await fetch("<?php echo $url; ?>/admin/kategori_data");
  let usr_dt = await usr.json();
  if(usr.ok){
    let k = "";
    for(let i in usr_dt.rows){
    
      k += "<tr><td><button class='btn btn-sm btn-info' title='Edit' style='cursor: pointer' data-toggle='modal' data-target='#bootstrap'  onclick='edit(this.id)' id='edit-" + usr_dt.rows[i].id + "'><i class='fa fa-pencil'></i></button>&nbsp;&nbsp;<button class='btn btn-sm btn-danger' title='Hapus' style='cursor: pointer' onclick='hapus(this.id)' id='del-" + usr_dt.rows[i].id + "'><i class='fa fa-trash-o'></i></button></td><td>" + usr_dt.rows[i].name + "</td></tr>";
    }
    document.getElementById("d-kat").innerHTML = k;
    $(".zero-configuration").DataTable();
  } 
}
load_data();

async function load_module(){
  let mod = await fetch("<?php echo $url; ?>/admin/module?token=<?php echo $this->input->get("token"); ?>");
  let data = await mod.json();
  if(mod.ok){
    let li = "";
    for(let i in data.rows){
        li += "<li class='nav-item'><a href='<?php echo $url; ?>/" + data.rows[i].link + "?token=<?php echo $this->input->get("token"); ?>'><i class='fa fa-gear'></i><span class='menu-item'>" + data.rows[i].name + "</span></a></li>";
    }
    document.getElementById("main-menu-navigation").innerHTML = li;
  }
}
load_module();
</script>
        

<?php require "footer.php"; ?>
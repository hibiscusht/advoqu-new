  <!-- BEGIN: Footer-->
  <footer class="footer footer-static footer-light navbar-border">
      <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2"><span class="float-md-left d-block d-md-inline-block">Copyright  &copy; 2020 <a class="text-bold-800 grey darken-2" href="https://1.envato.market/pixinvent_portfolio" target="_blank">PIXINVENT 			</a></span><span class="float-md-right d-none d-lg-block">Hand-crafted & Made with <i class="feather icon-heart pink"></i></span></p>
    </footer>
    <!-- END: Footer-->

<script>

async function rbac(){
  let a = await fetch("<?php echo $url; ?>/auth/rbac?token=<?php echo $this->input->get("token"); ?>");
  let b = await a.json();
  if(a.ok){
    document.getElementsByClassName("user-name")[0].innerHTML = b.rows[0].fullname;
    if(b.rows[0].type == "1"){
      document.getElementById("out").href = "<?php echo $url; ?>/admin/login";
    } else {
      
      document.querySelector(".member").classList.remove("hidden");
      document.getElementById("out").href = "<?php echo $url; ?>";
    }
    
  }
}

rbac();

</script>

  

    <!-- BEGIN: Page Vendor JS-->
    <script src="<?php echo $backbase;?>/app-assets/vendors/js/charts/apexcharts/apexcharts.min.js"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="<?php echo $backbase;?>/app-assets/js/core/app-menu.min.js"></script>
    <script src="<?php echo $backbase;?>/app-assets/js/core/app.min.js"></script>
    <script src="<?php echo $backbase;?>/app-assets/js/scripts/customizer.min.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="<?php echo $backbase;?>/app-assets/js/scripts/cards/card-statistics.min.js"></script>
    <!-- END: Page JS-->

    <script src="<?php echo $backbase;?>/app-assets/vendors/js/tables/datatable/datatables.min.js"></script>

  </body>
  <!-- END: Body-->

<!-- Mirrored from pixinvent.com/stack-responsive-bootstrap-4-admin-template/html/ltr/vertical-modern-menu-template/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 22 Nov 2020 02:13:31 GMT -->
</html>
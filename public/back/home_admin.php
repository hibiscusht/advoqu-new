<?php  
require "public/setup.php";  
require "header.php"; ?>

     <!-- BEGIN: Content-->
     <div class="app-content content">
      <div class="content-overlay"></div>
      <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">

        </div>
      </div>
    </div>
    <!-- END: Content-->

<script>
async function load_module(){
  let mod = await fetch("<?php echo $url; ?>/admin/module?token=<?php echo $this->input->get("token"); ?>");
  let data = await mod.json();
  if(mod.ok){
    let li = "";
    for(let i in data.rows){
      li += "<li class='nav-item'><a href='<?php echo $url; ?>/" + data.rows[i].link + "?token=<?php echo $this->input->get("token"); ?>'><i class='fa fa-gear'></i><span class='menu-item'>" + data.rows[i].name + "</span></a></li>";
    }
    document.getElementById("main-menu-navigation").innerHTML = li;
  }
}
load_module();
</script>

<?php require "footer.php"; ?>
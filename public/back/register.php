<?php require "public/setup.php"; ?>

<!DOCTYPE html>
<!--
Template Name: Stack - Stack - Bootstrap 4 Admin Template
Author: PixInvent
Website: http://www.pixinvent.com/ 
-->
<html class="loading" lang="en" data-textdirection="ltr">
  <!-- BEGIN: Head-->
  
<!-- Mirrored from pixinvent.com/stack-responsive-bootstrap-4-admin-template/html/ltr/vertical-modern-menu-template/register-with-bg-image.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 22 Nov 2020 02:15:43 GMT -->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta property="og:title" content="Advoqu" />
		<meta property="og:description" content="Advoqu | Layanan Hukum dalam Genggaman" />
		<meta property="og:image" content="<?php echo $backbase; ?>/img/advoqu_logo.png" />

		<!-- Favicon -->
		<link rel="shortcut icon" href="<?php echo $backbase; ?>/img/advicon.ico" type="image/x-icon" />
		<link rel="apple-touch-icon" href="<?php echo $backbase; ?>/img/advicon.ico">
    <title>R Subekti Law Office | ADVOQU</title> 
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo $backbase; ?>/app-assets/vendors/css/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $backbase; ?>/app-assets/vendors/css/forms/icheck/icheck.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $backbase; ?>/app-assets/vendors/css/forms/icheck/custom.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo $backbase; ?>/app-assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $backbase; ?>/app-assets/css/bootstrap-extended.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $backbase; ?>/app-assets/css/colors.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $backbase; ?>/app-assets/css/components.min.css">
    <!-- END: Theme CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo $backbase; ?>/app-assets/css/core/menu/menu-types/vertical-menu-modern.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $backbase; ?>/app-assets/css/core/colors/palette-gradient.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $backbase; ?>/app-assets/css/pages/login-register.min.css">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo $backbase; ?>/assets/css/style.css">
    <!-- END: Custom CSS-->

  </head>
  <!-- END: Head-->

  <!-- BEGIN: Body-->
  <body class="vertical-layout vertical-menu-modern 1-column  bg-full-screen-image blank-page blank-page" data-open="click" data-menu="vertical-menu-modern" data-col="1-column">
    <!-- BEGIN: Content-->
    <div class="app-content content">
      <div class="content-overlay"></div>
      <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body"><section class="row flexbox-container">
    <div class="col-12 d-flex align-items-center justify-content-center">
        <div class="col-lg-4 col-md-8 col-10 box-shadow-2 p-0">
            <div class="card border-grey border-lighten-3 px-1 py-1 m-0">
                <div class="card-header border-0 pb-0">
                    <div class="card-title text-center">
                    <img src="<?php echo $backbase; ?>/img/advoqu_logo.png" alt="branding logo" style="width: 25%">   </div>
                 <!--   <h6 class="card-subtitle line-on-side text-muted text-center font-small-3 pt-2"><span>Easily
                            Using</span></h6> -->
                </div>
              <!--  <div class="card-content">
                    <div class="text-center">
                        <a href="#" class="btn btn-social-icon mr-1 mb-1 btn-outline-facebook"><span
                                class="fa fa-facebook"></span></a>
                        <a href="#" class="btn btn-social-icon mr-1 mb-1 btn-outline-twitter"><span
                                class="fa fa-twitter"></span></a>
                        <a href="#" class="btn btn-social-icon mr-1 mb-1 btn-outline-linkedin"><span
                                class="fa fa-linkedin font-medium-4"></span></a>
                        <a href="#" class="btn btn-social-icon mr-1 mb-1 btn-outline-github"><span
                                class="fa fa-github font-medium-4"></span></a>
                    </div>
                    <p class="card-subtitle line-on-side text-muted text-center font-small-3 mx-2 my-1"><span>OR Using
                            Email</span></p> -->
                    <div class="card-body">
                    <fieldset class="form-group position-relative has-icon-left" id="info" style="background: lightgreen; color: darkgreen; font-weight: bold; padding: 1%; display: none">
                    <label><center> <i class="fa fa-check-square fa-2x"></i></center> Pendaftaran Berhasil! Anda akan dialihkan ke halaman login</label>
                                <div class="form-control-position">
                                   
                                </div>
                            </fieldset>
                      
                            <fieldset class="form-group position-relative has-icon-left">
                                <input type="text" class="form-control" id="f" placeholder="Full Name">
                                <div class="form-control-position">
                                    <i class="fa fa-user"></i>
                                </div>
                            </fieldset>
                            <fieldset class="form-group position-relative has-icon-left">
                                <input type="email" class="form-control" id="u"
                                    placeholder="Your Email Address" onblur="checkmail()" required>
                                    <small><span id="err" style="color: red"></span></small>
                                <div class="form-control-position">
                                    <i class="fa fa-envelope"></i>
                                </div>
                            </fieldset>
                            <fieldset class="form-group position-relative has-icon-left">
                                <input type="password" class="form-control" id="p"
                                    placeholder="Enter Password" required>
                                <div class="form-control-position">
                                    <i class="fa fa-key"></i>
                                </div>
                            </fieldset><!--
                            <div class="form-group row">
                                <div class="col-sm-6 col-12 text-center text-sm-left pr-0">
                                    <fieldset>
                                        <input type="checkbox" id="remember-me" class="chk-remember">
                                        <label for="remember-me"> Remember Me</label>
                                    </fieldset>
                                </div>
                                <div class="col-sm-6 col-12 float-sm-left text-center text-sm-right"><a
                                        href="recover-password.html" class="card-link">Forgot Password?</a></div>
                            </div> -->
                            <button type="button" class="btn btn-outline-primary btn-block" onclick="register()"><i
                                    class="fa fa-sign-in"></i> Register</button>
                      
                      <!--  <a href="login-with-bg-image.html" class="btn btn-outline-danger btn-block mt-2"><i
                                class="feather icon-unlock"></i> Login</a> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
        </div>
      </div>
    </div>
    <!-- END: Content-->

    <script>

    function checkmail(){
        document.getElementById("err").innerHTML = "";
        let check = document.getElementById("u").value;
        let mail = RegExp(".*@.*\..*","g");
        let chk = mail.test(check);
        if(!chk){
            document.getElementById("err").innerHTML = "alamat email salah";
            document.getElementById("u").focus();
        }
    }

    async function register(){
        let btn = document.getElementsByClassName("btn")[0];
        btn.setAttribute("disabled",true);
        let fd = new FormData();
        fd.append("username",document.getElementById("u").value);
        fd.append("password",document.getElementById("p").value);
        fd.append("fullname",document.getElementById("f").value);
        fd.append("cat","1");
        btn.innerHTML = "<i class='fa fa-spin fa-cog'></i> processing...";
        let a = await fetch("<?php echo $url; ?>/auth/register",{
            method: "POST",
            body: fd
        });
        if(a.ok){
            document.getElementById("info").style.display = "block";
            location.href = "<?php echo $url; ?>/main/login";
        }
    }

    </script>

    <!-- BEGIN: Vendor JS-->
    <script src="<?php echo $backbase; ?>/app-assets/vendors/js/vendors.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="<?php echo $backbase; ?>/app-assets/vendors/js/forms/validation/jqBootstrapValidation.js"></script>
    <script src="<?php echo $backbase; ?>/app-assets/vendors/js/forms/icheck/icheck.min.js"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="<?php echo $backbase; ?>/app-assets/js/core/app-menu.min.js"></script>
    <script src="<?php echo $backbase; ?>/app-assets/js/core/app.min.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="<?php echo $backbase; ?>/app-assets/js/scripts/forms/form-login-register.min.js"></script>
    <!-- END: Page JS-->

  </body>
  <!-- END: Body-->

<!-- Mirrored from pixinvent.com/stack-responsive-bootstrap-4-admin-template/html/ltr/vertical-modern-menu-template/register-with-bg-image.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 22 Nov 2020 02:15:43 GMT -->
</html>
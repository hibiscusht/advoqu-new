<?php  
require "public/setup.php";  
require "header.php"; ?>

     <!-- BEGIN: Content-->
     <div class="app-content content">
      <div class="content-overlay"></div>
      <div class="content-wrapper">
        <div class="content-header row">

        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title mb-0">Profile Klien</h3>
            <div class="row breadcrumbs-top">
              <div class="breadcrumb-wrapper col-12">
                <ol class="breadcrumb">
                  
                  <li class="breadcrumb-item">Home
                  </li>
                  <li class="breadcrumb-item active">Profile Klien
                  </li>
                </ol>
              </div>
            </div>
            </div>
        </div>
        <div class="content-body">
<div class="row main-profile-client">
<div class="col-md-12">
<div class="form-group">
<div class="col-md-6">Alamat</div>
<div class="col-md-6"><input type="text" class="form-control prof"/></div>
</div>
<div class="form-group">
<div class="col-md-6">No HP</div>
<div class="col-md-6"><input type="text" class="form-control prof"/></div>
</div>
<div class="form-group">
<button class="btn btn-info btnx" onclick="simpan()">Simpan</button>
</div>
</div>
</div>

<div class="row verified-profile-client hidden">
  verified
</div>

<div class="row validated-profile-client hidden">
 validated
</div>
        </div>
      </div>
    </div>
    <!-- END: Content-->

 

<script>

$(document).ready(function(){
 
});

async function simpan(){
    document.getElementsByClassName("btnx")[0].setAttribute("disabled","");
    document.getElementsByClassName("btnx")[0].innerHTML = "<i class='fa fa-circle-o-notch fa-spin'></i> mengirim...";
        let u = document.getElementsByClassName("prof"); 
        let fd = new FormData();
        fd.append("alamat",u[0].value);
        fd.append("no_hp",u[1].value); 
        let wt = await fetch("<?php echo $url; ?>/admin/klien_update?token=<?php echo $this->input->get("token"); ?>",{
            method: "POST",
            body: fd
        });
        if(wt.ok){
            Swal.fire({title:"Berhasil!",text:"Data anda telah disimpan",type:"success",confirmButtonClass:"btn btn-success",buttonsStyling:!1})
    .then(function(){
        document.getElementsByClassName("btnx")[0].removeAttribute("disabled");
        document.getElementsByClassName("btnx")[0].innerHTML = "Simpan";
    });
        }
}

async function load_data(){
  
  let dflt = document.querySelector(".main-profile-client")
    dflt.classList.add("hidden")
  //cek status client apakah sudah verifikasi atau aktivasi
  let q = await fetch("<?php echo $url; ?>/admin/klien_checkstatus?token=<?php echo $this->input->get("token"); ?>")
  let r = await q.json()
  if(q.ok){
    switch(r.rows[0].status){
      case "1": document.querySelector(".verified-profile-client").classList.remove("hidden"); break;
      case "2": document.querySelector(".validated-profile-client").classList.remove("hidden"); break;
      default: dflt.classList.remove("hidden"); break;
    }
  }
}
load_data();

async function load_module(){
  let mod = await fetch("<?php echo $url; ?>/admin/module?token=<?php echo $this->input->get("token"); ?>");
  let data = await mod.json();
  if(mod.ok){
    let li = "";
    for(let i in data.rows){
        li += "<li class='nav-item'><a href='<?php echo $url; ?>/" + data.rows[i].link + "?token=<?php echo $this->input->get("token"); ?>'><i class='fa fa-gear'></i><span class='menu-item'>" + data.rows[i].name + "</span></a></li>";
    }
    document.getElementById("main-menu-navigation").innerHTML = li;
  }
}
load_module();
</script>

<?php require "footer.php"; ?>
<?php  
require "public/setup.php";  
require "header.php"; ?>

     <!-- BEGIN: Content-->
     <div class="app-content content">
      <div class="content-overlay"></div>
      <div class="content-wrapper">
        <div class="content-header row">

        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title mb-0">Layanan Advoqu</h3>
            <div class="row breadcrumbs-top">
              <div class="breadcrumb-wrapper col-12">
                <ol class="breadcrumb">
                  
                  <li class="breadcrumb-item">Home
                  </li>
                  <li class="breadcrumb-item active">Layanan Advoqu
                  </li>
                </ol>
              </div>
            </div>
            </div>
        </div>
        <div class="content-body">

        <button class="btn btn-success" data-toggle='modal' data-target='#bootstrap' onclick="clrall()">Tambah Data</button>

<br/> <br/>

        <table class="table table-striped table-bordered zero-configuration">
                            <thead>
                                <tr>
                                <th>Aksi</th>
                                <th>Nama Layanan</th>
                                <th>Deskripsi</th>
                                <th>Link Kabar Advoqu</th>
                                <th>Icon</th>
                                </tr>
                            </thead>
                            <tbody id="d-kat">
                        </tbody>
                    </table>

        </div>
      </div>
    </div>
    <!-- END: Content-->


    
<!-- Modal -->
<div class="modal fade text-left" id="bootstrap" tabindex="-1" role="dialog" aria-labelledby="myModalLabel35" aria-hidden="true">
									  <div class="modal-dialog" role="document">
										<div class="modal-content">
										  <div class="modal-header">
											<h3 class="modal-title" id="myModalLabel35">Layanan Advoqu</h3>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											  <span aria-hidden="true">&times;</span>
											</button>
										  </div>
											<div class="modal-body kategori" style="overflow-y: auto;">
											 <div>
                    <input type="hidden" id="kat_id" />
         <div class="form-group">
         <label>Nama Layanan</label>
        <input type="text" class="form-control clr" data-key="name"/>
         </div>     
         <div class="form-group">
         <label>Deskripsi</label>
        <textarea class="form-control clr" data-key="descs"></textarea>
         </div>     
         <div class="form-group">
         <label>Icon</label>
        <select class="form-control clr" data-key="icon">
        <option value="haki.png">HAKI</option>
        <option value="notariat.png">Notariat</option>
        <option value="pemilukada.png">Pemilukada</option>
        <option value="pengadilan-agama.png">Pengadilan Agama</option>
        <option value="perizinan.png">Perizinan</option>
        <option value="perusahaan.png">Perusahaan</option>
        <option value="pidana-hukum.png">Pidana Umum</option>
        <option value="properti.png">Properti</option>
        <option value="sengketa-perdata.png">Sengketa Perdata</option>
        </select>
         </div>     
         <div class="form-group">
         <label>Link Kabar Advoqu <small>ketik untuk mencari dan klik pada daftar yang cocok</small></label>
         <input type="hidden" class="clr data-search" data-key="slug"/>
        <input type="text" class="form-control clr search" oninput="auths()" data-key="news"/>
        <span class="result"></span>
         </div>             


											</div>
											<div class="modal-footer">

                                            <button type="button" class="btnx btn btn-outline-secondary btn-lg" onclick="save_data()">Simpan</button>

												<input type="reset" class="btn btn-outline-secondary btn-lg" data-dismiss="modal" value="Tutup">
											
											</div>
										 
										</div>
									  </div>
									</div>


<script>

function useIt(id){
    document.querySelector(".data-search").value = id;
    document.querySelector(".search").value = document.querySelector("#" + id).innerHTML;
    document.querySelector(".result").innerHTML = "";
}

async function auths(){
    document.querySelector(".result").innerHTML = "searching...";
        let entry = document.querySelector(".search");
        let k = "";
        if(entry.value.length > 3){
            let fd = new FormData();
            fd.append("title",entry.value);
            let dt = await fetch("<?php echo $url; ?>/sys/read_like?table=news",{
                method:"POST",
                body:fd
            });
            let dta = await dt.json();
            if(dt.ok){
                for(let i in dta.rows){
                    k += "<p style='padding: 1%; background: blue; color: white; cursor: pointer' onclick='useIt(this.id)' id='" + dta.rows[i].slug + "'>" + dta.rows[i].title + "</p>";
                }
                if(k == ""){
                    document.querySelector(".result").innerHTML = "no result";
                } else {
                    document.querySelector(".result").innerHTML = k;
                }
                }
        } else if(entry.value.length == 0){
            document.querySelector(".result").innerHTML = "";
            } else {
            document.querySelector(".result").innerHTML = "searching...";
        }
       
    }



function clrall(){
  let d = document.querySelectorAll(".clr");
  for(let i = 0; i < d.length; i++){
    d[i].value = "";
  }
  
  document.querySelector("#kat_id").value = "";
}

async function hapus(id){
    let sw = await Swal.fire({title:"Hapus Layanan",text:"Yakin hapus layanan?",type:"question",showCancelButton:!0,confirmButtonColor:"#3085d6",cancelButtonColor:"#d33",confirmButtonText:"Lanjutkan",confirmButtonClass:"btn btn-primary",cancelButtonClass:"btn btn-danger ml-1",buttonsStyling:!1});

// console.log(typeof sw.value);

  if(typeof sw.value !== "undefined"){
   document.getElementById(id).innerHTML = "<i class='fa fa-circle-o-notch fa-fw fa-spin'></i>";
   let j = id.split("-");
   let val = await fetch("<?php echo $url; ?>/admin/svc_delete?id=" + j[1]);
   if(val.ok){
     Swal.fire({type:"success",title:"Berhasil",text:"Data telah dihapus",confirmButtonClass:"btn btn-success"})
     .then(function(){
       location.reload();
     });
   }
 }  
}


async function edit(id){
    let ids = id.split("-");
    let usr = await fetch("<?php echo $url; ?>/admin/svc_data?id=" + ids[1]);
    let usr_dt = await usr.json(); 
    clrall();
    document.querySelector("#kat_id").value = ids[1];
    let elem = document.querySelectorAll(".clr");
    for(let i = 0; i < elem.length; i++){ 
      elem[i].value = usr_dt.rows[0][elem[i].dataset.key];
    }
}


async function save_data(){
    document.querySelector(".btnx").setAttribute("disabled","");
    document.querySelector(".btnx").innerHTML = "<i class='fa fa-gear fa-spin'></i> menyimpan...";
    let fd = new FormData();
    let elem = document.querySelectorAll(".clr");
    for(let i = 0; i < elem.length; i++){
        fd.append(elem[i].dataset.key,elem[i].value);
    } 
    let edit = document.querySelector("#kat_id").value;
    let url = "<?php echo $url; ?>/admin/svc_add";
    if(edit != ""){
        url = "<?php echo $url; ?>/admin/svc_update?id=" + edit; 
    }
    let acts = await fetch(url,{
        method: "POST",
        body: fd
    });
    if(acts.ok){
        location.reload();
    }
}

async function load_data(){
  let usr = await fetch("<?php echo $url; ?>/admin/svc_data");
  let usr_dt = await usr.json();
  if(usr.ok){
    let k = "";
    for(let i in usr_dt.rows){
    
      k += "<tr><td><button class='btn btn-sm btn-info' title='Edit' style='cursor: pointer' data-toggle='modal' data-target='#bootstrap'  onclick='edit(this.id)' id='edit-" + usr_dt.rows[i].id + "'><i class='fa fa-pencil'></i></button>&nbsp;&nbsp;<button class='btn btn-sm btn-danger' title='Hapus' style='cursor: pointer' onclick='hapus(this.id)' id='del-" + usr_dt.rows[i].id + "'><i class='fa fa-trash-o'></i></button></td><td>" + usr_dt.rows[i].name + "</td><td>" + usr_dt.rows[i].descs + "</td><td>" + usr_dt.rows[i].news + "</td><td><img src='<?php echo $url; ?>/public/front/img/icon/" + usr_dt.rows[i].icon + "' style='width: 100px'></td></tr>";
    }
    document.getElementById("d-kat").innerHTML = k;
    $(".zero-configuration").DataTable();
  } 
}
load_data();

async function load_module(){
  let mod = await fetch("<?php echo $url; ?>/admin/module?token=<?php echo $this->input->get("token"); ?>");
  let data = await mod.json();
  if(mod.ok){
    let li = "";
    for(let i in data.rows){
        li += "<li class='nav-item'><a href='<?php echo $url; ?>/" + data.rows[i].link + "?token=<?php echo $this->input->get("token"); ?>'><i class='fa fa-gear'></i><span class='menu-item'>" + data.rows[i].name + "</span></a></li>";
    }
    document.getElementById("main-menu-navigation").innerHTML = li;
  }
}
load_module();
</script>
        

<?php require "footer.php"; ?>
<?php  
require "public/setup.php";  
require "header.php"; ?>

     <!-- BEGIN: Content-->
     <div class="app-content content">
      <div class="content-overlay"></div>
      <div class="content-wrapper">
        <div class="content-header row">

        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title mb-0">Data Klien</h3>
            <div class="row breadcrumbs-top">
              <div class="breadcrumb-wrapper col-12">
                <ol class="breadcrumb">
                  
                  <li class="breadcrumb-item">Home
                  </li>
                  <li class="breadcrumb-item active">Data Klien
                  </li>
                </ol>
              </div>
            </div>
            </div>
        </div>
        <div class="content-body">

        <table class="table table-striped table-bordered zero-configuration">
                            <thead>
                                <tr>
                                <th>Aksi</th>
                                <th>Kategori Klien</th> 
                                    <th>Nama Klien</th>
                                    <th>Username/Email</th>
                                    <th>Status</th>  
                                </tr>
                            </thead>
                            <tbody id="d-klien">
                        </tbody>
                    </table>


        </div>
      </div>
    </div>
    <!-- END: Content-->


<!-- Modal -->
<div class="modal fade text-left" id="bootstrap" tabindex="-1" role="dialog" aria-labelledby="myModalLabel35" aria-hidden="true">
									  <div class="modal-dialog" role="document">
										<div class="modal-content">
										  <div class="modal-header">
											<h3 class="modal-title" id="myModalLabel35"> Data Klien</h3>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											  <span aria-hidden="true">&times;</span>
											</button>
										  </div>
											<div class="modal-body data-klien" style="overflow-y: auto; height: 400px">
											 <div>
                       <i class="fa fa-spin fa-spinner fa-1x text-blue"></i> memuat data...
                       </div>
											</div>
											<div class="modal-footer">
												<input type="reset" class="btn btn-outline-secondary btn-lg" data-dismiss="modal" value="Tutup">
											
											</div>
										 
										</div>
									  </div>
									</div>


<script>

$(document).ready(function(){
  const ps = new PerfectScrollbar(".data-klien");
});


async function hapus(id){
    let sw = await Swal.fire({title:"Hapus Klien",text:"Yakin hapus klien?",type:"question",showCancelButton:!0,confirmButtonColor:"#3085d6",cancelButtonColor:"#d33",confirmButtonText:"Lanjutkan",confirmButtonClass:"btn btn-primary",cancelButtonClass:"btn btn-danger ml-1",buttonsStyling:!1});

// console.log(typeof sw.value);

  if(typeof sw.value !== "undefined"){
   document.getElementById(id).innerHTML = "<i class='fa fa-circle-o-notch fa-fw fa-spin'></i>";
   let j = id.split("-");
   let val = await fetch("<?php echo $url; ?>/admin/klien_delete?id=" + j[1]);
   if(val.ok){
     Swal.fire({type:"success",title:"Berhasil",text:"Data telah dihapus",confirmButtonClass:"btn btn-success"})
     .then(function(){
      location.reload();
     });
   }
 }  
}

async function load_data(){
  let usr = await fetch("<?php echo $url; ?>/admin/klien_data");
  let usr_dt = await usr.json();
  if(usr.ok){
    let k = "";
    for(let i in usr_dt.rows){
      let status = (usr_dt.rows[i].status == "1")? "<span class='badge badge-success'>Aktif</span>" : "<span class='badge badge-danger'>Non Aktif</span>";  

      let fw = new FormData();
      fw.append("id",usr_dt.rows[i].cat);
      let ca = await fetch("<?php echo $url; ?>/sys/reads?table=kategori_user",{
        method: "POST",
        body: fw
      });
      let cat = await ca.json();
     
      k += "<tr><td><button class='btn btn-sm btn-info' title='Detail' style='cursor: pointer' data-toggle='modal' data-target='#bootstrap' onclick='lihat_detail(this.id)' id='detail-" + usr_dt.rows[i].usr_id + "'><i class='fa fa-binoculars'></i></button>&nbsp;&nbsp;<button class='btn btn-sm btn-danger' title='Hapus' style='cursor: pointer'  onclick='hapus(this.id)' id='del-" + usr_dt.rows[i].usr_id + "'><i class='fa fa-trash-o'></i></button>&nbsp;&nbsp;<button class='btn btn-sm btn-success' title='Aktifkan/Non Aktifkan' style='cursor: pointer' onclick='ubah_status(this.id)' id='" + usr_dt.rows[i].usr_id + "'><i class='fa fa-check-square-o'></i></button>&nbsp;&nbsp;<button class='btn btn-sm btn-warning' title='Validasi' style='cursor: pointer' onclick='validasi(this.id)' id='valid-" + usr_dt.rows[i].usr_id + "'><i class='fa fa-arrow-up'></i></button></td><td>" + cat.rows[0].name + "</td><td>" + usr_dt.rows[i].fullname + "</td><td>" + usr_dt.rows[i].username + "</td><td>" + status + "</td></tr>";
    }
    document.getElementById("d-klien").innerHTML = k;
    $(".zero-configuration").DataTable();
  }
}
load_data();

async function validasi(id){
  let sw = await Swal.fire({title:"Atur Validasi Client",text:"Klien yang tervalidasi akan dapat membeli layanan Advoqu",type:"info",showCancelButton:!0,confirmButtonColor:"#3085d6",cancelButtonColor:"#d33",confirmButtonText:"Lanjutkan",confirmButtonClass:"btn btn-primary",cancelButtonClass:"btn btn-danger ml-1",buttonsStyling:!1});

 // console.log(typeof sw.value);

   if(typeof sw.value !== "undefined"){
    document.getElementById(id).innerHTML = "<i class='fa fa-circle-o-notch fa-fw fa-spin'></i>";
    let j = id.split("-");
    let val = await fetch("<?php echo $url; ?>/admin/klien_valid?id=" + j[1]);
    if(val.ok){
      Swal.fire({type:"success",title:"Berhasil",text:"Validasi klien telah diubah",confirmButtonClass:"btn btn-success"})
      .then(function(){
        location.reload();
      });
    }
  }  

}

async function lihat_detail(id){
  let j = id.split("-");
  let dt = await fetch("<?php echo $url; ?>/admin/klien_data?id=" + j[1]);
  let dts = await dt.json();
  if(dt.ok){  
    let k = "Klien belum mengisi data";
    if(dts.rows.length == 0){} else {
      k = "<div class='row'><div class='col-md-5'>Nama Klien</div><div class='col-md-5'>" + dts.rows[0].fullname + "</div></div>";
      k += "<div class='row'><div class='col-md-5'>Username/Password</div><div class='col-md-5'>" + dts.rows[0].username + "</div></div>";
      k += "<div class='row'><div class='col-md-5'>Alamat</div><div class='col-md-5'>" + dts.rows[0].alamat + "</div></div>";
      k += "<div class='row'><div class='col-md-5'>No Telp</div><div class='col-md-5'>" + dts.rows[0].no_hp + "</div></div>";
    }
     
      document.querySelector(".data-klien").innerHTML ="<div>" + k + "</div>"; 
  }
}

async function ubah_status(id){
  document.getElementById(id).innerHTML = "<i class='fa fa-circle-o-notch fa-fw fa-spin'></i>";
  let dt = await fetch("<?php echo $url; ?>/admin/klien_status?token=<?php echo $this->input->get("token"); ?>&id=" + id);
  if(dt.ok){ 
    Swal.fire({title:"Berhasil!",text:"Status klien telah diubah",type:"success",confirmButtonClass:"btn btn-success",buttonsStyling:!1})
    .then(function(){
      location.reload();
    });
 }
}

async function load_module(){
  let mod = await fetch("<?php echo $url; ?>/admin/module?token=<?php echo $this->input->get("token"); ?>");
  let data = await mod.json();
  if(mod.ok){
    let li = "";
    for(let i in data.rows){
        li += "<li class='nav-item'><a href='<?php echo $url; ?>/" + data.rows[i].link + "?token=<?php echo $this->input->get("token"); ?>'><i class='fa fa-gear'></i><span class='menu-item'>" + data.rows[i].name + "</span></a></li>";
    }
    document.getElementById("main-menu-navigation").innerHTML = li;
  }
}
load_module();
</script>

<?php require "footer.php"; ?>
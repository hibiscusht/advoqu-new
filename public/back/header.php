<!DOCTYPE html>
<!--
Template Name: Stack - Stack - Bootstrap 4 Admin Template
Author: PixInvent
Website: http://www.pixinvent.com/

-->
<html class="loading" lang="en" data-textdirection="ltr">
  <!-- BEGIN: Head-->
  
<!-- Mirrored from pixinvent.com/stack-responsive-bootstrap-4-admin-template/html/ltr/vertical-modern-menu-template/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 22 Nov 2020 02:12:05 GMT -->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta property="og:title" content="Advoqu" />
		<meta property="og:description" content="Advoqu | Layanan Hukum dalam Genggaman" />
		<meta property="og:image" content="<?php echo $backbase; ?>/img/advoqu_logo.png" />

		<!-- Favicon -->
		<link rel="shortcut icon" href="<?php echo $backbase; ?>/img/advicon.ico" type="image/x-icon" />
		<link rel="apple-touch-icon" href="<?php echo $backbase; ?>/img/advicon.ico">
    <title>R Subekti Law Office | ADVOQU</title> 
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo $backbase;?>/app-assets/vendors/css/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $backbase;?>/app-assets/vendors/css/tables/datatable/datatables.min.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo $backbase;?>/app-assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $backbase;?>/app-assets/css/bootstrap-extended.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $backbase;?>/app-assets/css/colors.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $backbase;?>/app-assets/css/components.min.css">
    <!-- END: Theme CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo $backbase;?>/app-assets/css/core/menu/menu-types/vertical-menu-modern.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $backbase;?>/app-assets/css/core/colors/palette-gradient.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $backbase;?>/app-assets/fonts/simple-line-icons/style.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $backbase;?>/app-assets/css/pages/card-statistics.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $backbase;?>/app-assets/css/pages/vertical-timeline.min.css">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo $backbase;?>/assets/css/style.css">
    <!-- END: Custom CSS-->

      <!-- BEGIN: Vendor JS-->
      
    <link rel="stylesheet" type="text/css" href="<?php echo $backbase;?>/app-assets/vendors/css/extensions/sweetalert2.min.css">
      <script src="<?php echo $backbase;?>/app-assets/vendors/js/vendors.min.js"></script>
    <script src="<?php echo $backbase;?>/app-assets/vendors/js/extensions/sweetalert2.all.min.js"></script>
    <!-- BEGIN Vendor JS-->

    
<link rel="stylesheet" href="<?php echo $frontbase; ?>/chat/chat.css?v=<?php echo rand(1,100000); ?>">
<script src="<?php echo $frontbase; ?>/chat/chat.js?v=<?php echo rand(1,100000); ?>"></script>

  </head>
  <!-- END: Head-->

  <style>
  .hidden {display: none}
  </style>

  <!-- BEGIN: Body-->
  <body class="vertical-layout vertical-menu-modern 2-columns   fixed-navbar" data-open="click" data-menu="vertical-menu-modern" data-col="2-columns">

    <!-- BEGIN: Header-->
    <nav class="header-navbar navbar-expand-lg navbar navbar-with-menu fixed-top navbar-semi-dark navbar-shadow">
      <div class="navbar-wrapper">
        <div class="navbar-header">
          <ul class="nav navbar-nav flex-row">
            <li class="nav-item mobile-menu d-lg-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="fa fa-bars fa-2x"></i></a></li>
            <li class="nav-item mr-auto"><a class="navbar-brand" href="<?php echo $url; ?>/admin/index?token=<?php echo $this->input->get("token"); ?>"><img class="brand-logo" alt="stack admin logo" src="<?php echo $backbase;?>/img/advoqu_logo2.png" style="width: 10%">
                <h3 class="brand-text">ADVOQU</h3></a></li>
            <li class="nav-item d-none d-lg-block nav-toggle"><a class="nav-link modern-nav-toggle pr-0" style="padding-top: 40%" data-toggle="collapse"><i class=" fa fa-arrows-h white" data-ticon="fa.fa-sitemap" style="font-size: 19pt"></i></a></li>
            <li class="nav-item d-lg-none"><a class="nav-link open-navbar-container" data-toggle="collapse" data-target="#navbar-mobile"><i class="fa fa-ellipsis-v"></i></a></li>
          </ul>
        </div>
        <div class="navbar-container content">
          <div class="collapse navbar-collapse" id="navbar-mobile">
            <ul class="nav navbar-nav mr-auto float-left">
             
            </ul>
            <ul class="nav navbar-nav float-right">
           
            <li class="dropdown dropdown-notification nav-item"><a class="nav-link nav-link-label" style="cursor: pointer"   data-toggle="modal" data-target="#largeModal" onclick="unchat()"><i class="fa fa-comment" style="font-size: 1.45em"></i><span class="badge badge-pill badge-danger badge-up hidden" style="font-size: 1pt; padding: 5px">&nbsp;</span></a>
              </li>


              <li class="dropdown dropdown-user nav-item"><a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
                  <div class="avatar avatar-online"><img src="<?php echo $backbase;?>/app-assets/images/portrait/small/avatar-s-1.png" alt="avatar"><i></i></div><span class="user-name"></span></a>
                <div class="dropdown-menu dropdown-menu-right">

             <!--   <a class="dropdown-item" href="user-profile.html"><i class="feather icon-user"></i> Edit Profile</a>
                
                <a class="dropdown-item" href="app-email.html"><i class="feather icon-mail"></i> My Inbox</a>
                
                <a class="dropdown-item" href="user-cards.html"><i class="feather icon-check-square"></i> Task</a> -->
                
                <a class="dropdown-item member hidden" href="<?php echo $url; ?>/curhatkasus/home?token=<?php echo $this->input->get("token"); ?>"><i class="fa fa-globe"></i> Kembali ke Advoqu Web</a>


                  <div class="dropdown-divider"></div>
                  
                  <a class="dropdown-item" href="" id="out"><i class="fa fa-sign-out"></i> Logout</a>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </nav>
    <!-- END: Header-->

    

    <!-- BEGIN: Main Menu-->
    <div class="main-menu menu-fixed menu-dark menu-accordion menu-shadow" data-scroll-to-active="true">
      <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
       
        </ul>
      </div>
    </div>
    <!-- END: Main Menu-->


    <!-- MODAL -->

    <div class="modal fade" id="largeModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
			<h4 class="modal-title" id="largeModalLabel"><i class="fa fa-comment"></i> Kontak Advomin</h4>
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			<div class="modal-body">
			
			
			<div class="col-lg-6 col-sm-offset-4 frame" style="margin: auto">
            <ul id="chat-body"></ul>
            <div>
                <div class="msj-rta macro">                        
                    <div class="text text-r" style="background:whitesmoke !important">
                        <input class="mytext" style="width: 100%" placeholder="Type a message" id="txt"/>
                    </div> 

                </div>
                <div style="padding: 5px;background: white;margin-top: 5px;margin-left: 5px; border-radius: 5px; cursor: pointer" onclick="submits()">
                    <span style="margin-top: 25%; color: #0088cc" class="fa fa-fw fa-paper-plane fa-2x"></span>
                </div>                
            </div>
        </div>       
			
			
			
			</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
												</div>
											</div>
										</div>
									</div>


    <!-- MODAL -->

    <script>
 
 function unchat(){
	resetChat();
insertChat("you", "Selamat Datang di Advoqu", 0);  
}

function submits(){
	insertChat("me", "hello world again", 0);
	insertChat("yout", "yes hello world again", 0);
}
    </script>
